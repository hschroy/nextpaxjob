import xml.etree.ElementTree as ET
from datetime import datetime
from pandas import Series, DataFrame
import pandas as pd
import csv
import os
from sqlalchemy import create_engine
import pymysql 
from sqlalchemy.types import Integer, Text, String, Numeric, DateTime, Boolean, Date
from tabulate import tabulate
import random
from np_constants import pms_list

start_time = datetime.now()

engine = create_engine("mysql+pymysql://{user}:{passwd}@{host}/{db}".format(host='35.192.116.102', user='henry', passwd='finley', db='nextpax', port=3306))

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname) 
# change working directory to same as current python file
# if local machine
dname = 'Xml'
os.chdir(dname) 
###############################################################
house_taxes = []

for pm_code in pms_list:
    xml_filename = 'paxgenerator_house_taxes_{pms}.xml'.format(pms=pm_code)
    xmlp = ET.XMLParser(encoding="utf-8")
    tree = ET.parse(xml_filename,parser=xmlp)
    root = tree.getroot()
    partner = root.find('Partner').text

    for taxes in root.iter('Taxes'):
        house_id = taxes.find('HouseID').text

        for tax in taxes.iter('Tax'):
            # add tax attributes to rate
            tax.attrib['Partner'] = partner
            tax.attrib['HouseID'] = house_id
            i = 0
            while i < len(tax):
                tax.attrib[tax[i].tag] = tax[i].text
                i += 1

            house_taxes.append(tax.attrib)

    xmlp = None
    tree = None
    root = None
    partner = None


taxes_df = DataFrame(house_taxes)

taxes_df.index = taxes_df.index.rename('id')
taxes_df = taxes_df.rename(columns={"Type": "TaxType"})

fieldschema={
        "Partner": String(50), "HouseID": String(50), "TaxType": String(50), "Percentage":  Numeric(11,8),
        "Included": Boolean
}

taxes_df = taxes_df.replace({'Included':{'true': True, 'false': False}})

taxes_df.to_sql('taxes', con = engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema)

print('\n# of Taxes:')
print(taxes_df.count()['TaxType'])
print('\n# by Property Mgr:')
# print(taxes_df.groupby('Partner').count()['TaxType'])
# print('\nTaxes for ',pms_list,':\n')
# print(taxes_df)

print(tabulate(pd.DataFrame(taxes_df.groupby('Partner')['TaxType'].count()), headers=['Partner','Count']))

print('\nTaxes for ',pms_list,':')

num_rows = len(taxes_df)
num_samples = 45

random_index = []

i = 0
while i < num_samples:
    random_int = random.randint(0,num_rows-1)
    if random_int not in random_index:
        random_index.append(random_int)
        i += 1

random_index.sort()

print(f'(randomly sampling {num_samples} taxes out of {num_rows}...)\nrow#')

# print(f'(sampling every {int(len(taxes_df)*0.033)} rows...)\n')

summary_fields = ['Partner','HouseID','TaxType','Percentage','Included']
print("<pre><code>")
print(tabulate(taxes_df.iloc[random_index][summary_fields], 
    headers=summary_fields, showindex=True))
print("</code></pre>")
# print(tabulate(taxes_df[taxes_df.index % int(len(taxes_df)*0.033) == 0][summary_fields], 
#     headers=summary_fields, showindex=False))


end_time = datetime.now()
print('Duration: {}'.format(end_time - start_time))