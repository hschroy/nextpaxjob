import pymysql 
import pandas as pd
from email_warn import send_warning
import datetime
from datetime import date
from dotenv import load_dotenv
import os
load_dotenv()

script_start = datetime.datetime.now()

print('\nCopy tables from nextpax to nextpax_db as a backup...')

NEXTPAX_HOST = os.getenv('NEXTPAX_HOST')
NEXTPAX_USER = os.getenv('NEXTPAX_USER')
NEXTPAX_PWD = os.getenv('NEXTPAX_PWD')

conn = pymysql.connect(host=NEXTPAX_HOST, user=NEXTPAX_USER, passwd=NEXTPAX_PWD, db='nextpax', port=3306, charset='utf8mb4')

cur = conn.cursor()
cur.execute('USE nextpax')

def do_table(tablename):
    # **PLACES
    # print(f'\n**** refreshing from preprocess:  nextpax.{tablename} --> nextpax_db.{tablename} ****')

    try:
        sql_string = f'DROP TABLE IF EXISTS nextpax_db.{tablename};'
        cur.execute(sql_string)
        # print('\n' + sql_string)
        sql_string = f'CREATE TABLE nextpax_db.{tablename} LIKE nextpax.{tablename};'
        cur.execute(sql_string)
        # print('\n' + sql_string)
        sql_string = f'INSERT nextpax_db.{tablename} SELECT * FROM nextpax.{tablename};'
        # print('\n' + sql_string)
        exec_result = cur.execute(sql_string)
        # print(f'\n{exec_result} row(s) affected...')
        conn.commit()
    except pymysql.Error as Error:
        msg = Error.args[1]
        send_warning(msg)
        print('\n' + msg)



for table_name in ['additional_costs','amenities','_AmountType','brands','calendars','_CostCode','_CostType',
    'daily_houses','descriptions','_DescriptionType','houses','houses_persist','_HouseType','outliers',
    'pictures','_PictureType','_PropertyCode','rates','reviews','skipped','taxes','_TaxType']:
    do_table(table_name)



cur.close()
conn.close()

cur = None

print(f"\n\nBackup nextpax DBs script elapsed time: {datetime.datetime.now() - script_start}")
