import requests
import json
import pymysql 
import pandas as pd
from pandas import Series, DataFrame
import datetime
import time
from tabulate import tabulate
import random
from sqlalchemy import create_engine
from sqlalchemy.types import Integer, Text, String, Numeric, DateTime, Boolean, Date, UnicodeText
from dotenv import load_dotenv
import os
load_dotenv()


NEXTPAX_HOST = os.getenv('NEXTPAX_HOST')
NEXTPAX_USER = os.getenv('NEXTPAX_USER')
NEXTPAX_PWD = os.getenv('NEXTPAX_PWD')

engine = create_engine("mysql+pymysql://{user}:{passwd}@{host}/{db}".format(host=NEXTPAX_HOST, user=NEXTPAX_USER, passwd=NEXTPAX_PWD, db='nextpax', port=3306, charset='utf8mb4'))

conn = pymysql.connect(host=NEXTPAX_HOST, user=NEXTPAX_USER, passwd=NEXTPAX_PWD, db='nextpax', port=3306, charset='utf8mb4')
cur = conn.cursor()
import_date = datetime.datetime.now().strftime("%Y-%m-%d")
from_date = (datetime.datetime.now() + datetime.timedelta(days = -1)).strftime("%Y-%m-%d")
until_date = (datetime.datetime.now() + datetime.timedelta(days = 730)).strftime("%Y-%m-%d")

token_start = datetime.datetime.now()
script_start = datetime.datetime.now()
print(f'nextpax import script starting at UTC datetime {script_start}')
error_retries = 0
RETRY_MAX = 101
HOUSE_RETRY_MAX = 2
RESPONSE_SUCCESS = 200
FAILED_DEFAULT = 99
dm_count = 0

CLIENT_SECRET = os.getenv('CLIENT_SECRET')
CLIENT_ID = os.getenv('CLIENT_ID')

client_secret=CLIENT_SECRET
client_id=CLIENT_ID
HTTP_HEADERS = {
    'Content-Type': 'application/x-www-form-urlencoded'
}
params = {
"client_secret":f"{client_secret}",
"client_id":f"{client_id}",
"grant_type":"client_credentials"
}

def sql_safe_str(value):
    if value is not None:
        sql_safe_str = value.replace("'","''").strip()
        return f"'{sql_safe_str}'"
    else:
        return 'null'

def safe_str(value):
    if value is not None:
        return f"{value}"
    else:
        return None

def safe_num(value):
    if value is not None:
        return value
    else:
        return None

def get_val(dict, key):
    try:
        return dict[key]
    except:
        return None

def safe_ratio(first, second):
    try:
        return first/second
    except:
        return 0
    
redirect = lambda x : x.replace('nextpax','sojournapi')

def join_safe(list_to_join):
    join_safe = ''
    for item in list_to_join:
        if item is not None:
            join_safe += item
    return join_safe

def get_persons(persons_list):
    get_persons = ''
    if persons_list[0] is not None:
        lowest = persons_list[0]
        highest = persons_list[0]

        for item in persons_list:
            lowest = min(lowest,item)
            highest = max(highest,item)

        get_persons = f'{lowest}-{highest}'
    return get_persons

def get_weekdays(days_list):
    get_weekdays = ''
    if len(days_list)>0:
        get_weekdays = '|'.join([str(x) for x in days_list])
    return get_weekdays

def get_access_token(error_retries, HTTP_HEADERS, params):
        endpoint = 'https://distribution.nextpax.app/auth/realms/api/protocol/openid-connect/token'
        # get access_token for this session
        response_status = FAILED_DEFAULT
        while response_status != RESPONSE_SUCCESS and error_retries < RETRY_MAX:
            try:
                response = requests.post(endpoint, headers=HTTP_HEADERS, data=params)
                response_status = response.status_code
                response_json = json.loads(response.content)
                if response_status != RESPONSE_SUCCESS and error_retries < RETRY_MAX:
                    print(f'response status {response_status}: wait one second, try again...')
                    time.sleep(1)
                    error_retries += 1
                    print(f"\n\nImport script elapsed time: {datetime.datetime.now() - script_start}")
                    print(f'error retries: {error_retries}')
            except:
                print(f'response status {response_status}: {endpoint}')
                print(f'error retries: {error_retries}')

        access_token = response_json['access_token']
        tok_dur = response_json['expires_in']

        pm_head = {
            "accept": "application/json",
            "Authorization": f"Bearer {access_token}"
        }
        print(f"\n\nNew access token at script elapsed time: {datetime.datetime.now() - token_start}\n\n")

        return pm_head, tok_dur

def print_me(msg):
    #print(msg)
    pass

# curl --location --request POST 'https://distribution.nextpax.app/auth/realms/api/protocol/openid-connect/token' \
# --header 'Content-Type: application/x-www-form-urlencoded' \
# --data-urlencode 'client_secret=c96ac38a-c9d5-4e50-94cb-56a8e24a66e0' \
# --data-urlencode 'client_id=client_SOJ358' \
# --data-urlencode 'grant_type=client_credentials'

pm_header, token_duration = get_access_token(error_retries, HTTP_HEADERS, params)

endpoint = "https://distribution.nextpax.app/api/v1/constants/mapping-codes?category="
response_status = FAILED_DEFAULT
while response_status != RESPONSE_SUCCESS and error_retries < RETRY_MAX:
    try:
        response = requests.get(endpoint, headers=pm_header)
        response_status = response.status_code
        mapping_json = json.loads(response.content)
        if response_status != RESPONSE_SUCCESS and error_retries < RETRY_MAX:
            print(f'response status {response_status}: wait one second, try again...')
            time.sleep(1)
            # token_start = datetime.datetime.now()
            # pm_header, token_duration = get_access_token(error_retries, HTTP_HEADERS, params)
            error_retries += 1
            print(f"\n\nImport script elapsed time: {datetime.datetime.now() - script_start}")
            print(f'error retries: {error_retries}')
    except:
        print(f'response status {response_status}: {endpoint}')
        print(f'error retries: {error_retries}')

#########################################################
if not mapping_json['data'][0]['propertyTypes'] is None:
    for property_type in mapping_json['data'][0]['propertyTypes']:
        house_type = property_type['typeCode']
        description = property_type['name']['en']
        sql_string = f"select * from nextpax._HouseType where HouseType = {sql_safe_str(house_type)}"
        cur.execute(sql_string)
        result = cur.fetchone()
        if not result is None:
            sql_string = f"update nextpax._HouseType set Description = {sql_safe_str(description)} where HouseType = {sql_safe_str(house_type)}"
        else:
            sql_string = f"insert into nextpax._HouseType (HouseType, Description) values ({sql_safe_str(house_type)}, {sql_safe_str(description)})"
        print_me('\n' + sql_string)
        exec_result = cur.execute(sql_string)
        print_me(f'\n{exec_result} row(s) affected...')
        conn.commit()
#########################################################
if not mapping_json['data'][0]['propertyFees'] is None:
    for property_fee in mapping_json['data'][0]['propertyFees']:
        cost_code = property_fee['feeCode']
        description = property_fee['name']['en']
        sql_string = f"select * from nextpax._CostCode where CostCode = {sql_safe_str(cost_code)}"
        cur.execute(sql_string)
        result = cur.fetchone()
        if not result is None:
            sql_string = f"update nextpax._CostCode set Description = {sql_safe_str(description)} where CostCode = {sql_safe_str(cost_code)}"
        else:
            sql_string = f"""insert into nextpax._CostCode (CostCode, Description) values ({sql_safe_str(cost_code)}, {sql_safe_str(description)})"""
        print_me('\n' + sql_string)
        exec_result = cur.execute(sql_string)
        print_me(f'\n{exec_result} row(s) affected...')
        conn.commit()
#########################################################
if not mapping_json['data'][0]['propertyTaxes'] is None:
    for property_tax in mapping_json['data'][0]['propertyTaxes']:
        tax_code = property_tax['taxCode']
        description = property_tax['name']['en']
        sql_string = f"select * from nextpax._TaxType where TaxType = {sql_safe_str(tax_code)}"
        cur.execute(sql_string)
        result = cur.fetchone()
        if not result is None:
            sql_string = f"update nextpax._TaxType set Description = {sql_safe_str(description)} where TaxType = {sql_safe_str(tax_code)}"
        else:
            sql_string = f"""insert into nextpax._TaxType (TaxType, Description) values ({sql_safe_str(tax_code)}, {sql_safe_str(description)})"""
        print_me('\n' + sql_string)
        exec_result = cur.execute(sql_string)
        print_me(f'\n{exec_result} row(s) affected...')
        conn.commit()
#########################################################
if not mapping_json['data'][0]['propertyImages'] is None:
    for property_pic in mapping_json['data'][0]['propertyImages']:
        type_id = property_pic['typeCode']
        description = property_pic['name']['en']
        sql_string = f"select * from nextpax._PictureType where TypeID = {sql_safe_str(type_id)}"
        cur.execute(sql_string)
        result = cur.fetchone()
        if not result is None:
            sql_string = f"update nextpax._PictureType set Description = {sql_safe_str(description)} where TypeID = {sql_safe_str(type_id)}"
        else:
            sql_string = f"""insert into nextpax._PictureType (TypeID, Description) values ({sql_safe_str(type_id)}, {sql_safe_str(description)})"""
        print_me('\n' + sql_string)
        exec_result = cur.execute(sql_string)
        print_me(f'\n{exec_result} row(s) affected...')
        conn.commit()
#########################################################
if not mapping_json['data'][0]['propertyChargeModes'] is None:
    for charge_type in mapping_json['data'][0]['propertyChargeModes']:
        cost_type = charge_type['typeCode']
        description = charge_type['name']['en']
        sql_string = f"select * from nextpax._CostType where CostType = {sql_safe_str(cost_type)}"
        cur.execute(sql_string)
        result = cur.fetchone()
        if not result is None:
            sql_string = f"update nextpax._CostType set Description = {sql_safe_str(description)} where CostType = {sql_safe_str(cost_type)}"
        else:
            sql_string = f"""insert into nextpax._CostType (CostType, Description) values ({sql_safe_str(cost_type)}, {sql_safe_str(description)})"""
        print_me('\n' + sql_string)
        exec_result = cur.execute(sql_string)
        print_me(f'\n{exec_result} row(s) affected...')
        conn.commit()                
#########################################################
if not mapping_json['data'][0]['propertyChargeTypes'] is None:
    for property_charge in mapping_json['data'][0]['propertyChargeTypes']:
        amount_type = property_charge['typeCode']
        description = property_charge['name']['en']
        sql_string = f"select * from nextpax._AmountType where AmountType = {sql_safe_str(amount_type)}"
        cur.execute(sql_string)
        result = cur.fetchone()
        if not result is None:
            sql_string = f"update nextpax._AmountType set Description = {sql_safe_str(description)} where AmountType = {sql_safe_str(amount_type)}"
        else:
            sql_string = f"""insert into nextpax._AmountType (AmountType, Description) values ({sql_safe_str(amount_type)}, {sql_safe_str(description)})"""
        print_me('\n' + sql_string)
        exec_result = cur.execute(sql_string)
        print_me(f'\n{exec_result} row(s) affected...')
        conn.commit()        
#########################################################
if not mapping_json['data'][0]['propertyDescriptionTypes'] is None:
    for description_type in mapping_json['data'][0]['propertyDescriptionTypes']:
        type_id = description_type['typeCode']
        description = description_type['name']['en']
        sql_string = f"select * from nextpax._DescriptionType where TypeID = {sql_safe_str(type_id)}"
        cur.execute(sql_string)
        result = cur.fetchone()
        if not result is None:
            sql_string = f"update nextpax._DescriptionType set Description = {sql_safe_str(description)} where TypeID = {sql_safe_str(type_id)}"
        else:
            sql_string = f"""insert into nextpax._DescriptionType (TypeID, Description) values ({sql_safe_str(type_id)}, {sql_safe_str(description)})"""
        print_me('\n' + sql_string)
        exec_result = cur.execute(sql_string)
        print_me(f'\n{exec_result} row(s) affected...')
        conn.commit()
#########################################################
if not mapping_json['data'][0]['propertyAmenities'] is None:
    for property_amenity in mapping_json['data'][0]['propertyAmenities']:
        property_code = property_amenity['amenityCode']
        amenity_type = property_amenity['amenityType']
        description = property_amenity['name']['en']
        if amenity_type == 'options':
            try:
                description = join_safe([description,f" ({join_safe([join_safe([x['attribute'],' = ',x['label']['en'],', ']) for x in property_amenity['options']]).rstrip(', ')})"])
            except:
                pass

        sql_string = f"select * from nextpax._PropertyCode where PropertyCode = {sql_safe_str(property_code)}"
        cur.execute(sql_string)
        result = cur.fetchone()
        if not result is None:
            sql_string = f"update nextpax._PropertyCode set Description = {sql_safe_str(description)} where PropertyCode = {sql_safe_str(property_code)}"
        else:
            sql_string = f"""insert into nextpax._PropertyCode (PropertyCode, Description) values ({sql_safe_str(property_code)}, {sql_safe_str(description)})"""
        print_me('\n' + sql_string)
        exec_result = cur.execute(sql_string)
        print_me(f'\n{exec_result} row(s) affected...')
        conn.commit()    

###############################################################################
endpoint = "https://distribution.nextpax.app/api/v1/content/property-managers"

response_status = FAILED_DEFAULT
while response_status != RESPONSE_SUCCESS and error_retries < RETRY_MAX:
    try:
        response = requests.get(endpoint, headers=pm_header)
        response_status = response.status_code
        pms_json = json.loads(response.content)
        if response_status != RESPONSE_SUCCESS and error_retries < RETRY_MAX:
            print(f'response status {response_status}: wait one second, try again...')
            time.sleep(1)
            # token_start = datetime.datetime.now()
            # pm_header, token_duration = get_access_token(error_retries, HTTP_HEADERS, params)
            error_retries += 1
            print(f"\n\nImport script elapsed time: {datetime.datetime.now() - script_start}")
            print(f'error retries: {error_retries}')
    except:
        print(f'response status {response_status}: {endpoint}')
        print(f'error retries: {error_retries}')

endpoint = "https://distribution.nextpax.app/api/v1/content/properties"

brand_report = []
unique_house_ids = []
house_coll = []
house_additional_costs = []
house_taxes = []
house_pictures = []
amenities = []
house_descriptions = []
house_rates = []
calendars = []
skips = []
dm_limit = 10
# dm_limit = 999999
print(f'limiting demo properties to {dm_limit}')
print(f'pulling rates and calendars until {until_date}')

for brand in pms_json['data']:
    BrandID = brand['propertyManager']
    if True: #BrandID == 'LR000131':
        #print('stop')
    
        NP_Name = brand['name']
        PMS = brand['pms']
        num_places = 0
        fee_count = 0
        tax_count = 0
        picture_count = 0
        amenity_count = 0
        description_count = 0
        rate_count = 0
        calendar_count = 0
        # print(f"\n{PMS} {BrandID} {NP_Name}")
        if PMS == 'PaxGenerator':
            list_count = 20
        else:
            list_count = 20
        while list_count == 20:
            # keep on offsetting
            if (datetime.datetime.now() - token_start).seconds > token_duration * .99:
                token_start = datetime.datetime.now()
                pm_header, token_duration = get_access_token(error_retries, HTTP_HEADERS, params)

            response_status = FAILED_DEFAULT
            while response_status != RESPONSE_SUCCESS and error_retries < RETRY_MAX:
                try:
                    response = requests.get(f'{endpoint}/?offset={num_places}&propertyManager={BrandID}&extras=all', headers=pm_header)
                    response_status = response.status_code
                    houselist_json = json.loads(response.content)
                    if response_status != RESPONSE_SUCCESS and error_retries < RETRY_MAX:
                        print('failure:')
                        print(f'token duration {token_duration} seconds; time since token start {(datetime.datetime.now() - token_start).seconds} seconds')
                        print(f'{endpoint}/?offset={num_places}&propertyManager={BrandID}&extras=all')
                        # print(f'headers={pm_header}')
                        print(f'response status {response_status}; response reason {response.reason}')
                        pass

                        print(f'response status {response_status}: wait one second, try again...')
                        time.sleep(1)
                        # token_start = datetime.datetime.now()
                        # pm_header, token_duration = get_access_token(error_retries, HTTP_HEADERS, params)
                        error_retries += 1
                        print(f"\n\nImport script elapsed time: {datetime.datetime.now() - script_start}")
                        print(f'error retries: {error_retries}')
                    else:
                        # print('success:')
                        # print(f'token duration {token_duration} seconds; time since token start {(datetime.datetime.now() - token_start).seconds} seconds')
                        # print(f'{endpoint}/?offset={num_places}&propertyManager={BrandID}&extras=all')
                        # print(f'headers={pm_header}')
                        # print(f'response status {response_status}; response reason {response.reason}')
                        pass
                except:
                    print(f'response status {response_status}: {endpoint}/?offset={num_places}&propertyManager={BrandID}&extras=all')
                    print(f'error retries: {error_retries}')

            try:
                house_list_is_none = houselist_json['data'] is None
            except:
                house_list_is_none = True

            if response_status != FAILED_DEFAULT and not house_list_is_none:
                list_count = 0
                for house in houselist_json['data']:
                    ##
                    ## Houses
                    ## 
                    list_count += 1
                    try:
                        house_images_is_none = house['images'] is None 
                    except:
                        house_images_is_none = True

                    try:
                        house_descriptions_is_none = house['descriptions'] is None 
                    except:
                        house_descriptions_is_none = True

                    try:
                        house_amenities_is_none = house['amenities'] is None 
                    except:
                        house_amenities_is_none = True

                    if PMS != 'PaxGenerator' or (PMS == 'PaxGenerator' and dm_count < dm_limit and not house_images_is_none
                        and not house_descriptions_is_none and not house_amenities_is_none):
                        if PMS == 'PaxGenerator':
                            dm_count += 1
                        house_fee_count = 0
                        house_tax_count = 0
                        house_pic_count = 0
                        house_amen_count = 0
                        house_desc_count = 0
                        house_rate_count = 0
                        house_cal_count = 0
                        house_err_count = 0
                        external_property_id = None
                        parent_id = None
                        classification = None

                        skip_me = False
                        House_ID = str(house['propertyId'])
                        if f'#{House_ID}#' not in unique_house_ids:
                            unique_house_ids.append(f'#{House_ID}#')

                            Property_Mgr = house['propertyManager']
                            print_me(f"\n\n{PMS} {BrandID} {NP_Name} {Property_Mgr} {House_ID}")
                            print_me(house['propertyId'])
                            # add house to table

                            try:
                                external_property_id = house['externalPropertyId']
                            except:
                                external_property_id = None

                            try:
                                parent_id = str(house['parentId'])
                            except:
                                parent_id = None

                            try:
                                classification = house['classification']
                            except:
                                classification = None

                            try:
                                try:
                                    house_address = safe_str(house['general']['address'])
                                except:
                                    house_address = ''

                                house_frame = {'HouseID': safe_str(House_ID), 'Brand': safe_str(Property_Mgr), 'Partner': safe_str(PMS),
                                    'HouseName': safe_str(house['general']['name']), 'HouseType': safe_str(house['general']['typeCode']),
                                    'Country': safe_str(house['general']['countryCode']), 'Place': safe_str(house['general']['city']),
                                    'Address': house_address, 'ZipCode': safe_str(get_val(get_val(house,'general'),'postalCode')),
                                    'Latitude': safe_num(house['general']['latitude']), 'Longitude': safe_num(house['general']['longitude']),
                                    'MinPersons': safe_num(house['general']['minOccupancy']), 'MaxPersons': safe_num(house['general']['maxOccupancy']),
                                    'NumberOfPets': safe_num(house['general']['maxPets']), 'ExternalPropertyId': safe_str(external_property_id),
                                    'ParentId': safe_str(parent_id), 'Classification': safe_str(classification)}
                                house_coll.append(house_frame)

                                # now add all fees (if there are any)
                                try:
                                    this_house_currency = 'USD'  # default to USD
                                    for fee in house['fees']:
                                        fee_count += 1
                                        house_fee_count += 1
                                        try:
                                            this_house_currency = safe_str(fee['currency'])
                                        except:
                                            pass

                                        fee_from = get_val(fee,'fromDate')
                                        if fee_from[19:] == 'Z':
                                            fee_from = fee_from[:10]

                                        fee_until = get_val(fee,'untilDate')
                                        if fee_until[19:] == 'Z':
                                            fee_until = fee_until[:10]

                                        additional_cost = {'Partner': safe_str(PMS), 'HouseID': safe_str(House_ID), 'CostCode': safe_str(fee['feeCode']),
                                            'CostType': safe_str(fee['chargeMode']), 'CostAmount': safe_ratio(fee['amount'],100), 
                                            'CostAmountType': safe_str(fee['chargeType']), 'CostCurrency': this_house_currency,
                                            'CostNumber': house_fee_count, 'FromDate': safe_str(fee_from), 'UntilDate': safe_str(fee_until)}
                                        
                                        house_additional_costs.append(additional_cost)

                                    print_me(f"{PMS} {BrandID} {NP_Name} {Property_Mgr} {House_ID} fees:{house_fee_count}")
                                except:
                                    pass
    
                                # now add all taxes (if there are any)
                                try:
                                    for tax in house['taxes']:
                                        tax_count += 1
                                        house_tax_count += 1
                                        tax_frame = {'Partner':safe_str(PMS), 'HouseID':safe_str(House_ID), 'TaxType':safe_str(tax['taxCode']),
                                        'Percentage':safe_num(tax['percentage']),'Included':safe_num(tax['included']),'TaxNumber':house_tax_count}
                                        house_taxes.append(tax_frame)
                                    print_me(f"{PMS} {BrandID} {NP_Name} {Property_Mgr} {House_ID} taxes:{house_tax_count}")
                                except:
                                    pass
    
                                # now add all pictures (if there are any)
                                try:
                                    for picture in house['images']:
                                        picture_count += 1
                                        house_pic_count += 1
                                        FileUrl = redirect(picture['url'])
                                        try:
                                            picture_frame = {'Partner':safe_str(PMS),'HouseID':safe_str(House_ID),
                                            'FileUrl':safe_str(FileUrl),'PictureType':safe_str(picture['typeCode']),'PictureNumber':house_pic_count}
                                            house_pictures.append(picture_frame)
                                        except:
                                            picture_frame = {'Partner':safe_str(PMS),'HouseID':safe_str(House_ID),
                                            'FileUrl':safe_str(FileUrl),'PictureType':None,'PictureNumber':house_pic_count}
                                            house_pictures.append(picture_frame)

                                    print_me(f"{PMS} {BrandID} {NP_Name} {Property_Mgr} {House_ID} images:{house_pic_count}")
                                except:
                                    pass

                                # now add all amenities (if there are any)
                                try:
                                    for amenity in house['amenities']:
                                        amenity_count += 1
                                        house_amen_count += 1
                                        amenity_frame = {'Partner':safe_str(PMS),'HouseID':safe_str(House_ID),
                                        'PropertyCode':safe_str(amenity['typeCode']),'AmenityValue':safe_str(join_safe(amenity['attribute'])),'AmenityNumber':house_amen_count}
                                        amenities.append(amenity_frame)

                                    print_me(f"{PMS} {BrandID} {NP_Name} {Property_Mgr} {House_ID} amenities:{house_amen_count}")
                                except:
                                    pass
    
                                # now add all descriptions (if there are any)
                                try:
                                    for description in house['descriptions']:
                                        description_count += 1
                                        house_desc_count += 1
                                        description_frame = {'Partner':safe_str(PMS),'HouseID':safe_str(House_ID),
                                        'LanguageCode':safe_str(description['language']),'Description':safe_str(description['text']),
                                        'DescriptionType':safe_str(description['typeCode']),'DescriptionNumber':house_desc_count}
                                        house_descriptions.append(description_frame)

                                    print_me(f"{PMS} {BrandID} {NP_Name} {Property_Mgr} {House_ID} descriptions:{house_desc_count}")
                                except:
                                    pass
                                ##
                                ## Rates
                                ##
                                if (datetime.datetime.now() - token_start).seconds > token_duration * .99:
                                    token_start = datetime.datetime.now()
                                    pm_header, token_duration = get_access_token(error_retries, HTTP_HEADERS, params)
                                rates_endpoint = "https://distribution.nextpax.app/api/v1/availability/rates"
                                response_status = FAILED_DEFAULT
                                while not skip_me and response_status != RESPONSE_SUCCESS and house_err_count < HOUSE_RETRY_MAX:
                                    try:
                                        # if House_ID == '667256':
                                        #     print("stop")

                                        rates_response = requests.get(f'{rates_endpoint}/{House_ID}?fromDate={from_date}&untilDate={until_date}', headers=pm_header)
                                        response_status = rates_response.status_code
                                        rates_json = json.loads(rates_response.content)
                                        if response_status != RESPONSE_SUCCESS and error_retries < RETRY_MAX:
                                            # print(f"\n\nfailure at import script elapsed time: {datetime.datetime.now() - script_start}")
                                            # print(f'token duration {token_duration} seconds; time since token start {(datetime.datetime.now() - token_start).seconds} seconds')
                                            # print(f'{rates_endpoint}/{House_ID}?untilDate={until_date}')
                                            # # print(f'headers={pm_header}')
                                            # print(f'response status {response_status}; response reason {rates_response.reason}')
                                            # pass

                                            # print(f'response status {response_status}: wait one second, try again...')
                                            time.sleep(0.5)
                                            # token_start = datetime.datetime.now()
                                            # pm_header, token_duration = get_access_token(error_retries, HTTP_HEADERS, params)
                                            # error_retries += 1
                                            house_err_count += 1
                                            if house_err_count >= HOUSE_RETRY_MAX:
                                                skip_me = True
                                                skip_frame = {'UTCDateTime': f'{datetime.datetime.now()}', 'HouseID': safe_str(House_ID), 'BrandID': safe_str(BrandID), 
                                                    'PMS': safe_str(PMS), 'PM_Name': safe_str(NP_Name), 'Description': 'rates', 
                                                    'Request': f'{rates_endpoint}/{House_ID}?untilDate={until_date}',
                                                    'Status': response_status, 'Reason': rates_response.reason, 
                                                    'TokenDur': token_duration, 'TokenTime': (datetime.datetime.now() - token_start).seconds}
                                                skips.append(skip_frame)
                                        else:
                                            # print('success:')
                                            # print(f'token duration {token_duration} seconds; time since token start {(datetime.datetime.now() - token_start).seconds} seconds')
                                            # print(f'{rates_endpoint}/{House_ID}?untilDate={until_date}')
                                            # print(f'headers={pm_header}')
                                            # print(f'response status {response_status}; response reason {rates_response.reason}')
                                            pass

                                    except:
                                        pass
                                        # print(f'response status {response_status}: {rates_endpoint}/{House_ID}?untilDate={until_date}')
                                        # print(f'error retries: {error_retries}')

                                try:
                                    if response_status != FAILED_DEFAULT and not rates_json['data'][0]['rates'] is None:
                                        for rate in rates_json['data'][0]['rates']:

                                            fee_from = rate['fromDate']
                                            if fee_from[19:] == 'Z':
                                                fee_from = fee_from[:10]

                                            fee_until = rate['untilDate']
                                            if fee_until[19:] == 'Z':
                                                fee_until = fee_until[:10]                                            

                                            if rate['baseAmount'] != 0:
                                                rate_count += 1
                                                house_rate_count += 1
                                                persons = get_persons(rate['persons'])
                                                weekdays = get_weekdays(rate['weekdays'])
                                                rate_frame = {'PropertyManagementSystem':safe_str(PMS),'HouseID':safe_str(House_ID),'PropertyManager':safe_str(Property_Mgr),
                                                'fromDate':safe_str(fee_from),'tillDate':safe_str(fee_until),'duration':safe_num(rate['duration']),
                                                'amount':safe_ratio(rate['baseAmount'],100),'currency':safe_str(rate['currency']),'persons':safe_str(persons),
                                                'weekdays':safe_str(weekdays),'minimumStay':safe_num(rate['minStay']),'maximumStay':safe_num(rate['maxStay']),
                                                'extraPersonFeeAmount':safe_ratio(rate['extraPersonFeeAmount'],100),'extraPersonFeeCurrency':safe_str(rate['extraPersonFeeCurrency']),
                                                'RateNumber':house_rate_count}
                                                house_rates.append(rate_frame)
                                except:
                                    pass
                                    # print(f"\nrates_json['data'] not found; HouseID = {House_ID}")

                                print_me(f"{PMS} {BrandID} {NP_Name} {Property_Mgr} {House_ID} rates:{house_rate_count}")
                                ##
                                ## Calendars
                                ##
                                if (datetime.datetime.now() - token_start).seconds > token_duration * .99:
                                    token_start = datetime.datetime.now()
                                    pm_header, token_duration = get_access_token(error_retries, HTTP_HEADERS, params)
                                calendars_endpoint = "https://distribution.nextpax.app/api/v1/availability/calendars"
                                response_status = 99
                                while not skip_me and response_status != RESPONSE_SUCCESS and house_err_count < HOUSE_RETRY_MAX:
                                    try:
                                        calendars_response = requests.get(f'{calendars_endpoint}/{House_ID}?fromDate={from_date}&untilDate={until_date}', headers=pm_header)
                                        response_status = calendars_response.status_code
                                        calendars_json = json.loads(calendars_response.content)
                                        if response_status != RESPONSE_SUCCESS and error_retries < RETRY_MAX:
                                            # print(f"\n\nfailure at import script elapsed time: {datetime.datetime.now() - script_start}")
                                            # print(f'token duration {token_duration} seconds; time since token start {(datetime.datetime.now() - token_start).seconds} seconds')
                                            # print(f'{calendars_endpoint}/{House_ID}?untilDate={until_date}')
                                            # # print(f'headers={pm_header}')
                                            # print(f'response status {response_status}; response reason {calendars_response.reason}')
                                            # pass

                                            # print(f'response status {response_status}: wait one second, try again...')
                                            time.sleep(0.5)
                                            # token_start = datetime.datetime.now()
                                            # pm_header, token_duration = get_access_token(error_retries, HTTP_HEADERS, params)
                                            # error_retries += 1
                                            house_err_count += 1
                                            if house_err_count >= HOUSE_RETRY_MAX:
                                                skip_me = True
                                                skip_frame = {'UTCDateTime': f'{datetime.datetime.now()}', 'HouseID': safe_str(House_ID), 'BrandID': safe_str(BrandID), 
                                                    'PMS': safe_str(PMS), 'PM_Name': safe_str(NP_Name), 'Description': 'calendars', 
                                                    'Request': f'{calendars_endpoint}/{House_ID}?untilDate={until_date}',
                                                    'Status': response_status, 'Reason': calendars_response.reason, 
                                                    'TokenDur': token_duration, 'TokenTime': (datetime.datetime.now() - token_start).seconds}
                                                skips.append(skip_frame)
                                        else:
                                            # print('success:')
                                            # print(f'token duration {token_duration} seconds; time since token start {(datetime.datetime.now() - token_start).seconds} seconds')
                                            # print(f'{calendars_endpoint}/{House_ID}?untilDate={until_date}')
                                            # print(f'headers={pm_header}')
                                            # print(f'response status {response_status}; response reason {calendars_response.reason}')
                                            pass

                                    except:
                                        pass
                                        # print(f'response status {response_status}: {calendars_endpoint}/{House_ID}?untilDate={until_date}')
                                        # print(f'error retries: {error_retries}')

                                try:
                                    if response_status != FAILED_DEFAULT and not calendars_json['data'][0]['availability'] is None:
                                        for calendar in calendars_json['data'][0]['availability']:
                                            calendar_count += 1
                                            house_cal_count += 1
                                            calendar_frame = {'PropertyManagementSystem':safe_str(PMS),'HouseID':safe_str(House_ID),'PropertyManager':safe_str(Property_Mgr),
                                            'calendarDate':safe_str(calendar['date']),'quantity':safe_num(calendar['quantity']),'arrivalAllowed':safe_num(calendar['restrictions']['arrivalAllowed']),
                                            'departureAllowed':safe_num(calendar['restrictions']['departureAllowed']),'minimumStay':safe_num(calendar['restrictions']['minStay']),
                                            'maximumStay':safe_num(calendar['restrictions']['maxStay']),'CalendarNumber':house_cal_count}
                                            calendars.append(calendar_frame)
                                except:
                                    pass
                                    # print(f"\ncalendars_json['data'] not found; HouseID = {House_ID}")

                                print_me(f"{PMS} {BrandID} {NP_Name} {Property_Mgr} {House_ID} calendars:{house_cal_count}")
                                print_me(f"Token elapsed time: {(datetime.datetime.now() - token_start).seconds}")
                                print_me(f"Token duration:     {token_duration}    {PMS} Num of places:{(dm_count if PMS == 'PaxGenerator' else num_places+list_count)}")
                                print_me(f"Import script elapsed time: {datetime.datetime.now() - script_start}")
                                print_me(f'error retries: {error_retries}')
                            except Exception as err:
                                print(f"skipping house {House_ID}. Status: {house['status']}. Import script elapsed time: {datetime.datetime.now() - script_start}")
                                pass

                num_places += list_count
            else:
                list_count = 0

        # update brands
        sql_string = f"""SELECT id FROM nextpax.brands WHERE BrandID = '{BrandID}'"""
        cur.execute(sql_string)
        result = cur.fetchone()
        if not result is None:
            brands_id = result[0]
            sql_string = f"""UPDATE nextpax.brands SET NP_Name = '{NP_Name}', PMS = '{PMS}', LatestImport = '{import_date}',
            Num_Places = {(dm_count if PMS == 'PaxGenerator' else num_places)}, Num_Fees = {fee_count}, Num_Taxes = {tax_count}, Num_Pictures = {picture_count},
            Num_Amenities = {amenity_count}, Num_Descriptions = {description_count}, Num_Rates = {rate_count},
            Num_Calendars = {calendar_count}
            WHERE id = {brands_id}"""
        else:
            sql_string = f"""INSERT INTO nextpax.brands (BrandID, NP_Name, PMS, LatestImport, Num_Places, Num_Fees, Num_Taxes,
            Num_Pictures, Num_Amenities, Num_Descriptions, Num_Rates, Num_Calendars) 
            VALUES ('{BrandID}', '{NP_Name}', '{PMS}', '{import_date}', {(dm_count if PMS == 'PaxGenerator' else num_places)}, {fee_count}, {tax_count}, {picture_count},
            {amenity_count}, {description_count}, {rate_count}, {calendar_count})"""

        # print('\n' + sql_string)
        exec_result = cur.execute(sql_string)
        # print(f'\n{exec_result} row(s) affected...')
        conn.commit()
        brand_frame = {'BrandID':f"{BrandID.ljust(8)}",'NumPlaces':(f"{dm_count:,}" if PMS == 'PaxGenerator' else f"{num_places:,}"),
        'PMS': PMS,'NPName':NP_Name,'Rates':f"{rate_count:,}",'Calendars':f"{calendar_count:,}",
        'TokenTime':str(datetime.datetime.now() - token_start),'ErrorRetries':error_retries}

        # print(f"{BrandID.ljust(8)}\tNum Places:  {str(dm_count if PMS == 'PaxGenerator' else num_places).zfill(5)}\t{PMS}  {NP_Name}\tRates: {str(rate_count).zfill(6)}\tCalendars: {str(calendar_count).zfill(6)}\tToken elapsed time: {datetime.datetime.now() - token_start}\t\t{error_retries} error retries...")
        # if PMS == 'PaxGenerator':
        #     print(f'        (limiting DEMO properties to {dm_limit} houses)\n')

        brand_report.append(brand_frame)

if len(skips) > 0:
    print(f'\nThe following properties have been skipped:')
    print("<pre><code>")
    skips_df = DataFrame(skips)
    print(tabulate(skips_df,headers=skips_df.columns.tolist(), showindex=False))
    print("</code></pre>")
else:
    skip_frame = {'UTCDateTime': f'{datetime.datetime.now()}', 'HouseID': None, 'BrandID': None, 
    'PMS': None, 'PM_Name': None, 'Description': 'No Skips', 'Request': None,
    'Status': None, 'Reason': None, 'FailureTime': None,
    'TokenDur': None, 'TokenTime': None}
    skips.append(skip_frame)
    skips_df = DataFrame(skips)


fieldschema = {'UTCDateTime': String(30), 'HouseID': String(50), 'BrandID': String(50), 'PMS': String(50), 'PM_Name': String(50), 
    'Description': String(25), 'Request': String(100), 'Status': Integer, 'Reason': String(25), 
    'TokenDur': Integer, 'TokenTime': Integer}
skips_df.index = skips_df.index.rename('id')
skips_df.to_sql('skipped', con = engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema)


print(f"\n\nImport script elapsed time: {datetime.datetime.now() - script_start}")
brand_rep_df = DataFrame(brand_report)
print("<pre><code>")
print(tabulate(brand_rep_df,headers=brand_rep_df.columns.tolist()))
print("</code></pre>")


print('\nCreating houses table:  ')
fieldschema={
    'Partner': String(50), 'Brand': String(50), 'HouseID': String(50), 'Country': String(50), 
    'Place': Text, 'Address': Text, 'ZipCode': String(50), 'HouseName': Text,	'MinPersons': Integer, 
    'MaxPersons': Integer, 'MinChildren': Integer,	'NumberOfPets': Integer, 'HouseType': String(20), 
    'Latitude': Numeric(18,13), 'Longitude': Numeric(17,12), 'Currency': String(50), 'LicenseNumber': String(50),
    'ExternalPropertyId': String(50), 'ParentId': String(50), 'Classification': String(50)
}
house_df = DataFrame(house_coll)
house_df.index = house_df.index.rename('id')
house_df.to_sql('houses', con = engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema)
print(f'created {len(house_df)} houses')
########################################
print('\n# by Property Mgr:')
print(tabulate(pd.DataFrame(house_df.groupby('Partner')['Place'].count()), headers=['Partner','Count']))
num_rows = len(house_df)
num_samples = 45
random_index = []
i = 0
while i < num_samples:
    random_int = random.randint(0,num_rows-1)
    if random_int not in random_index:
        random_index.append(random_int)
        i += 1
random_index.sort()
print(f'(randomly sampling {num_samples} houses out of {num_rows}...)\nrow#')
summary_fields = ['Partner','HouseID','Place','HouseName','Country','Latitude','Longitude']
print("<pre><code>")
print(tabulate(house_df.iloc[random_index][summary_fields], headers=summary_fields, showindex=True))
print("</code></pre>")
#################################################################################
print(f"\n\nImport script elapsed time: {datetime.datetime.now() - token_start}")
print('\nCreating additional_costs table:  ')
fieldschema={
	'Partner': String(50), 'HouseID': String(50), 'CostCode': String(50), 'CostType': String(50),
	'CostAmount': Numeric(10,2), 'CostAmountType': String(50), 'CostCurrency': String(50),
	'CostNumber': Integer, 'FromDate': Date, 'UntilDate': Date
}
additional_costs_df = DataFrame(house_additional_costs)
additional_costs_df.index = additional_costs_df.index.rename('id')
# drop duplicates
additional_costs_df = additional_costs_df.drop_duplicates(subset=['Partner', 'HouseID', 'CostCode', 'CostType', 'CostAmount', 'CostAmountType', 'CostCurrency', 'FromDate', 'UntilDate'])

additional_costs_df.to_sql('additional_costs', con = engine, if_exists = 'replace', 
    chunksize = 1000, dtype=fieldschema)
print(f'created {len(additional_costs_df)} additional_costs')
#############################################################
num_rows = len(additional_costs_df)
num_samples = 45
random_index = []
i = 0
while i < num_samples:
    random_int = random.randint(0,num_rows-1)
    if random_int not in random_index:
        random_index.append(random_int)
        i += 1
random_index.sort()
print(f'(randomly sampling {num_samples} additional costs out of {num_rows} rows...)\nrow#')
summary_fields = ['Partner','HouseID','CostCode','CostType','CostAmount','CostAmountType','CostCurrency','FromDate','UntilDate']
print("<pre><code>")
print(tabulate(additional_costs_df.iloc[random_index][summary_fields], 
    headers=summary_fields, showindex=True))
print("</code></pre>")
#################################################################################
print(f"\n\nImport script elapsed time: {datetime.datetime.now() - token_start}")
print('\nCreating taxes table:  ')
fieldschema={
        "Partner": String(50), "HouseID": String(50), "TaxType": String(50), "Percentage":  Numeric(11,8),
        "Included": Boolean, "TaxNumber": Integer
}
taxes_df = DataFrame(house_taxes)
taxes_df.index = taxes_df.index.rename('id')
taxes_df.to_sql('taxes', con = engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema)
print(f'created {len(taxes_df)} taxes')
#######################################
num_rows = len(taxes_df)
num_samples = 45
random_index = []
i = 0
while i < num_samples:
    random_int = random.randint(0,num_rows-1)
    if random_int not in random_index:
        random_index.append(random_int)
        i += 1
random_index.sort()
print(f'(randomly sampling {num_samples} taxes out of {num_rows}...)\nrow#')
summary_fields = ['Partner','HouseID','TaxType','Percentage','Included']
print("<pre><code>")
print(tabulate(taxes_df.iloc[random_index][summary_fields], 
    headers=summary_fields, showindex=True))
print("</code></pre>")
#################################################################################
print(f"\n\nImport script elapsed time: {datetime.datetime.now() - token_start}")
print('\nCreating pictures table:  ')
fieldschema={
	'Partner': String(50), 'HouseID': String(50), 'FileUrl': Text, 'PictureType': String(25), 'PictureNumber':Integer
}
pictures_df = DataFrame(house_pictures)
pictures_df.index = pictures_df.index.rename('id')
pictures_df.to_sql('pictures', con = engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema)
print(f'created {len(pictures_df)} pictures')
##############################################
num_rows = len(pictures_df)
num_samples = 45
random_index = []
i = 0
while i < num_samples:
    random_int = random.randint(0,num_rows-1)
    if random_int not in random_index:
        random_index.append(random_int)
        i += 1
random_index.sort()
print(f'(randomly sampling {num_samples} pictures out of {num_rows}...)\nrow#')
summary_fields = ['Partner','HouseID','FileUrl','PictureType']
image_prefix = '<p><img src="'
image_postfix = '" alt="" width="500"></p>'
pictures_df['FileUrl'] = pictures_df['FileUrl'] + image_prefix + pictures_df['FileUrl'] + image_postfix
print(tabulate(pictures_df.iloc[random_index][summary_fields], 
    headers=summary_fields, showindex=True))
#################################################################################
print(f"\n\nImport script elapsed time: {datetime.datetime.now() - token_start}")
print('\nCreating amenities table:  ')
fieldschema={
	'Partner': String(50), 'HouseID': String(50), 'PropertyCode': String(50), 'AmenityValue': String(50), 'AmenityNumber': Integer
}
amenities_df = DataFrame(amenities)
amenities_df.index = amenities_df.index.rename('id')
# drop duplicates
amenities_df = amenities_df.drop_duplicates(subset=['Partner' ,'HouseID', 'PropertyCode', 'AmenityValue'])
amenities_df.to_sql('amenities', con = engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema)
print(f'created {len(amenities_df)} amenities')
###############################################
num_rows = len(amenities_df)
num_samples = 45
random_index = []
i = 0
while i < num_samples:
    random_int = random.randint(0,num_rows-1)
    if random_int not in random_index:
        random_index.append(random_int)
        i += 1
random_index.sort()
print(f'(randomly sampling {num_samples} amenities out of {num_rows}...)\nrow#')
summary_fields = ['Partner','HouseID','PropertyCode','AmenityValue']
print("<pre><code>")
print(tabulate(amenities_df.iloc[random_index][summary_fields], 
    headers=summary_fields, showindex=True))
print("</code></pre>")
#################################################################################
print(f"\n\nImport script elapsed time: {datetime.datetime.now() - token_start}")
print('\nCreating descriptions table:  ')
fieldschema={
	'Partner': String(50), 'HouseID': String(50), 'LanguageCode': String(5), 'Description': UnicodeText, 'DescriptionType': String(25), 'DescriptionNumber': Integer
}
descriptions_df = DataFrame(house_descriptions)
descriptions_df.index = descriptions_df.index.rename('id')
descriptions_df.to_sql('descriptions', con = engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema)
print(f'created {len(descriptions_df)} descriptions')
#####################################################
num_rows = len(descriptions_df)
num_samples = 5
random_index = []
i = 0
while i < num_samples:
    random_int = random.randint(0,num_rows-1)
    if random_int not in random_index:
        random_index.append(random_int)
        i += 1
random_index.sort()
print(f'(randomly sampling {num_samples} descriptions out of {num_rows}...)\nrow#')
summary_fields = ['Partner','HouseID','Description']
print("<pre><code>")
print(tabulate(descriptions_df.iloc[random_index][summary_fields],headers=summary_fields,tablefmt='plain',showindex=True))
print("</code></pre>")
#################################################################################
print(f"\n\nImport script elapsed time: {datetime.datetime.now() - token_start}")
print('\nCreating rates table:  ')
fieldschema={
        "PropertyManagementSystem": String(50), "HouseID": String(50), "PropertyManager": String(50),
        "fromDate":  Date, "tillDate": Date, 'duration': Integer, 'amount': Numeric(10,2), 'currency': String(50),
        'persons': String(50), 'weekdays': String(50), 'minimumStay': Integer, 'maximumStay': Integer,
        'extraPersonFeeAmount': Numeric(10,2), 'extraPersonFeeCurrency': String(50), 'RateNumber': Integer
}
rates_df = DataFrame(house_rates)
rates_df.index = rates_df.index.rename('id')
rates_df.to_sql('rates', con = engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema)
print(f'created {len(rates_df)} rates')
#######################################
num_rows = len(rates_df)
num_samples = 45
random_index = []
i = 0
while i < num_samples:
    random_int = random.randint(0,num_rows-1)
    if random_int not in random_index:
        random_index.append(random_int)
        i += 1
random_index.sort()
print(f'(randomly sampling {num_samples} rates out of {num_rows}...)\nrow#')
summary_fields = ['PropertyManagementSystem','HouseID','fromDate','tillDate','duration','amount','currency','minimumStay']
print("<pre><code>")
print(tabulate(rates_df.iloc[random_index][summary_fields], 
    headers=summary_fields, showindex=True))
print("</code></pre>")
#################################################################################
print(f"\n\nImport script elapsed time: {datetime.datetime.now() - token_start}")
print('\nCreating calendars table:  ')
fieldschema={'PropertyManagementSystem': String(50), 'HouseID': String(50), 'PropertyManager': String(50),
	'calendarDate': Date, 'quantity': Integer, 'arrivalAllowed': Boolean, 'departureAllowed': Boolean,
	'minimumStay': Integer, 'maximumStay': Integer, 'CalendarNumber': Integer
}
calendars_df = DataFrame(calendars)
calendars_df.index = calendars_df.index.rename('id')
calendars_df.to_sql('calendars', con = engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema)
print(f'created {len(calendars_df)} calendars')
###############################################
num_rows = len(calendars_df)
num_samples = 45
random_index = []
i = 0
while i < num_samples:
    random_int = random.randint(0,num_rows-1)
    if random_int not in random_index:
        random_index.append(random_int)
        i += 1
random_index.sort()
print(f'(randomly sampling {num_samples} calendar records out of {num_rows}...)\nrow#')
summary_fields = ['PropertyManagementSystem','HouseID','calendarDate','quantity','arrivalAllowed','departureAllowed','minimumStay','maximumStay']
print("<pre><code>")
print(tabulate(calendars_df.iloc[random_index][summary_fields], 
    headers=summary_fields, showindex=True))
print("</code></pre>")
##########################
print('\nIMPORT FINISHED')
print(f"\n\nImport script total elapsed time: {datetime.datetime.now() - script_start}")
print(f'error retries: {error_retries}')



cur.close()
conn.close()

cur = None
engine.dispose()

