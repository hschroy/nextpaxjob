import pymysql 
import pandas as pd
from email_warn import send_warning
import random

print('\n**** NEXTPAX POSTPROCESS... ****')

conn = pymysql.connect(host='34.70.178.230', user='henry', passwd='finley', db='sojourn', port=3306)

cur = conn.cursor()
cur.execute('USE sojourn')

def do_amenity(fieldname, search_string, not_like):
    print(f'\n**** checking amenity:  {fieldname} ****')
    sql_string = f'''SELECT sojournID FROM sojourn_db.places WHERE {fieldname} = 0 AND sojournID IN 
        (SELECT sojournID FROM sojourn_db.descriptions WHERE description LIKE '{search_string}'
        AND description NOT LIKE '{not_like}')'''
    print(sql_string)
    num_rows = cur.execute(sql_string)
    print(f'{num_rows} places where {fieldname} is in description but is set to False\n')
    if num_rows > 0:

        num_samples = 45
        if num_samples >= num_rows:
            num_samples = num_rows
            random_index = list(range(0,num_rows))
            print(f'\n(showing {num_samples} out of {num_rows} places updated...)')
        else:
            random_index = []
            i = 0
            while i < num_samples:
                random_int = random.randint(0,num_rows-1)
                if random_int not in random_index:
                    random_index.append(random_int)
                    i += 1
            random_index.sort()
            print(f'\n(randomly sampling {num_samples} out of {num_rows} places updated...)')


        place_ids = pd.read_sql(sql_string,conn)
        for index, row in place_ids.iterrows(): 
            sql_string=f'''SELECT description FROM sojourn_db.descriptions 
                WHERE description LIKE '{search_string}' AND description NOT LIKE '{not_like}' AND sojournID = '{row.sojournID}'
            ''' 
            # print('\n' + sql_string)
            description_result = pd.read_sql(sql_string,conn)
            # print(f'\n{exec_result} row(s) affected...')
            for i, desc_row in description_result.iterrows():
                _description = desc_row.description
                _position = _description.upper().find(search_string.upper().replace('%',''))
                _from = _description[0:_position].rfind(' ',0,-25)
                _to = _position + _description[(_position+25):].find(' ') + 26
                sql_string=f'''UPDATE sojourn_db.places SET {fieldname} = 1 WHERE sojournID = '{row.sojournID}'
                '''
                exec_result = cur.execute(sql_string)
                conn.commit()
                if exec_result > 0:
                    if index in random_index:
                        # only show a random sampling of up to num_samples
                        print(f'{row.sojournID}  ...{_description[_from:_to]}... {fieldname} updated to True'.replace('\n',''))


sql_string = '''ALTER TABLE `sojourn_db`.`places` 
CHANGE COLUMN `sojournID` `sojournID` VARCHAR(50) NOT NULL ,
ADD PRIMARY KEY (`sojournID`)
'''
print('\n' + sql_string)
try:
    exec_result = cur.execute(sql_string)
    print(f'\n{exec_result} row(s) affected...')
    conn.commit()
except pymysql.Error as Error:
    msg = Error.args[1]
    # send_warning(msg)
    print('\n' + msg)
    print('\n** (this routine has already been run today) **')


amenities_to_check = [
    {
        'field_name': 'kitchen',
        'search_string': '%kitchen%',
        'not_like': ''
    },
    {
        'field_name': 'dryer',
        'search_string': '%dryer%',
        'not_like': ''
    },
    {
        'field_name': 'washer',
        'search_string': '%washer%',
        'not_like': ''
    },
    # this was ending up with too many false positives, ie: -Sorry, not Pet Friendly ($250 fine will be assessed for bringing any pets into this property).
    # {
    #     'field_name': 'pets_allowed',
    #     'search_string': '% pets%',
    #     'not_like': '%no pets%'
    # },
    {
        'field_name': 'TV',
        'search_string': '% TV%',
        'not_like': ''
    },
    {
        'field_name': 'hot_tub',
        'search_string': '%Jacuzzi%',
        'not_like': ''
    },
    # {
    #     'field_name': 'pool',
    #     'search_string': '% pool%',
    #     'not_like': '% pool table%'   # this will miss cases where a place has both a 'pool' and a 'pool table' ...
    # }
]

[do_amenity(x['field_name'], x['search_string'], x['not_like']) for x in amenities_to_check]

cur.close()
conn.close()

cur = None

