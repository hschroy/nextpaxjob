# using SendGrid's Python Library
# https://github.com/sendgrid/sendgrid-python
import os
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import *
from pathlib import Path
import datetime
from datetime import date

start_time = datetime.datetime.now()

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
# change working directory to same as current python file
os.chdir(dname) 

morninglog = "Hello from sojourn-api. This is the output from today's morningjob:\n\n\n" + Path('morning.log').read_text()

message = Mail(
    from_email='henry@furraylogic.com',
    to_emails='henry@furraylogic.com', #['henry@furraylogic.com','george@furraylogic.com','murray@furraylogic.com'],
    subject=start_time.strftime("sojourn-api-dev: %m/%d/%Y, %H:%M:%S"),
    plain_text_content = Content("text/plain", morninglog))
    #html_content=morninglog) # '<strong>and easy to do anywhere, even with Python</strong>')
    #to_emails='henry@furraylogic.com',
    #  #,'george@furraylogic.com','murray@furraylogic.com'],
tracking_settings = TrackingSettings()
tracking_settings.click_tracking = ClickTracking(enable=False)
tracking_settings.open_tracking = OpenTracking(enable=False)
tracking_settings.subscription_tracking = SubscriptionTracking(enable=False)
message.tracking_settings = tracking_settings

try:
    sg = SendGridAPIClient(os.environ.get('SENDGRID_API_KEY'))
    response = sg.send(message)
    print(response.status_code)
    print(response.body)
    print(response.headers)
except Exception as e:
    print(e.message)