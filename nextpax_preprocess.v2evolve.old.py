from flask import Flask, request, render_template, render_template_string, session, redirect, url_for, flash, Markup, g
from flask_bootstrap import Bootstrap
from flask_moment import Moment 
import datetime
from datetime import date
from flask_wtf import FlaskForm 
from wtforms import StringField, SubmitField, DateField, IntegerField
import timestring
import json
from wtforms.validators import DataRequired, Length 
import time

import os
import pandas as pd
from pandas import DataFrame
from flask_sqlalchemy import SQLAlchemy 
from sqlalchemy import create_engine, func, or_
import pymysql 
from sqlalchemy.types import Integer, Text, String, Numeric, DateTime, Boolean, Date, UnicodeText, SmallInteger
from flask_table import Table, Col, LinkCol
from geopy.geocoders import GoogleV3
from geopy import distance
import ccy
import pymysql 

conn = pymysql.connect(host='104.197.116.86', user='henry', passwd='finley', db='sojourn', port=3306)
cur = conn.cursor()
cur.execute('USE sojourn')

# moving data from nextpax staging (ingest) to sojourn dev api db

geolocator = GoogleV3(api_key="AIzaSyA_gzlbNDzI-CfN0rvNlpL9UFv1vkLVGjY",
     domain='maps.googleapis.com')

start_time = datetime.datetime.now()

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
app.config['SECRET_KEY'] = 'obiandfinley2020!'
app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://{user}:{passwd}@{host}/{db}?charset=utf8mb4".format(host='35.192.116.102', user='henry', passwd='finley', db='nextpax', port=3306)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
bootstrap = Bootstrap(app)
moment = Moment(app)

sojourn_engine = create_engine("mysql+pymysql://{user}:{passwd}@{host}/{db}?charset=utf8mb4".format(host='104.197.116.86', user='henry', passwd='finley', db='sojourn_db', port=3306))

class module_object:
    pass
# create search_area variable global to this module
_m = module_object()
#_m.search_area = None
_m.check_in = date.today()
_m.date_today = date.today()
_m.check_out = date.today() + datetime.timedelta(1)
_m.num_guests = 1
_m.num_geolocate_calls = 0

# Nextpax data objects:
class brand(db.Model):
    __tablename__ = 'brands'
    id = db.Column(db.BigInteger, primary_key=True)
    BrandID = db.Column(db.String(50), db.ForeignKey('houses.Brand'))
    NP_Name = db.Column(db.String(100))
    PMS = db.Column(db.String(50))
    LatestImport = db.Column(db.Date)
    Num_Places = db.Column(db.Integer)
    Num_Fees = db.Column(db.Integer)
    Num_Taxes = db.Column(db.Integer)
    Num_Pictures = db.Column(db.Integer)
    Num_Amenities = db.Column(db.Integer)
    Num_Descriptions = db.Column(db.Integer)
    Num_Rates = db.Column(db.Integer)
    Num_Calendars = db.Column(db.Integer)
    PMC = db.Column(db.String(100))
    MainContact = db.Column(db.String(100))
    Phone = db.Column(db.String(100))
    Email = db.Column(db.String(150))
    Location = db.Column(db.String(100))
    Payment = db.Column(db.String(25))
    AddtlMarkups = db.Column(db.Boolean)
    DefaultRating = db.Column(db.Numeric(6,4))
    Discount = db.Column(db.Numeric(9,5))
    NetRate = db.Column(db.Numeric(9,5))
    BaseMarkup = db.Column(db.Numeric(9,5))

class review(db.Model):
    __tablename__ = 'reviews'
    id = db.Column(db.BigInteger, primary_key=True)
    HouseID = db.Column(db.String(50), db.ForeignKey('houses.HouseID'))
    rating = db.Column(db.SmallInteger)
    review_summary = db.Column(db.String(100))
    review_detail = db.Column(db.Text)
    reviewed_by = db.Column(db.String(50))

# lookup table
class tax_type(db.Model):
    __tablename__ = '_TaxType'
    TaxType = db.Column(db.String(50), primary_key=True)
    Description = db.Column(db.Text)
    taxes = db.relationship('tax', backref='tax_type', lazy='dynamic')

# lookup table
class house_type(db.Model):
    __tablename__ = '_HouseType'
    HouseType = db.Column(db.String(50), primary_key=True)
    Description = db.Column(db.Text)
    houses = db.relationship('house', backref='house_type', lazy='dynamic')
    def __repr__(self):
        return self.Description

# lookup table
class property_code(db.Model):
    __tablename__ = '_PropertyCode'
    PropertyCode = db.Column(db.String(50), primary_key=True)
    Description = db.Column(db.Text)
    amenities = db.relationship('amenity', backref='property_code', lazy='dynamic')
    def __repr__(self):
        return self.Description

# lookup table
class cost_code(db.Model):
    __tablename__ = '_CostCode'
    CostCode = db.Column(db.String(50), primary_key=True)
    Description = db.Column(db.Text)
    additional_costs = db.relationship('additional_cost', backref='cost_code', lazy='dynamic')
    def __repr__(self):
        return self.Description

# lookup table
class cost_type(db.Model):
    __tablename__ = '_CostType'
    CostType = db.Column(db.String(50), primary_key=True)
    Description = db.Column(db.Text)
    additional_costs = db.relationship('additional_cost', backref='cost_type', lazy='dynamic')
    def __repr__(self):
        return self.Description

# lookup table
class amount_type(db.Model):
    __tablename__ = '_AmountType'
    AmountType = db.Column(db.String(50), primary_key=True)
    Description = db.Column(db.Text)
    additional_costs = db.relationship('additional_cost', backref='amount_type', lazy='dynamic')
    def __repr__(self):
        return self.Description

# lookup table
class description_type(db.Model):
    __tablename__ = '_DescriptionType'
    TypeID = db.Column(db.String(50), primary_key=True)
    Description = db.Column(db.Text)
    descriptions = db.relationship('description', backref='description_type', lazy='dynamic')

    def __repr__(self):
        return self.Description

# lookup table
class picture_type(db.Model):
    __tablename__ = '_PictureType'
    TypeID = db.Column(db.String(50), primary_key=True)
    Description = db.Column(db.Text)
    pictures = db.relationship('picture', backref='picture_type', lazy='dynamic')

    def __repr__(self):
        return self.Description

class additional_cost(db.Model):
    __tablename__ = 'additional_costs'
    id = db.Column(db.BigInteger, primary_key=True)
    Partner = db.Column(db.String(50))
    HouseID = db.Column(db.String(50), db.ForeignKey('houses.HouseID'))
    CostCode = db.Column(db.String(50), db.ForeignKey('_CostCode.CostCode'))
    CostType = db.Column(db.String(50), db.ForeignKey('_CostType.CostType'))
    CostAmount = db.Column(db.Numeric(10,2), index=True)
    CostAmountType = db.Column(db.String(50), db.ForeignKey('_AmountType.AmountType'))
    CostCurrency = db.Column(db.String(50))
    CostNumber = db.Column(db.Integer)
    FromDate = db.Column(db.Date, index=True)
    UntilDate = db.Column(db.Date, index=True)

class picture(db.Model):
    __tablename__ = 'pictures'
    id = db.Column(db.BigInteger, primary_key=True)
    Partner = db.Column(db.String(50))
    HouseID = db.Column(db.String(50), db.ForeignKey('houses.HouseID'))
    FileUrl = db.Column(db.Text)
    PictureType = db.Column(db.String(50), db.ForeignKey('_PictureType.TypeID'))
    PictureNumber = db.Column(db.Integer)
    def __repr__(self):
        return self.FileUrl
        
class rate(db.Model):
    __tablename__ = 'rates'
    id = db.Column(db.BigInteger, primary_key=True)
    PropertyManagementSystem = db.Column(db.String(50))
    HouseID = db.Column(db.String(50), db.ForeignKey('houses.HouseID'))
    PropertyManager = db.Column(db.String(50))
    fromDate = db.Column(db.Date, index=True)
    tillDate = db.Column(db.Date, index=True)
    duration = db.Column(db.Integer)
    amount = db.Column(db.Numeric(10,2))
    currency = db.Column(db.String(50))
    persons = db.Column(db.String(50))
    weekdays = db.Column(db.String(50))
    minimumStay = db.Column(db.Integer)
    maximumStay = db.Column(db.Integer)
    extraPersonFeeAmount = db.Column(db.Numeric(10,2))
    extraPersonFeeCurrency = db.Column(db.String(50))
    RateNumber = db.Column(db.Integer)

class description(db.Model):
    __tablename__ = 'descriptions'
    id = db.Column(db.BigInteger, primary_key=True)
    Partner = db.Column(db.String(50))
    HouseID = db.Column(db.String(50), db.ForeignKey('houses.HouseID'))
    LanguageCode = db.Column(db.String(5))
    Description = db.Column(db.UnicodeText)
    DescriptionType = db.Column(db.String(50), db.ForeignKey('_DescriptionType.TypeID'))
    DescriptionNumber = db.Column(db.Integer)
    def __repr__(self):
        return self.Description
    
class amenity(db.Model):
    __tablename__ = 'amenities'
    id = db.Column(db.BigInteger, primary_key=True)
    Partner = db.Column(db.String(50))
    HouseID = db.Column(db.String(50), db.ForeignKey('houses.HouseID'))
    PropertyCode = db.Column(db.String(50), db.ForeignKey('_PropertyCode.PropertyCode'))
    AmenityValue = db.Column(db.String(50))
    AmenityNumber = db.Column(db.Integer)
    
class tax(db.Model):
    __tablename__ = 'taxes'
    id = db.Column(db.BigInteger, primary_key=True)
    Partner = db.Column(db.String(50))
    HouseID = db.Column(db.String(50), db.ForeignKey('houses.HouseID'))
    TaxType = db.Column(db.String(50), db.ForeignKey('_TaxType.TaxType'))
    Percentage = db.Column(db.Numeric(11,8))
    Included = db.Column(db.Boolean)
    TaxNumber = db.Column(db.Integer)

class calendar(db.Model):
    __tablename__ = 'calendars'
    id = db.Column(db.BigInteger, primary_key=True)
    PropertyManagementSystem = db.Column(db.String(50))
    HouseID = db.Column(db.String(50), db.ForeignKey('houses.HouseID'))
    PropertyManager = db.Column(db.String(50))
    calendarDate = db.Column(db.Date, index=True)
    quantity = db.Column(db.Integer)
    arrivalAllowed = db.Column(db.Boolean)
    departureAllowed = db.Column(db.Boolean)
    minimumStay = db.Column(db.Integer)
    maximumStay = db.Column(db.Integer)
    CalendarNumber = db.Column(db.Integer)

class house(db.Model):
    __tablename__ = 'houses'
    id = db.Column(db.BigInteger, primary_key=True)
    Partner = db.Column(db.String(50))
    Brand = db.Column(db.String(50))
    HouseID = db.Column(db.String(50), nullable=False, unique=True, index=True)
    Country = db.Column(db.String(50))
    Place = db.Column(db.Text)
    Address = db.Column(db.Text)
    ZipCode = db.Column(db.String(50))
    HouseName = db.Column(db.Text)
    MinPersons = db.Column(db.Integer)
    MaxPersons = db.Column(db.Integer)
    MinChildren = 0 #db.Column(db.Integer)
    NumberOfPets = db.Column(db.Integer)
    HouseType = db.Column(db.Integer, db.ForeignKey('_HouseType.HouseType'))
    Latitude = db.Column(db.Numeric(10,8))
    Longitude = db.Column(db.Numeric(11,8))
    Currency = '' #db.Column(db.String(50))
    LicenseNumber = '' #db.Column(db.String(50))
    taxes = db.relationship('tax', backref='house', lazy='dynamic')
    calendars = db.relationship('calendar', backref='house', lazy='dynamic')
    amenities = db.relationship('amenity', backref='house', lazy='dynamic')
    additional_costs = db.relationship('additional_cost', backref='house', lazy='dynamic')
    descriptions = db.relationship('description', backref='house', lazy='dynamic')
    pictures = db.relationship('picture', backref='house', lazy='dynamic')
    rates = db.relationship('rate', backref='house', lazy='dynamic')
    reviews = db.relationship('review', backref='house', lazy='dynamic')


    def rate_value(self):
        rate_duration = 1  # get day rate
        rates_query = self.rates.filter(rate.fromDate <= _m.date_today, 
            rate.tillDate >= _m.date_today, rate.duration == rate_duration)

        if rates_query.count() > 0:
            rates_query_first = rates_query.first()

            rate_amount = rates_query_first.amount
            rate_currency = rates_query_first.currency
            return rate_amount
        else:
            try:
                rates_query_first = self.rates.order_by('fromDate','duration').first()
                rate_amount = round(rates_query_first.amount / rates_query_first.duration,2)
                return rate_amount
            except :
                return 0

    def has_wifi(self):
        wifi = self.amenities.filter(amenity.PropertyCode == 'W07')
        if wifi.count() > 0:
            return wifi.first().AmenityValue == 'Y'
        else:
            return False

    def has_iron(self):
        iron = self.amenities.filter(or_(amenity.PropertyCode == 'I03',amenity.PropertyCode == 'I04'))
        if iron.count() > 0:
            return True
            # return iron.first().AmenityValue == 'Y'
        else:
            return False

    def has_air_conditioning(self):
        air_conditioning = self.amenities.filter(or_(amenity.PropertyCode == 'A01', amenity.PropertyCode == 'A05', \
            amenity.PropertyCode == 'A06'))
        #  * . Air conditioning A01 Air conditioning ('Y' = available) A05	Air conditioning in living room ('Y' = available) 
        # A06 Air conditioning in bedroom(s) ('Y' = available)  
        if air_conditioning.count() > 0:
            return air_conditioning.first().AmenityValue == 'Y'
        else:
            return False

    def has_hot_tub(self):
        hot_tub = self.amenities.filter(or_(amenity.PropertyCode == 'W04', amenity.PropertyCode == 'J01')) 
        # whirlpool | jacuzzi 
        # ('Y' = available, 'I' = indoor available, 'O' = outdoor available)
        if hot_tub.count() > 0:
            firstValue = hot_tub.first().AmenityValue
            return firstValue == 'Y' or firstValue == 'I' or firstValue == 'O'
        else:
            return False

    def has_pool(self):
        house_pool = self.amenities.filter(or_(amenity.PropertyCode == 'P18', amenity.PropertyCode == 'P04', \
            amenity.PropertyCode == 'P10', amenity.PropertyCode == 'P05', amenity.PropertyCode == 'P09', \
                amenity.PropertyCode == 'C16'))
        #  # (P18 Swimming pool ('Y' = available)), P04 Indoor swimming pool ('Y' = available, 'P' = private, 'C' = communal), 
        # P05 Outdoor swimming pool ('Y' = available, 'P' = private, 'C' = communal), P09 Private swimming pool, can be inside 
        # or outside ('Y' = available) , P10 Heated swimming pool, can be inside or outside ('Y' = available), C16	Children 
        # swimming Pool ('Y' = available)
        if house_pool.count() > 0:
            firstValue = house_pool.first().AmenityValue
            return firstValue == 'Y' or firstValue == 'P' or firstValue == 'C'
        else:
            return False

    def has_washer(self):
        house_washer = self.amenities.filter(or_(amenity.PropertyCode == 'L03', amenity.PropertyCode == 'W01'))
        # (L03 Laundromat ('Y' = available, 'C' = in the city, 'P' = park facility, 'S' = service)), W01	
        # Washing machine ('Y' = available)             
        if house_washer.count() > 0:
            firstValue = house_washer.first().AmenityValue
            return firstValue == 'Y'
        else:
            return False

    def has_free_parking(self):
        house_park_free = self.amenities.filter(or_(amenity.PropertyCode == 'P21', amenity.PropertyCode == 'G01', 
            amenity.PropertyCode == 'P27', amenity.PropertyCode == 'V10'), amenity.AmenityValue == 'Y')
        if house_park_free.count() > 0:
            return True
        else:
            return False

    def has_TV(self):
        house_TV = self.amenities.filter(or_(amenity.PropertyCode == 'L12', amenity.PropertyCode == 'F10', amenity.PropertyCode == 'S05', amenity.PropertyCode == 'T01'))
        # * . TV L12 Shared lounge/TV area F10 Flat-screen TV ('Y' = available) S05 Satellite TV ('Y' = available)  
        if house_TV.count() > 0:
            # firstValue = house_TV.first().AmenityValue
            # return firstValue == 'Y'
            return True
        else:
            return False

    def has_dryer(self):
        house_dryer = self.amenities.filter(amenity.PropertyCode == 'D08')
        # * . Dryer (D08 Dryer ('Y' = available))
        if house_dryer.count() > 0:
            firstValue = house_dryer.first().AmenityValue
            return firstValue == 'Y'
        else:
            return False

    def has_breakfast(self):
        house_breakfast = self.amenities.filter(amenity.PropertyCode == 'B23')
        #. Breakfast (B23 Breakfast ('Y' = Free, 'S' = Surcharge))
        if house_breakfast.count() > 0:
            firstValue = house_breakfast.first().AmenityValue
            return firstValue == 'Y' or firstValue == 'S'
        else:
            return False

    def has_hangers(self):
        house_hangers = self.amenities.filter(amenity.PropertyCode == 'H42')
        # . Hangers (H42 Hangers ('Y' = available))
        if house_hangers.count() > 0:
            firstValue = house_hangers.first().AmenityValue
            return firstValue == 'Y'
        else:
            return False

    def has_hair_dryer(self):
        house_hair_dryer = self.amenities.filter(amenity.PropertyCode == 'H41')
        # . Hair dryer (H41 Hair dryer ('Y' = available))
        if house_hair_dryer.count() > 0:
            firstValue = house_hair_dryer.first().AmenityValue
            return firstValue == 'Y'
        else:
            return False

    def has_smoke_alarm(self):
        house_smoke_alarm = self.amenities.filter(amenity.PropertyCode == 'S44')
        # . Smoke alarm (S44 Smoke alarm ('Y' = available))
        if house_smoke_alarm.count() > 0:
            firstValue = house_smoke_alarm.first().AmenityValue
            return firstValue == 'Y'
        else:
            return False

    def has_high_chair(self):
        house_high_chair = self.amenities.filter(amenity.PropertyCode == 'C14')
        # . High chair (C14 Baby chair(s)/highchair(s) on request)
        return house_high_chair.count() > 0

    def pets_allowed(self):
        house_pets_ok = self.amenities.filter(amenity.PropertyCode == 'P02')
        # * . P02 House is suitable for pets ('Y' = applicable to this house, 'R' = on request)
        if house_pets_ok.count() > 0:
            firstValue = house_pets_ok.first().AmenityValue
            return firstValue == 'Y' or firstValue == 'R'
        else:
            return False

    def has_kitchen(self):
        house_kitchen = self.amenities.filter(amenity.PropertyCode == 'K04')
        # . Kitchen (K04 Kitchen ('Y' = available))
        if house_kitchen.count() > 0:
            firstValue = house_kitchen.first().AmenityValue
            return firstValue == 'Y'
        else:
            return False

    def has_heating(self):
        house_heating = self.amenities.filter(amenity.PropertyCode == 'K04')
        # . Heating (H40	Heating ('C' = central heating, 'E' = electric, 'F' = floor, 'G' = gas,  
        # 'H' = hot air, 'S' = stove, 'Y' = available))
        if house_heating.count() > 0:
            firstValue = house_heating.first().AmenityValue
            return firstValue == 'C' or firstValue == 'E' or firstValue == 'F' or firstValue == 'G' \
                or firstValue == 'H' or firstValue == 'S' or firstValue == 'Y'
        else:
            return False

    def has_fireplace(self):
        house_fireplace = self.amenities.filter(amenity.PropertyCode == 'F02')
        # . Indoor fireplace (F02	Fireplace or heater ('Y' = available))
        if house_fireplace.count() > 0:
            firstValue = house_fireplace.first().AmenityValue
            return firstValue == 'Y'
        else:
            return False

    def laptop_friendly(self):
        house_laptop = self.amenities.filter(amenity.PropertyCode == 'L06')
        # . Laptop-friendly workspace (L06 Laptop friendly ('Y' = available))
        if house_laptop.count() > 0:
            firstValue = house_laptop.first().AmenityValue
            return firstValue == 'Y'
        else:
            return False

    def has_crib(self):
        house_crib = self.amenities.filter(amenity.PropertyCode == 'B15')
        # . Crib (B15 Babybed ('Y' = available))
        if house_crib.count() > 0:
            firstValue = house_crib.first().AmenityValue
            return firstValue == 'Y'
        else:
            return False

    def carbon_monoxide(self):
        house_CO = self.amenities.filter(amenity.PropertyCode == 'C18')
        # . Carbon monoxide alarm (C18 Carbon monoxide detector ('Y' = available))
        if house_CO.count() > 0:
            firstValue = house_CO.first().AmenityValue
            return firstValue == 'Y'
        else:
            return False

    def has_gym(self):
        house_gym = self.amenities.filter(amenity.PropertyCode == 'F12')
        # . Gym (F12 Fitness/Gym ('Y' = available))
        if house_gym.count() > 0:
            firstValue = house_gym.first().AmenityValue
            return firstValue == 'Y'
        else:
            return False

    def ski_in_out(self):
        house_ski_in = self.amenities.filter(or_(amenity.PropertyCode == 'S50', amenity.PropertyCode == 'S51'))
        # . Ski-in/ski-out (S50 Ski in Property Location) (S51 Ski out Property Location)
        return house_ski_in.count() > 0

    def num_beds(self):
        # * . # of Beds    (B13 Number of sleeping spots in bunkbeds)
        #   (D09 Number of sleeping spots in double beds)
        #   (K01 Number of (extra) cots / kids beds) 
        # (K05 Number of sleeping spots in King beds) 
        #   (S30 Number of sleeping spots in single beds) 
        # (S31 Number of sleeping spots on sofas/sofabeds)

        if self.amenities.filter(amenity.PropertyCode == 'B17').count() > 0:
            # (B17 Total number of beds)
            total_num_beds = int(self.amenities.filter(amenity.PropertyCode == 'B17').first().AmenityValue)
        else:
            total_num_beds = 0

        if self.amenities.filter(amenity.PropertyCode == 'S02').count() > 0:
            # (S02 Number of single beds)
            num_single_beds = int(self.amenities.filter(amenity.PropertyCode == 'S02').first().AmenityValue)
        else:
            num_single_beds = 0

        if self.amenities.filter(amenity.PropertyCode == 'D02').count() > 0:
            # (D02 Number of double beds)
            num_double_beds = int(self.amenities.filter(amenity.PropertyCode == 'D02').first().AmenityValue)
        else:
            num_double_beds = 0

        if self.amenities.filter(amenity.PropertyCode == 'G02').count() > 0:
            # (G02 Number of king size beds (= extra large double bed))
            num_king_beds = int(self.amenities.filter(amenity.PropertyCode == 'G02').first().AmenityValue)
        else:
            num_king_beds = 0

        if self.amenities.filter(amenity.PropertyCode == 'K06').count() > 0:
            # (K06 Number of King Beds (= extra large double bed))
            num_king_beds = num_king_beds + int(self.amenities.filter(amenity.PropertyCode == 'K06').first().AmenityValue)
        else:
            num_king_beds = num_king_beds

        if self.amenities.filter(amenity.PropertyCode == 'Q02').count() > 0:
            # (Q02 Number of Queen beds)
            num_queen_beds = int(self.amenities.filter(amenity.PropertyCode == 'Q02').first().AmenityValue)
        else:
            num_queen_beds = 0

        if self.amenities.filter(amenity.PropertyCode == 'B06').count() > 0:
            #(B06 Number of bunk beds)
            num_bunk_beds = int(self.amenities.filter(amenity.PropertyCode == 'B06').first().AmenityValue)
        else:
            num_bunk_beds = 0
        total_num_beds = max((num_single_beds + num_double_beds + num_king_beds + num_queen_beds + num_bunk_beds), total_num_beds)

        if total_num_beds == 0:
            total_num_beds = self.num_bedrooms()
        return total_num_beds

    def num_bedrooms(self):
        # * . # of Bedrooms (B02 Number of bedrooms)
        house_bedrooms = self.amenities.filter(amenity.PropertyCode == 'B02')
        if house_bedrooms.count() > 0:
            return house_bedrooms.first().AmenityValue
        else:
            return 0

    def num_bathrooms(self):
        # * . # of Bathrooms (B01 Number of bathrooms)
        house_bathrooms = self.amenities.filter(amenity.PropertyCode == 'B01')
        if house_bathrooms.count() > 0:
            num_baths = house_bathrooms.first().AmenityValue
            if num_baths == 'Y':
                num_baths = 1
            return num_baths
        else:
            return 0

    def house_latitude(self):
        if self.Latitude == 0 or self.Latitude is None:
            found = find_HouseID(self.HouseID)
            if not found is None:
                self.Latitude = found[1]
                self.Longitude = found[2]
                return self.Latitude

            mylookup = join_safe([self.Address,' ',self.Place,' ',self.Country,' ',self.ZipCode])
            myplace = geolocator.geocode(mylookup)
            _m.num_geolocate_calls = _m.num_geolocate_calls + 1
            self.Longitude =  myplace.point.longitude
            self.Latitude = myplace.point.latitude
        return self.Latitude

    def house_longitude(self):
        if self.Longitude == 0 or self.Longitude is None:
            found = find_HouseID(self.HouseID)
            if not found is None:
                self.Latitude = found[1]
                self.Longitude = found[2]
                return self.Longitude

            mylookup = join_safe([self.Address,' ',self.Place,' ',self.Country,' ',self.ZipCode])
            myplace = geolocator.geocode(mylookup)
            self.Latitude = myplace.point.latitude
            self.Longitude =  myplace.point.longitude

        return self.Longitude
    # geolocate the house address from coordinates
    def location(self):
        if self.Latitude is None:
            self.Latitude = self.house_latitude()

        if self.Longitude is None:
            self.Longitude = self.house_longitude()

        housepoint = self.Latitude, self.Longitude
        houseplace = None
        if not (housepoint[0] == 0 and housepoint[1] == 0):
            found = find_location(housepoint)
            if not found is None:
                return found

            houseplace = geolocator.reverse(housepoint, exactly_one=True)
            _m.num_geolocate_calls = _m.num_geolocate_calls + 1
        else:
            found = find_HouseID(self.HouseID)
            if not found is None:
                return found[0]

        if houseplace is None:
            mylookup = join_safe([self.Address,' ',self.Place,' ',self.Country,' ',self.ZipCode])
            myplace = geolocator.geocode(mylookup)
            _m.num_geolocate_calls = _m.num_geolocate_calls + 1
            if myplace is None:
                return ''
            else:
                return myplace.address
        else:
            return houseplace.address

    def currency_name(self):
        return self.Currency
    
    def first_pic(self):
        pic_url = self.pictures.first()
        if pic_url is None:
            return ''
        else:
            return pic_url.FileUrl

    def get_rating(self):
        if self.reviews.with_entities(func.avg(review.rating)).first()[0] is not None:
            # return average rating from reviews if any exist
            return self.reviews.with_entities(func.avg(review.rating)).first()[0]

        property_mgr = brand.query.filter(brand.BrandID == self.Brand).first()
        if not property_mgr is None:
            if property_mgr.DefaultRating > 0:
                # add a review for this default PM rating
                new_review = review()
                new_review.HouseID = self.HouseID
                new_review.rating = property_mgr.DefaultRating
                new_review.review_summary = 'Sojourn Estimated Rating'
                new_review.review_detail = 'This rating was selected by an industry expert using industry standards'
                new_review.reviewed_by = 'Sojourn API'
                db.session.add(new_review)
                db.session.commit()
            # return default rating for property mgr
            return property_mgr.DefaultRating
        else:
            # otherwise, default to zero
            return 0

    def num_reviews(self):
        if self.reviews.count() == 0:
            property_mgr = brand.query.filter(brand.BrandID == self.Brand).first()
            if not property_mgr is None:
                if property_mgr.DefaultRating > 0:
                    return 1
                else:
                    return 0

        return self.reviews.count()

def find_location(lookup_point):
    # use cached latitude and longitude to return location
    sql_string = f"""SELECT location FROM sojourn.geo_cache
    WHERE latitude = {lookup_point[0]} AND longitude = {lookup_point[1]}
    """
    cur.execute(sql_string)
    result = cur.fetchone()
    if not result is None:
        return result[0]
    else:
        return None

def find_HouseID(HouseID):
    try:
        # use cached House information to bypass geolookup
        sql_string = f"""SELECT location, latitude, longitude, city, state, country
        FROM sojourn.geo_cache WHERE sojournID = 'NP{HouseID}'"""
        cur.execute(sql_string)
        result = cur.fetchone()
        if not result is None:
            return {'location':result[0], 'latitude':result[1], 'longitude':result[2],
            'city':result[3],'state':result[4],'country':result[5]}
        else:
            return None
    except:
        return None


def join_safe(list_to_join):
    join_safe = ''
    for item in list_to_join:
        if item is not None:
            join_safe += item
    return join_safe

print('\n**** NEXTPAX PREPROCESS - CONVERT TO SOJOURN DB ****')
# **PLACES
print('\nnextpax preprocess: UPDATING sojourn_db.places')
fieldschema={"sojournID": String(50), "PMS": String(50), "pm_id": String(50), "property_name": Text, 'country': String(50),  'city':String(50),
    'state': String(50),'place': String(100), 
    'street': Text, "location":  Text, 'zip_code': String(50), 'latitude': Numeric(18,13), 'longitude': Numeric(17,12), 
    'min_persons': Integer, 'max_persons': Integer, 'min_children': Integer, 'number_of_pets': Integer, 
    'house_type': Text, 'picture': Text, 'currency': String(50), 'rate': Numeric(10,2), 
    'wifi': Boolean, 'iron': Boolean, 'air_conditioning': Boolean, 'hot_tub': Boolean, 'pool': Boolean, 'washer': Boolean, 
    'free_parking': Boolean, 'TV': Boolean, 'dryer': Boolean, 'breakfast': Boolean, 'hangers': Boolean, 'hair_dryer': Boolean, 
    'high_chair': Boolean, 'smoke_alarm': Boolean, 'pets_allowed': Boolean, 'kitchen': Boolean, 'heating': Boolean, 
    'fireplace': Boolean, 'laptop_friendly': Boolean, 'crib': Boolean, 'carbon_monoxide_alarm': Boolean, 'gym': Boolean, 
    'ski_in_out': Boolean, 'num_beds': Integer, 'num_bedrooms': Integer, 'num_bathrooms': Integer,
    'average_rating': Numeric(6,4), 'num_reviews': Integer}

places_coll = []
print('\ncreating places table...')
error_retries = 0
for x in house.query.all():
    state = ''
    city = ''
    location = ''
    country = ''
    geo_success = False
    geo_cached = find_HouseID(x.HouseID)
    if geo_cached is not None:
        state = geo_cached['state']
        city = geo_cached['city']
        country = geo_cached['country']
        location = geo_cached['location']
        x.Latitude = geo_cached['latitude']
        x.Longitude = geo_cached['longitude']
        x.Country = country
    else:
        while not geo_success and error_retries < 21:
            geo_success = False
            try:
                mylookup = join_safe([x.Address,' ',x.Place,' ',x.Country,' ',x.ZipCode])
                myplace = geolocator.geocode(mylookup)
                _m.num_geolocate_calls += 1
                geo_success = True
                if myplace is not None:
                    for add_comp in myplace.raw['address_components']:
                        if add_comp['types'][0] == 'administrative_area_level_1':
                            state = add_comp['long_name']
                        if add_comp['types'][0] in ['locality','neighborhood']:
                            city = add_comp['long_name']
                        if add_comp['types'][0] == 'country':
                            country = add_comp['short_name']

                    location = myplace.address
                    if x.Latitude is None or x.Latitude != myplace.point.latitude:
                        x.Latitude = myplace.point.latitude
                    if x.Longitude is None or x.Longitude != myplace.point.longitude:
                        x.Longitude = myplace.point.longitude
                    if x.Country is None or x.Country != country:
                        x.Country = country  
            except:
                print(f'geo coding failure: wait half a second, try again...')
                time.sleep(.5)
                error_retries += 1
                print(f"Preprocessing elapsed time: {datetime.datetime.now() - start_time}")
                print(f'error retries: {error_retries}')

    # print(f'{x.HouseID}\t{x.Address}, {city}, {state}\t\t\t{location}\t\t{x.Place}')
    place_frame = {'sojournID':'NP' + x.HouseID, 'PMS':x.Partner, 'pm_id':x.Brand, 'property_name':x.HouseName, 'country':x.Country, 'city':city,
    'state':state,'place':f'{x.Country}, {state}, {x.Place}', 
    'street':x.Address,'location':location, 'zip_code':x.ZipCode, 'latitude':x.Latitude, 
    'longitude':x.Longitude, 
    'min_persons':x.MinPersons, 'max_persons':x.MaxPersons, 'min_children':x.MinChildren, 
    'number_of_pets':x.NumberOfPets, 'house_type':('Home' if x.house_type is None else x.house_type.Description), 
    'picture':x.first_pic(), 'currency':x.currency_name(), 
    'rate':x.rate_value(),'wifi':x.has_wifi(), 'iron':x.has_iron(), 'air_conditioning':x.has_air_conditioning(), 
    'hot_tub':x.has_hot_tub(), 'pool':x.has_pool(), 'washer':x.has_washer(), 'free_parking':x.has_free_parking(), 
    'TV':x.has_TV(), 'dryer':x.has_dryer(), 'breakfast':x.has_breakfast(), 'hangers':x.has_hangers(), 'hair_dryer':x.has_hair_dryer(), 
    'high_chair':x.has_high_chair(), 'smoke_alarm':x.has_smoke_alarm(), 'pets_allowed':x.pets_allowed(), 
    'kitchen':x.has_kitchen(), 'heating':x.has_heating(), 'fireplace':x.has_fireplace(), 'laptop_friendly':x.laptop_friendly(), 
    'crib':x.has_crib(), 'carbon_monoxide_alarm':x.carbon_monoxide(), 'gym':x.has_gym(), 'ski_in_out':x.ski_in_out(), 
    'num_beds':x.num_beds(), 'num_bedrooms':x.num_bedrooms(), 'num_bathrooms':x.num_bathrooms(),
    'average_rating':x.get_rating(), 'num_reviews':x.num_reviews()}
    places_coll.append(place_frame)

places_df = DataFrame(places_coll)
places_df.index = places_df.index.rename('id')

print('\n# of places:')
print(places_df.count()['sojournID'])

places_df.to_sql('places', con = sojourn_engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema) 

print(f'\n# of geolocate calls: {_m.num_geolocate_calls}')
print(f"Preprocessing elapsed time: {datetime.datetime.now() - start_time}")

# # **TAXES
print('\nnextpax preprocess: UPDATING sojourn_db.taxes')
fieldschema={"sojournID": String(50), "tax_type": Text, "percentage":  Numeric(11,8), "included": 
    Boolean, 'tax_number':Integer}

taxes_df = pd.DataFrame([('NP' + x.HouseID, x.tax_type.Description, x.Percentage, x.Included, x.TaxNumber) for x in 
    tax.query.all()], columns=('sojournID', 'tax_type', 'percentage', 'included','tax_number'))

taxes_df.index = taxes_df.index.rename('id')

print('\n# of taxes:')
print(taxes_df.count()['sojournID'])
print(f"Preprocessing elapsed time: {datetime.datetime.now() - start_time}")

taxes_df.to_sql('taxes', con = sojourn_engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema) 

# # **CALENDARS
print('\nnextpax preprocess: UPDATING sojourn_db.calendars')
fieldschema={"sojournID": String(50), 'calendar_date': Date, 'quantity': Integer, 'arrival_allowed': 
    Boolean, 'departure_allowed': Boolean, 'minimum_stay': Integer, 'maximum_stay': Integer,'calendar_number':Integer}

calendars_df = pd.DataFrame([('NP' + x.HouseID, x.calendarDate, x.quantity, x.arrivalAllowed, x.departureAllowed, 
    x.minimumStay, x.maximumStay, x.CalendarNumber) for x in calendar.query.all()], columns=('sojournID', 'calendar_date', 'quantity', 
    'arrival_allowed', 'departure_allowed', 'minimum_stay', 'maximum_stay','calendar_number'))

print('\n# of calendars records:')
print(calendars_df.count()['sojournID'])
print(f"Preprocessing elapsed time: {datetime.datetime.now() - start_time}")

calendars_df.index = calendars_df.index.rename('id')

calendars_df.to_sql('calendars', con = sojourn_engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema) 

# # **AMENITIES
print('\nnextpax preprocess: UPDATING sojourn_db.amenities')
fieldschema={"sojournID": String(50), 'property_code': String(255), 'amenity_value': String(50),'amenity_number':Integer}

amenities_df = pd.DataFrame([('NP' + x.HouseID, x.property_code.Description, x.AmenityValue, x.AmenityNumber) for x in amenity.query.all()], 
    columns=('sojournID', 'property_code', 'amenity_value','amenity_number'))

print('\n# of amenities:')
print(amenities_df.count()['sojournID'])
print(f"Preprocessing elapsed time: {datetime.datetime.now() - start_time}")

amenities_df.index = amenities_df.index.rename('id')

amenities_df.to_sql('amenities', con = sojourn_engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema) 

# # **ADDITIONAL COSTS
print('\nnextpax preprocess: UPDATING sojourn_db.additional_costs')
fieldschema={"sojournID": String(50), 'cost_code': String(50), 'cost_type': 
    String(50), 'cost_currency': String(50), 'cost_amount': Numeric(10,2), 'amount_type': String(50), 'cost_number': Integer, 
    'from_date': Date, 'until_date': Date}

additional_costs_df = pd.DataFrame([('NP' + x.HouseID, '' if x.cost_code is None else x.cost_code.Description, 
    '' if x.cost_type is None else x.cost_type.Description, x.CostCurrency, x.CostAmount, 
    '' if x.amount_type is None else x.amount_type.Description, x.CostNumber, x.FromDate, x.UntilDate) 
    for x in additional_cost.query.all()], columns=('sojournID', 'cost_code', 'cost_type', 'cost_currency', 'cost_amount', 
    'amount_type', 'cost_number', 'from_date', 'until_date'))

additional_costs_df.index = additional_costs_df.index.rename('id')

print('\n# of additional costs:')
print(additional_costs_df.count()['sojournID'])
print(f"Preprocessing elapsed time: {datetime.datetime.now() - start_time}")

additional_costs_df.to_sql('additional_costs', con = sojourn_engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema) 

# # **DESCRIPTIONS
print('\nnextpax preprocess: UPDATING sojourn_db.descriptions')
fieldschema={"sojournID": String(50), 'language_code': String(5), 'description': UnicodeText, 
'description_type': Text, 'description_number': Integer}

descriptions_df = pd.DataFrame([('NP' + x.HouseID, x.LanguageCode, x.Description, x.description_type.Description,
x.DescriptionNumber) for x in description.query.all()], columns=('sojournID', 'language_code', 'description', 
'description_type','description_number'))

descriptions_df.index = descriptions_df.index.rename('id')

print('\n# of descriptions:')
print(descriptions_df.count()['sojournID'])
print(f"Preprocessing elapsed time: {datetime.datetime.now() - start_time}")

descriptions_df.to_sql('descriptions', con = sojourn_engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema) 

# # # ** PICTURES
print('\nnextpax preprocess: UPDATING sojourn_db.pictures')
fieldschema={"sojournID": String(50), 'file_url': Text, 'picture_type': Text, 'picture_number': Integer}

pictures_df = pd.DataFrame([('NP' + x.HouseID, x.FileUrl, ('' if x.picture_type is None else x.picture_type.Description),
x.PictureNumber) for x in picture.query.all()], 
    columns=('sojournID', 'file_url', 'picture_type', 'picture_number'))

pictures_df.index = pictures_df.index.rename('id')

print('\n# of pictures:')
print(pictures_df.count()['sojournID'])
print(f"Preprocessing elapsed time: {datetime.datetime.now() - start_time}")

pictures_df.to_sql('pictures', con = sojourn_engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema) 

# # **RATES
print('\nnextpax preprocess: UPDATING sojourn_db.rates')
fieldschema={"sojournID": String(50), "from_date":  Date, "till_date": Date, 'duration': Integer, 
    'amount': Numeric(10,2), 'currency': String(50), 'persons': String(50), 'weekdays': String(50), 'minimum_stay': Integer, 
    'maximum_stay': Integer, 'extra_person_fee_amount': Numeric(10,2), 'extra_person_fee_currency': String(50),
    'rate_number': Integer}

rates_df = pd.DataFrame([('NP' + x.HouseID, x.fromDate, x.tillDate, x.duration, x.amount, x.currency, 
    x.persons, x.weekdays, x.minimumStay, x.maximumStay, x.extraPersonFeeAmount, x.extraPersonFeeCurrency,
    x.RateNumber) for x in 
    rate.query.all()], columns=('sojournID', 'from_date', 'till_date', 'duration', 'amount', 'currency', 
    'persons', 'weekdays', 'minimum_stay', 'maximum_stay', 'extra_person_fee_amount', 'extra_person_fee_currency',
    'rate_number'))


rates_df.index = rates_df.index.rename('id')

print('\n# of rates:')
print(rates_df.count()['sojournID'])
print(f"Preprocessing elapsed time: {datetime.datetime.now() - start_time}")

rates_df.to_sql('rates', con = sojourn_engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema)


# # # ** COST CODE
print('\nnextpax preprocess: UPDATING sojourn_db.NP_CostCode')
fieldschema={"CostCode": String(50), 'Description': Text}

cost_code_df = pd.DataFrame([(x.CostCode, x.Description) for x in cost_code.query.all()], 
    columns=('CostCode', 'Description'))

cost_code_df.index = cost_code_df.index.rename('id')

print('\n# of CostCodes:')
print(cost_code_df.count()['CostCode'])
print(f"Preprocessing elapsed time: {datetime.datetime.now() - start_time}")

cost_code_df.to_sql('NP_CostCode', con = sojourn_engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema) 

# # # ** TAX TYPE
print('\nnextpax preprocess: UPDATING sojourn_db.NP_TaxType')
fieldschema={"TaxType": String(50), 'Description': Text}

tax_type_df = pd.DataFrame([(x.TaxType, x.Description) for x in tax_type.query.all()], 
    columns=('TaxType', 'Description'))

tax_type_df.index = tax_type_df.index.rename('id')

print('\n# of TaxTypes:')
print(tax_type_df.count()['TaxType'])
print(f"Preprocessing elapsed time: {datetime.datetime.now() - start_time}")

tax_type_df.to_sql('NP_TaxType', con = sojourn_engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema) 

# brands -> managers
print('\nnextpax preprocess: UPDATING sojourn_db.managers')
fieldschema = {'pm_id': String(50),'pm_name': String(100), 'pm_short_name': String(100),
'PMS': String(50),'channel_mgr': String(50), 'main_contact': String(100),
'phone': String(100), 'email': String(150), 'location': String(100),
'discount': Numeric(9,5), 'payment': String(25), 'net_rate': Numeric(9,5),
'base_markup': Numeric(9,5), 'addtl_markups': Boolean}

managers_df = pd.DataFrame([(x.BrandID, x.PMC, x.NP_Name, x.PMS, 'NextPax', x.MainContact,
x.Phone, x.Email, x.Location, x.Discount, x.Payment, x.NetRate, x.BaseMarkup, x.AddtlMarkups) 
for x in brand.query.filter(brand.PMS.isnot(None)).all()], 
columns=('pm_id', 'pm_name', 'pm_short_name', 'PMS', 'channel_mgr', 'main_contact',
'phone', 'email', 'location', 'discount', 'payment', 'net_rate', 'base_markup', 'addtl_markups'))

managers_df.index = managers_df.index.rename('id')

print('\n# of Managers:')
print(managers_df.count()['pm_id'])
print(f"Preprocessing elapsed time: {datetime.datetime.now() - start_time}")
managers_df.to_sql('managers', con = sojourn_engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema)

# reviews
print('\nnextpax preprocess: UPDATING sojourn_db.reviews')
fieldschema={'sojournID': String(50), 'rating': SmallInteger, 
'review_summary': String(100), 'review_detail': UnicodeText, 'reviewed_by': String(50)}

reviews_df = pd.DataFrame([('NP' + x.HouseID, x.rating, x.review_summary, x.review_detail, x.reviewed_by) for x in 
    review.query.all()], columns=('sojournID', 'rating', 'review_summary','review_detail','reviewed_by'))

reviews_df.index = reviews_df.index.rename('id')

print('\n# of Reviews:')
print(reviews_df.count()['sojournID'])
print(f"Preprocessing elapsed time: {datetime.datetime.now() - start_time}")
reviews_df.to_sql('reviews', con = sojourn_engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema)
