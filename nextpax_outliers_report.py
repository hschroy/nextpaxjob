import pymysql 
import pandas as pd
from email_warn import send_warning
import datetime
from datetime import date
from tabulate import tabulate

print('\n**** NEXTPAX OUTLIERS REPORT... ****')
print('\nNextpax outliers report...')
check_date = datetime.datetime.now().strftime("%Y-%m-%d")
print(check_date)

conn = pymysql.connect(host='35.192.116.102', user='henry', passwd='finley', db='nextpax', port=3306)

cur = conn.cursor()
cur.execute('USE nextpax')

def do_table(tablename):
    # **PLACES
    print(f'\n**** checking {tablename} ****')
    print(f'houses in {tablename} but not in houses')
    sql_string = f'SELECT DISTINCT HouseID FROM nextpax.{tablename} WHERE HouseID NOT IN (SELECT DISTINCT HouseID FROM nextpax.houses)'
    print(sql_string)
    num_rows = cur.execute(sql_string)
    print(f'{num_rows} row(s) returned')
    if num_rows > 0:
        house_ids = pd.read_sql(sql_string,conn)
        for index, row in house_ids.iterrows(): 
            sql_string=f'''
            INSERT INTO nextpax.outliers(checked_date, HouseID, {tablename})
            SELECT ol.* FROM
            (SELECT '{check_date}' AS checked_date, '{row.HouseID}' AS HouseID, 1 AS {tablename}) AS ol
            ON DUPLICATE KEY UPDATE checked_date=ol.checked_date, HouseID=ol.HouseID, {tablename}=ol.{tablename}
            ''' 
            cur.execute(sql_string)
        conn.commit()

    print(f'houses in houses but not in {tablename}')
    sql_string = f'SELECT DISTINCT HouseID FROM nextpax.houses WHERE Partner != "demo" AND HouseID NOT IN (SELECT DISTINCT HouseID FROM nextpax.{tablename})'
    print(sql_string)
    num_rows = cur.execute(sql_string)
    print(f'{num_rows} row(s) returned')
    if num_rows > 0:
        house_ids = pd.read_sql(sql_string,conn)
        for index, row in house_ids.iterrows(): 
            sql_string=f'''
            INSERT INTO nextpax.outliers(checked_date, HouseID, houses)
            SELECT ol.* FROM
            (SELECT '{check_date}' AS checked_date, '{row.HouseID}' AS HouseID, 1 AS houses) AS ol
            ON DUPLICATE KEY UPDATE checked_date=ol.checked_date, HouseID=ol.HouseID, houses=ol.houses
            ''' 
            cur.execute(sql_string)
        conn.commit()


# dependent_tables = ['additional_costs','amenities','calendars','descriptions','pictures','rates','taxes']
dependent_tables = ['amenities','calendars','descriptions','pictures','rates']

for table_name in dependent_tables:
    do_table(table_name)

print(f"\n**** removing any outlier houses from houses table: ****")

sql_string = '''ALTER TABLE `nextpax`.`houses` 
CHANGE COLUMN `HouseID` `HouseID` VARCHAR(50) NOT NULL ,
ADD PRIMARY KEY (`HouseID`)'''
print('\n' + sql_string)
try:
    exec_result = cur.execute(sql_string)
    print(f'\n{exec_result} row(s) affected...')
    conn.commit()
except pymysql.Error as Error:
    msg = Error.args[1]
    # send_warning(msg)
    print('\n' + msg)
    print('\n** (this routine has already removed outlier houses earlier today) **')

sql_string = f'''SELECT HouseID FROM nextpax.outliers WHERE checked_date = '{check_date}' AND houses > 0 AND HouseID IN (SELECT HouseID FROM nextpax.houses)'''
print('\n' + sql_string)
num_rows = cur.execute(sql_string)
print(f'{num_rows} row(s) returned')
if num_rows > 0:
    house_ids = pd.read_sql(sql_string,conn)
    for index, row in house_ids.iterrows(): 
        sql_string=f'''DELETE FROM nextpax.houses WHERE HouseID = '{row.HouseID}'
        ''' 
        print('\n' + sql_string)
        exec_result = cur.execute(sql_string)
        print(f'\n{exec_result} row(s) affected...')

    conn.commit()


print('\n*** run outliers again, this fills out summary table below... ***')

# now, check additional_costs and taxes as well...
# dependent_tables = ['additional_costs','amenities','calendars','descriptions','pictures','rates','taxes']

for table_name in dependent_tables:
    do_table(table_name)


print(f"\n**** today's outliers (x's indicate HouseID exists in table): ****")
# field_list = '''checked_date, HouseID, IF(houses,'x','') AS houses, IF(additional_costs,'x','') AS additional_costs, 
# IF(amenities,'x','') AS amenities, IF(calendars,'x','') AS calendars, IF(descriptions,'x','') AS descriptions, 
# IF(pictures,'x','') AS pictures, IF(rates,'x','') AS rates, IF(taxes,'x','') AS taxes'''
# we'll allow houses through if they don't have additional_costs... OR taxes 
# For the other tables, such rates or descriptions, we'll exclude them for now since the data is incomplete.

field_list = '''checked_date, HouseID, IF(houses,'x','') AS houses, 
IF(amenities,'x','') AS amenities, IF(calendars,'x','') AS calendars, IF(descriptions,'x','') AS descriptions, 
IF(pictures,'x','') AS pictures, IF(rates,'x','') AS rates'''
#, IF(taxes,'x','') AS taxes'''

sql_string = f"SELECT {field_list} FROM nextpax.outliers WHERE checked_date = '{check_date}'"
todays_house_ids = pd.read_sql(sql_string,conn)
# summary_fields = ['checked_date','HouseID','houses','additional_costs','amenities','calendars','descriptions','pictures','rates','taxes']
summary_fields = ['checked_date','HouseID','houses','amenities','calendars','descriptions','pictures','rates','taxes']
print("<pre><code>")
#print(tabulate(house_df[house_df.index % int(len(house_df)*0.033) == 0][summary_fields], headers=summary_fields, showindex=False, tablefmt="html"))
print(tabulate(todays_house_ids, headers=summary_fields, showindex=False))
print("</code></pre>")





cur.close()
conn.close()

cur = None

