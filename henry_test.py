import pymysql 
import pandas as pd
from enum import Enum
from email_warn import send_warning
import datetime
from geopy.geocoders import GoogleV3
import time
from time import time

from dotenv import load_dotenv
import os
load_dotenv()
SOJOURN_HOST = os.getenv('SOJOURN_HOST')
SOJOURN_USER = os.getenv('SOJOURN_USER')
SOJOURN_PWD = os.getenv('SOJOURN_PWD')
GEO_GOOG_API = os.getenv('GEO_GOOG_API')

geolocator = GoogleV3(api_key=GEO_GOOG_API, domain='maps.googleapis.com')

script_start = datetime.datetime.now()
import_date = datetime.datetime.now().strftime("%Y-%m-%d")

conn = pymysql.connect(host=SOJOURN_HOST, user=SOJOURN_USER, passwd=SOJOURN_PWD, db='sojourn', port=3306)

cur = conn.cursor()
cur.execute('USE sojourn')

class module_object:
    pass
# create search_area variable global to this module
_m = module_object()
_m.num_geolocate_calls = 0
_m.continue_run = True


# sql_string = f"""select a.* from
#     (SELECT count(sojournID) as unit_count, pm_name from bkgpal_evolve.places 
#     where sojournID in (select sojournID from bkgpal_evolve.listing_status where activate = 1)
#     group by pm_name) a
#     where unit_count > 4
#     order by 1 desc"""

# cur.execute(sql_string)
# result = cur.fetchall()
# if not result is None:
#     for pm in result:
#         unit_cnt = 1
#         msg = f"""PM unit_count: {pm[0]} | pm_name: {pm[1]} \n\n"""
#         print(msg)



# now add hash_tag aliases
sql_string = f"""select * from sojourn.hash_alias;"""
print('\nAdd hash_tag aliases...')
exec_result = cur.execute(sql_string)
result = cur.fetchall()
if not result is None:
    for alias in result:
        msg = f"""hash_tag: #{alias[0]} | alias: #{alias[1]}"""
        print(msg)
        sql_command = f"""set sql_safe_updates = 0;"""
        exec_result = cur.execute(sql_command)

        # sql_command = f"""update sojourn.places set meta_tags = concat(meta_tags,'|{alias[1]}') WHERE meta_tags like '%{alias[0]}%';"""
        sql_command =f"""update sojourn.places set meta_tags = concat(meta_tags,'|{alias[1]}') WHERE meta_tags like '%{alias[0]}%' and meta_tags not like '%|{alias[1]}%';"""
        print('\n' + sql_command)
        tic = time()
        exec_result = cur.execute(sql_command)
        conn.commit()
        toc = time()
        print(f"\nExecution time: {toc-tic:.3f} seconds.")
        print(f'{exec_result} row(s) affected...')


