import pymysql 
import pandas as pd
from email_warn import send_warning
import datetime
from geopy.geocoders import GoogleV3
import time

geolocator = GoogleV3(api_key="AIzaSyA_gzlbNDzI-CfN0rvNlpL9UFv1vkLVGjY",
     domain='maps.googleapis.com')

script_start = datetime.datetime.now()

conn = pymysql.connect(host='104.197.116.86', user='henry', passwd='finley', db='sojourn', port=3306)

cur = conn.cursor()
cur.execute('USE sojourn')
print(f"\n\nEVOLVE calendar preprocess elapsed time: {datetime.datetime.now() - script_start}")

sql_string = f"""SELECT sojournID, calendar_date, arrival_allowed FROM sandbox.calendars
order by 1, 2 desc"""
print(sql_string)
exec_result = cur.execute(sql_string)
print(f'\n{exec_result} row(s) returned...')
print(f"\nEVOLVE calendar preprocess elapsed time: {datetime.datetime.now() - script_start}")

result = cur.fetchall()
if not result is None:
    print('\nchecking availability matrix....')
    place_count = 0
    this_place = ''
    last_place = this_place
    for place_date in result:
        this_date = place_date[1]
        this_allowed = place_date[2]
        this_place = place_date[0]
        if this_place != last_place:
            # new sojournID
            place_count += 1
            last_place = this_place
            days_avail = 0

        if this_allowed:
            days_avail += 1
        else:
            days_avail = 0


        sql_string = f"""update sandbox.calendars set days_avail = {days_avail} where sojournID = '{this_place}' and calendar_date = '{this_date}'"""
        # print(f'\n{sql_string}')
        print(sql_string)
        exec_result = cur.execute(sql_string)
        # print(f'\n{exec_result} row(s) affected...')
        conn.commit()


        print(f"\nEVOLVE calendar preprocess elapsed time: {datetime.datetime.now() - script_start}")

        pass
