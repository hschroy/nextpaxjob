#!/bin/bash
PATH=$PATH:/home/henry_schroy/google-cloud-sdk/bin
cd ~/daily/nextpaxjob;
# source ~/./sendgrid.env;
rm nextpaxjob.log;
#time bash morningjob.sh >> morning.log
(time bash nextpaxjob.sh) > nextpaxjob.log 2>&1
source "/home/henry_schroy/venv/bin/activate"
cd ~/daily/nextpaxjob;
python send_email.py;

#repeat steps for evolve-sojourn db
#this should be wrapped into main nextpax process
#the only difference is the sojourn db ip address
#python nextpax_preprocess.v2evolve.py;
#python nextpax_postprocess.v2evolve.py;
#python nextpax_sojourn_upsert.v2evolve.py;
#echo 'restarting docker container sojournapi on sojournapi-dev2:'
#gcloud beta compute ssh --zone "us-central1-a" "sojournapi-dev2"  --project "sojourn-api-development" --command="docker restart sojournapi"

