import pymysql 
import pandas as pd
from email_warn import send_warning
import datetime
import time
import re

script_start = datetime.datetime.now()

search_rooms = ['Master Bedroom:',
    'Master Bedroom 1:',
    '| Master Bedroom 2:',
    '\nBedroom:','\nBedroom 1',
    '| Living Room:',
    '| Bedroom 2',
    '| Bedroom 3',
    '| Bedroom 4',
    '| Bedroom 5',
    '| Bedroom 6',
    '| Bedroom 7',
    '| Bedroom 8',
    '| Bedroom 9',
    '| Bedroom 10',
    '| Bedroom 11',
    '| Bedroom 12',
    '| Hallway Sleeping Nook:',
    '| Additional Sleeping:',
    '| Great Room:',
    '| Loft:','| Media Room:'
    ]

def sql_safe_str(value):
    if value is not None:
        sql_safe_str = value.replace("'","''").replace('â€\x9d','”').strip()
        return f"'{sql_safe_str}'"
    else:
        return 'null'

conn = pymysql.connect(host='104.197.116.86', user='henry', passwd='finley', db='sojourn', port=3306)

cur = conn.cursor()
cur.execute('USE sojourn')
print(f"\n\nEVOLVE spaces preprocess elapsed time: {datetime.datetime.now() - script_start}")

cur.execute('set sql_safe_updates = 0;')
sql_string = """ delete FROM sojourn.spaces where sojournID like 'FX%';"""
print(sql_string)
exec_result = cur.execute(sql_string)
print(f'\n{exec_result} row(s) affected...')
print(f"\nEVOLVE spaces preprocess elapsed time: {datetime.datetime.now() - script_start}")
conn.commit()


sql_string = f"""SELECT sojournID, description FROM evolve.descriptions where description_type = 'space'"""
print(sql_string)
exec_result = cur.execute(sql_string)
print(f'\n{exec_result} row(s) returned...')
print(f"\nEVOLVE spaces preprocess elapsed time: {datetime.datetime.now() - script_start}")

result = cur.fetchall()
if not result is None:
    print('\nchecking spaces matrix....')
    place_count = 0
    this_place = ''
    last_place = this_place
    for place_desc in result:
        desc = place_desc[1]
        this_place = place_desc[0]
        if this_place != last_place:
            # new sojournID
            place_count += 1
            outliers = 0
            last_place = this_place

        for search_room in search_rooms:
            room = ''
            found_one = desc.find(search_room)
            if found_one > -1:
                if desc[found_one:].find('|') == 0:
                    found_one += 1
                elif desc[found_one:].find('\n') == 0:
                    found_one += 1

                if desc[found_one:].find('|') > -1:
                    room = desc[found_one:][:desc[found_one:].find('|')]
                elif desc[found_one:].find('\n') > -1:
                    room = desc[found_one:][:desc[found_one:].find('\n')]

            if room != '':
                try:
                    room_name, room_contents = re.split(':',room)

                except:
                    outliers += 1
                    room_name = f'outlier {outliers}'
                    room_contents = room

                if this_place == 'FX417362':
                    print('stop')

                room_name = room_name[:100]
                room_contents = room_contents[:100]
                
                try:
                    sql_string = f"""insert into sojourn.spaces (sojournID, room_name, room_contents) 
                        select nr.* from
                        (SELECT '{this_place}' as sojournID, {sql_safe_str(room_name.strip())} as room_name, {sql_safe_str(room_contents.strip())} as room_contents) as nr
                        on duplicate key update sojournID=nr.sojournID, room_name=nr.room_name, room_contents=nr.room_contents;"""
                    print(sql_string)
                    exec_result = cur.execute(sql_string)
                    print(f'\n{exec_result} row(s) affected...')
                    conn.commit()
                except:
                    print('stop')


        print(f"\nEVOLVE spaces preprocess elapsed time: {datetime.datetime.now() - script_start}")

