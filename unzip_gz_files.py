import os
import gzip 
import shutil 


abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
# change working directory to same as current python file
os.chdir(dname) 

os.chdir('Xml')

files = []
files = os.listdir()

for file in files:
    with gzip.open(file, 'rb') as f_in:
        with open(file.replace('.gz',''), 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)
            f_in.close()
            f_out.close()
            os.remove(file)

print('\nunzipping files: ')
print(files)
print('\n')