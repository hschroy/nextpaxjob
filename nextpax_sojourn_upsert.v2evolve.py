import pymysql 
import pandas as pd
from email_warn import send_warning

import datetime
from dotenv import load_dotenv
import os
load_dotenv()

SOJOURN_HOST = os.getenv('SOJOURN_HOST')
SOJOURN_USER = os.getenv('SOJOURN_USER')
SOJOURN_PWD = os.getenv('SOJOURN_PWD')

script_start = datetime.datetime.now()

conn = pymysql.connect(host=SOJOURN_HOST, user=SOJOURN_USER, passwd=SOJOURN_PWD, db='sojourn', port=3306, charset='utf8mb4')

cur = conn.cursor()
cur.execute('USE sojourn')

class module_object:
    pass

_m = module_object()

_m.limit_exceeded = ''

warn_list = []
warn_limit = 99

def add_warning(msg):
    if len(warn_list)+1 > warn_limit:
        _m.limit_exceeded = f"\n(Warning Limit Exceeded.)\n"
    else:
        warn_list.append(f"\nWarning No. {len(warn_list)+1}\n{msg}")


def do_table(databasename, tablename, command_list):
    # **PLACES
    print(f'\n**** upserting from preprocess:  sojourn_db.{tablename} --> {databasename}.{tablename} ****')
    sql_string = f'select count(id) AS num_records from sojourn_db.{tablename}'
    num_rows = 0

    try:
        # see if it exists, then drop it
        cur.execute(sql_string)
        # sojourn_db.{tablename} exists
        num_rows = cur._rows[0][0]
    except pymysql.Error as Error:
        msg = f"sojourn_db.{tablename} doesn't exist."
        add_warning(msg)
        print('\n' + msg)

    if num_rows > 0:
        # we have {tablename}, let's update main sojourn database
        print(f'\nupsert into {databasename}.{tablename}, {num_rows} rows:')
        try:
            for sql_command in command_list:
                print('\n' + sql_command)
                exec_result = cur.execute(sql_command)
                print(f'\n{exec_result} row(s) affected...')
                conn.commit()
        except pymysql.Error as Error:
            msg = f'{sql_command}\n\n{Error.args[1]}'
            add_warning(msg)
            print('\n' + msg)

    else:
        msg = f"no records in sojourn_db.{tablename}, skipping this {databasename} database table update..."
        add_warning(msg)
        print('\n' + msg)

#print('\n ****** upsert into two databases, the sojourn api database and the sojourn_django website database ... ******')
# db_name_list = ['sojourn','sojourn_django']
print('\n ****** upsert into the sojourn api database ... ******')
db_name_list = ['sojourn']
for db_name in db_name_list:

    print(f'\n**** NEXTPAX UPSERT into {db_name.upper()} production database... ****')

    tables = {'places':[
        f"SET SQL_SAFE_UPDATES = 0;",
        f'''
        DELETE from {db_name}.places WHERE sojournID LIKE "OR%";
        '''
        ,
        f'''
        INSERT INTO `{db_name}`.`places`(sojournID, PMS, pm_id, property_name, country, city, state, street,
        place, zip_code, min_persons, max_persons, 
        min_children, number_of_pets, min_stay, house_type, latitude, longitude, location, picture, currency, rate, wifi, 
        iron, air_conditioning, hot_tub, pool, washer, free_parking, TV, dryer, breakfast, hangers,
        hair_dryer, high_chair, smoke_alarm, pets_allowed, kitchen, heating, fireplace, laptop_friendly,
        crib, carbon_monoxide_alarm, gym, ski_in_out, num_beds, num_bedrooms, num_bathrooms, average_rating,
        num_reviews, last_updated) SELECT pl.* FROM
        (SELECT sojournID, PMS, pm_id, property_name, country, city, state, street, 
        place, zip_code, min_persons, max_persons, min_children, 
        number_of_pets, min_stay, house_type, latitude, longitude, location, picture, currency, rate, wifi, 
        iron, air_conditioning, hot_tub, pool, washer, free_parking, TV, dryer, breakfast, hangers,
        hair_dryer, high_chair, smoke_alarm, pets_allowed, kitchen, heating, fireplace, laptop_friendly,
        crib, carbon_monoxide_alarm, gym, ski_in_out, num_beds, num_bedrooms, num_bathrooms, average_rating,
        num_reviews, '{script_start}' FROM `sojourn_db`.`places`) AS pl
        ''']
        ,'NP_CostCode':[
        f'''
        DELETE from {db_name}.NP_CostCode;
        '''
        ,
        f'''
        INSERT INTO {db_name}.NP_CostCode (select * from sojourn_db.NP_CostCode);
        '''
        ]
        ,'NP_CostType':[
        f'''
        DELETE from {db_name}.NP_CostType;
        '''
        ,
        f'''
        INSERT INTO {db_name}.NP_CostType (select * from sojourn_db.NP_CostType);
        '''
        ]
        ,'NP_AmountType':[
        f'''
        DELETE from {db_name}.NP_AmountType;
        '''
        ,
        f'''
        INSERT INTO {db_name}.NP_AmountType (select * from sojourn_db.NP_AmountType);
        '''
        ]
        ,'NP_TaxType':[
        f'''
        DELETE from {db_name}.NP_TaxType;
        '''
        ,
        f'''
        INSERT INTO {db_name}.NP_TaxType (select * from sojourn_db.NP_TaxType);
        '''
        ]
        ,'managers':[
        f'''
        DELETE from {db_name}.managers WHERE channel_mgr LIKE "NextPax%";
        ''',
        f'''
        INSERT INTO {db_name}.managers(pm_id, pm_name, pm_short_name, PMS, 
        channel_mgr, main_contact, phone, email, location, discount, payment,
        net_rate, base_markup, addtl_markups, bundle_fee_tax)
        SELECT mg.* FROM
        (SELECT pm_id, pm_name, pm_short_name, PMS, 
        channel_mgr, main_contact, phone, email, location, discount, payment,
        net_rate, base_markup, addtl_markups, bundle_fee_tax FROM sojourn_db.managers) AS mg
        '''
        ]
        ,'reviews':[
            f'''DELETE from {db_name}.reviews WHERE  sojournID LIKE "OR%";''',
            f'''INSERT INTO {db_name}.reviews(sojournID, rating, review_summary,
            review_detail, reviewed_by)
            SELECT rv.* FROM
            (SELECT sojournID, rating, review_summary,
            review_detail, reviewed_by FROM sojourn_db.reviews) AS rv
            '''
        ]
        ,'additional_costs':[
        f'''
        DELETE from {db_name}.additional_costs WHERE sojournID LIKE "OR%";
        '''
        ,
        f'''
        INSERT INTO {db_name}.additional_costs(id, sojournID, cost_code, cost_type, cost_currency, 
        cost_amount, amount_type, cost_number, from_date, until_date)
        SELECT ac.* FROM
        (SELECT concat(sojournID,'.',cost_number) as id, sojournID, cost_code, cost_type, cost_currency, cost_amount, amount_type, cost_number, 
        from_date, until_date FROM sojourn_db.additional_costs) AS ac
        ''']
        ,'amenities':[
        f'''
        DELETE from {db_name}.amenities WHERE sojournID LIKE "OR%";
        '''
        ,
        f'''
        INSERT INTO {db_name}.amenities(id, sojournID, property_code, amenity_value)
        SELECT am.* FROM
        (SELECT concat(sojournID,'.',amenity_number) as id, sojournID, property_code, amenity_value FROM sojourn_db.amenities) as am
        ''']
        ,'calendars':[
        f'''
        DELETE from {db_name}.calendars WHERE sojournID LIKE "OR%";
        '''
        ,
        f'''
        INSERT INTO {db_name}.calendars(id, sojournID, calendar_date, quantity, arrival_allowed, departure_allowed, 
        minimum_stay, maximum_stay, days_available)
        SELECT cs.* FROM
        (SELECT concat(sojournID,'.',calendar_number) as id, sojournID, calendar_date, quantity, arrival_allowed, departure_allowed, 
        minimum_stay, maximum_stay, days_available FROM sojourn_db.calendars) as cs
        ''']
        ,'descriptions':[
        f'''
        DELETE from {db_name}.descriptions WHERE sojournID LIKE "OR%";
        '''
        ,
        f'''
        INSERT INTO {db_name}.descriptions(id, sojournID, language_code, description, description_type)
        SELECT ds.* FROM
        (SELECT concat(sojournID,'.',description_number) as id, sojournID, language_code, description, description_type FROM sojourn_db.descriptions) AS ds
        ''']
        ,'pictures':[
        f'''
        DELETE from {db_name}.pictures WHERE sojournID LIKE "OR%";
        '''
        ,
        f'''
        INSERT INTO {db_name}.pictures(id, sojournID, file_url, picture_type)
        SELECT ps.* FROM
        (SELECT concat(sojournID,'.',picture_number) as id, sojournID, file_url, picture_type FROM sojourn_db.pictures) AS ps
        ''' ]
        ,'rates':[
        f'''
        DELETE from {db_name}.rates WHERE sojournID LIKE "OR%";
        '''
        ,
        f'''
        INSERT INTO {db_name}.rates(id, sojournID, from_date, till_date, duration, amount, currency, persons, 
        weekdays, minimum_stay, maximum_stay, extra_person_fee_amount, extra_person_fee_currency)
        SELECT rs.* FROM
        (SELECT concat(sojournID,'.',rate_number) as id, sojournID, from_date, till_date, duration, amount, currency, persons, 
        weekdays, minimum_stay, maximum_stay, extra_person_fee_amount, extra_person_fee_currency FROM sojourn_db.rates) as rs
        ''']
        ,'taxes':[
        f'''
        DELETE from {db_name}.taxes WHERE sojournID LIKE "OR%";
        '''
        ,
        f'''
        INSERT INTO {db_name}.taxes(id, sojournID, tax_type, percentage, included)
        SELECT tx.* FROM
        (SELECT concat(sojournID,'.',tax_number) as id, sojournID, tax_type, percentage, included FROM sojourn_db.taxes) AS tx
        ''']
    }

    # ie., to run one table only: 
    # do_table(db_name, 'pictures',tables['pictures'])

    [do_table(db_name, x, tables[x]) for x in tables]

sql_command = f'''insert into sojourn.geo_cache(sojournID, country, city, state, street, place, zip_code, latitude, longitude, location)
        select pl.* from
        (select sojournID, country, city, state, street, place, zip_code, latitude, longitude, location from sojourn_db.places) as pl
        on duplicate key update sojournID=pl.sojournID, country=pl.country, city=pl.city, state=pl.state, street=pl.street,
        place=pl.place, zip_code=pl.zip_code, latitude=pl.latitude, longitude=pl.longitude;
        '''
print('\n**** upserting from preprocess:  sojourn_db.places --> sojourn.geo_cache ****')
print('\n' + sql_command)
exec_result = cur.execute(sql_command)
print(f'\n{exec_result} row(s) affected...')
conn.commit()



cur.close()
conn.close()

cur = None

print(f"\nNEXTPAX sojourn-upsert elapsed time: {datetime.datetime.now() - script_start}")



if len(warn_list) > 0:
    message = f'\nnextpax_sojourn_upsert.v2evolve.py\n\n{len(warn_list)} Warnings...\n{_m.limit_exceeded}'+'\n'.join(warn_list)
    print(message)
    send_warning(message)
else:
    print('\nNo Warnings...')
