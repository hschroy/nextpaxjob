import xml.etree.ElementTree as ET
from datetime import datetime
from pandas import Series, DataFrame
import pandas as pd
import csv
import os
from sqlalchemy import create_engine
import pymysql 
from sqlalchemy.types import Integer, Text, String, Numeric, DateTime, Boolean, Date
from tabulate import tabulate
import random
from np_constants import pms_list

start_time = datetime.now()

engine = create_engine("mysql+pymysql://{user}:{passwd}@{host}/{db}".format(host='35.192.116.102', user='henry', passwd='finley', db='nextpax', port=3306))

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname) 
# change working directory to same as current python file
# if local machine
dname = 'Xml'
os.chdir(dname) 
###############################################################
amenities = []

for pm_code in pms_list:
    xml_filename = 'paxgenerator_house_properties_{pms}.xml'.format(pms=pm_code)
    xmlp = ET.XMLParser(encoding="utf-8")
    tree = ET.parse(xml_filename,parser=xmlp)
    root = tree.getroot()
    partner = root.find('Partner').text

    for properties in root.iter('Properties'):
        house_id = properties.find('HouseID').text
        for proprty in properties.iter('Property'):
            # add proprty attributes to rate
            proprty.attrib['Partner'] = partner
            proprty.attrib['HouseID'] = house_id
            i = 0
            while i < len(proprty):
                proprty.attrib[proprty[i].tag] = proprty[i].text
                i += 1

            amenities.append(proprty.attrib)

    xmlp = None
    tree = None
    root = None
    partner = None


amenities_df = DataFrame(amenities)

amenities_df.index = amenities_df.index.rename('id')
amenities_df = amenities_df.rename(columns={"Value": "AmenityValue"})

fieldschema={
	'Partner': String(50), 'HouseID': String(50), 'PropertyCode': String(50), 'AmenityValue': String(50)
}

# drop duplicates
amenities_df = amenities_df.drop_duplicates()

amenities_df.to_sql('amenities', con = engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema)

print('\n# of Amenities:')
print(amenities_df.count()['PropertyCode'])
print('\n# by Property Mgr:')
# print(amenities_df.groupby('Partner').count()['PropertyCode'])
# print('\nAmenities for ',pms_list,':\n')
# print('\namenities_df')
# print(amenities_df)

print(tabulate(pd.DataFrame(amenities_df.groupby('Partner')['PropertyCode'].count()), headers=['Partner','Count']))

print('\nAmenities for ',pms_list,':')



num_rows = len(amenities_df)
num_samples = 45

random_index = []

i = 0
while i < num_samples:
    random_int = random.randint(0,num_rows-1)
    if random_int not in random_index:
        random_index.append(random_int)
        i += 1

random_index.sort()

print(f'(randomly sampling {num_samples} amenities out of {num_rows}...)\nrow#')

# print(f'(sampling every {int(len(amenities_df)*0.033)} rows...)\n')

summary_fields = ['Partner','HouseID','PropertyCode','AmenityValue']
print("<pre><code>")
print(tabulate(amenities_df.iloc[random_index][summary_fields], 
    headers=summary_fields, showindex=True))
print("</code></pre>")
# print(tabulate(amenities_df[amenities_df.index % int(len(amenities_df)*0.033) == 0][summary_fields], 
#     headers=summary_fields, showindex=False))

end_time = datetime.now()
print('Duration: {}'.format(end_time - start_time))