#!/bin/bash
echo 'starting nextpax extract (v2):'
date
echo
echo

source "/home/henry_schroy/venv/bin/activate"
cd ~/daily/nextpaxjob;
python newapi.v2.py;
python backup_nextpax_dbs.py;
python nextpax_parents.py;
python nextpax_outliers_report.v2.py;
python nextpax_preprocess.v2evolve.py;
python nextpax_post_parents.py;
python nextpax_calendar_preprocessor.py;
python nextpax_postprocess.v2evolve.py;
python nextpax_sojourn_upsert.v2evolve.py;
python update_stars.py;
python useMySql.py;

echo 'ending nextpax extract (v2):'
date
#echo 'restarting docker container sojournapi on sojournapi-20200905:'
#gcloud beta compute ssh --zone "us-central1-a" "sojournapi-20200905"  --project "sojourn-api-development" --command="docker restart sojournapi"

#echo 'restarting docker container sojournapi on sojournapi-dev2:'
#gcloud beta compute ssh --zone "us-central1-a" "sojournapi-dev2"  --project "sojourn-api-development" --command="docker restart sojournapi"
echo
echo
echo 'Batch run time:'



