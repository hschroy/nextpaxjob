import pymysql 
import pandas as pd
import datetime
from tabulate import tabulate

def sql_safe_str(value):
    if value is not None:
        sql_safe_str = value.replace("'","''").strip()
        return f"'{sql_safe_str}'"
    else:
        return 'null'

def safe_str(value):
    if value is not None:
        return f"{value}"
    else:
        return None

def safe_num(value):
    if value is not None:
        return value
    else:
        return None

import_date = datetime.datetime.now().strftime("%Y-%m-%d")
yesterday = (datetime.datetime.now() - datetime.timedelta(days = 1)).strftime("%Y-%m-%d")

# conn = pymysql.connect(host='34.70.178.230', user='henry', passwd='finley', db='sojourn', port=3306)
conn = pymysql.connect(host='104.197.116.86', user='henry', passwd='finley', db='sojourn', port=3306)

cur = conn.cursor()
cur.execute('USE sojourn')


picture_list = [
'https://a0.muscache.com/im/pictures/f963690e-ba9e-4b22-a0ba-f8b3818b0209.jpg?im_w=1200', 
'https://a0.muscache.com/im/pictures/84adb181-56fc-4deb-a933-df86a250e944.jpg?im_w=720', 
'https://a0.muscache.com/im/pictures/196a5a6d-2f86-4caa-8b4a-f6c15bd7e956.jpg?im_w=720', 
'https://a0.muscache.com/im/pictures/fa410976-6f84-45d4-b59f-9f6fcf15f5a4.jpg?im_w=720', 
'https://a0.muscache.com/im/pictures/768cbff5-6dfb-4ece-b808-4f435797f0b3.jpg?im_w=720', 
'https://a0.muscache.com/im/pictures/e12f8577-987f-4fc9-9af3-ff6b30838ae1.jpg?im_w=720', 
'https://a0.muscache.com/im/pictures/f68b4e74-5022-4e2a-be58-2a02238f93b6.jpg?im_w=720', 
'https://a0.muscache.com/im/pictures/81dc9c43-6a67-4d36-b955-b5b2970107da.jpg?im_w=720', 
'https://a0.muscache.com/im/pictures/miso/Hosting-36390811/original/83945b65-1b44-4417-bbeb-2a7a37ac2c5c.jpeg?im_w=720', 
'https://a0.muscache.com/im/pictures/c8833500-1c16-42a6-bc7e-85a49b74a8f1.jpg?im_w=720', 
'https://a0.muscache.com/im/pictures/c15d56bb-f704-4a91-98c8-5d052b242580.jpg?im_w=720', 
'https://a0.muscache.com/im/pictures/87cc9dab-6e04-4601-bedb-48e76e32bfe2.jpg?im_w=720', 
'https://a0.muscache.com/im/pictures/033c126e-3ef3-4ca9-92c2-5766abdf3e67.jpg?im_w=720', 
'https://a0.muscache.com/im/pictures/e24d6fe3-01c6-463a-8971-2da73322e7b3.jpg?im_w=720', 
'https://a0.muscache.com/im/pictures/469db1d2-9ce1-451a-a87e-b91384367b27.jpg?im_w=720', 
'https://a0.muscache.com/im/pictures/2ee4ae68-ca35-455d-8edc-100d10e983f8.jpg?im_w=720', 
'https://a0.muscache.com/im/pictures/4e5b3a50-fcf6-42b5-82b9-66052617d7b9.jpg?im_w=720', 
'https://a0.muscache.com/im/pictures/8492e8f6-e5b5-4bc1-b9b5-4b295e7577b3.jpg?im_w=720', 
'https://a0.muscache.com/im/pictures/885bbe23-d8de-4384-84a3-c8d4ca81534d.jpg?im_w=720', 
'https://a0.muscache.com/im/pictures/9ea14a8e-1eef-4e30-adc9-10fbc2132234.jpg?im_w=720', 
'https://a0.muscache.com/im/pictures/c273c444-f30f-4477-86a0-7d232ce95781.jpg?im_w=720', 
'https://a0.muscache.com/im/pictures/3c3543dc-378a-4d9a-9998-a10823c6cb36.jpg?im_w=720', 
'https://a0.muscache.com/im/pictures/b9d2a46c-d5e4-446a-bbe0-cacf041e6386.jpg?im_w=720', 
'https://a0.muscache.com/im/pictures/9e29b79e-8664-4f3d-a0d4-36898a4fc014.jpg?im_w=720', 
'https://a0.muscache.com/im/pictures/62fc3a97-0df4-4072-879c-cb66156876ec.jpg?im_w=720'
]

description_list = ["""
The Waterfall Cabin is near the Waterfall Tiny House just a couple of hundred feet above the falls. This peaceful setting is a place to unwind, and walk in the forest. We are located at the end of a dirt road where usually the loudest sound is the waterfalls. Just slightly off the beaten path, this location is an easy jumping-off spot to Bar Harbor & Acadia Ntl. Park, the picturesque coastal town of Belfast, and an easy drive to enjoy concerts in Bangor.
The space
The Waterfall Cabin has a rustic feel, it is one room. with three windows looking into the forest with the waterfall view. It has a shared indoor/outdoor bathroom space comprised of a composting toilet, outdoor shower, hot and cold running water for the shower and the bathroom sink. The bathroom is shared with the owner and sometimes one other person who may be visiting. This is a nature lovers' cabin.

The nice firm couch with a moose and nature theme makes into a comfortable bed. I am not just saying that I have tried it out and it actually feels good. Clean, fresh well water is brought in for drinking water in glass jars. There is a small fridge, microwave, and coffee maker.

In Monroe: we are slightly off the beaten path allowing you to enjoy the Coastal activities while staying in a very quiet rural setting. We are 15 miles from Belfast where you will find the Hills to Sea Trail starting from the town walking bridge. Here in Monroe, we have the Northern Pond hiking trail. Acadia National Park is just a 1hr and 16 min. drive. Along your way DownEast, you will find the Fort Knox and Penobscot Narrows Observatory which is the tallest in the Northern Hemisphere. The Camden Hills are 46 minutes away to the South East.

An Overview :
ShabdSangeet and the kitties have not been here very long; this is just the second season in our tiny paradise. Last year I kept an eye out to see what kinds of flowers were growing throughout the overgrown gardens. I wanted to preserve the existing gardens, so I just watched, noting which flowers grow where. In the spring the snowdrops bloomed for about 6 weeks, along with two varieties little blue lilies and a few purple and yellow crocus scattered about along with many clumps of daffodils here and there, much to my delight.

Discoveries are still being unearthed and restoration is underway. The first year about 30 bushy pine trees, which had invaded, were cut down to allowing the daylilies to begin flourishing once again. Historically, this was the Hermitage Gardens where the once well-known horticulturist, Phil White lived. He was known up and down the New England Coast for his unique daylilies, iris, and peony varieties. People would come here to spend the whole day, purchase bulbs, stroll among the many gardens and picnic near the waterfalls.

The spring of 2020 saw a more thorough removal of brush early in the season in between snowstorms. Rambling old grapevines have been cut, some were relocated in hopes of them growing as living fences, while some will be used to weave into fences around some garden areas. During the process of collecting old fall leaves for the compost piles, an old stepping stone path through daffodils and daylilies below the Waterfall Cabin was discovered, how delightful! All kinds of flowers are blooming that did not last summer. There is an old variety of climbing Rose with tiny white flowers which tiny bees love and in general, the plants are growing with more vigor.

Just as the gardens were about tidied from brush up we had 15 inches of heavy wet snowfall. There were many cracks, creaking and snapping sounds throughout the night, along with loss of power for a week. This resulted in a new mess of broken trees tops and branches and then some new inspiration for the vegetable garden; Hugelkultur! This is a big project, old and rotting logs, branches, compost, soils, hay, manure piled and arranged just so. This is like a water sponge, a great habitat for an abundant garden. In late May this is still a work in progress along with some planted mounds.
Guest access
Gardens, waterfall, nearby forest with trails.
The waterfalls are just 250 feet downhill from the cottage, are easily accessed by a trail near the old bridge. You can enjoy peaceful mornings wandering through the gardens, sitting on the rocks near the falls, swimming, sitting in chairs outside the house taking in the falls and the flowers altogether, or walking along the trails in the surrounding forest.

Other things to note:
This is a non-smoking property.
Please enjoy the surroundings with gentle respect. I like to suggest the mindset of leaving no trace but your happy memories.
Other things to note
With the soothing sounds of the waterfall in the background to enhance your visit, during the summer, you have the opportunity to take advantage of the health and wellness services are offered here:

Reiki treatment - Have you ever experienced a deep Reiki slumber? There is nothing else like it as it is like the best vacation you have ever been on without having to go anywhere. It is deeply relaxing, and restorative. $135.

Dancing Light Orchid Flower Essence Session - We begin with an intention you have opening the possibility for the subtle healing and elevating energies of the flowers help you make inner shifts and transformations when you can use some extra support with moving through resistance or simply elevating yourself. $135.

Revibrate Your Frequency with the Symphonic Gong - All you need to do is to be there while the vibrations of the Gong surround you, revibrating your energies, loosening your energetic patterns during this time allowing them to purify, release and transform into energies that support you in being who you really are. $135.

Essence Infusion Session - Take advantage of this special experience when you want help with going deep into yourself to move through things that feel stuck and you want to shift so you can go with your authentic flow, so to live your life as your best self. This session combines the potent vibrations of the Gong with the subtle vibrations of the Dancing Light Orchid Essences along with some Reiki, providing you with a unique opportunity. $175. This is the best session of all.

If you are interested in a session let's arrange a time prior to your visit. Please bring cash for your session.

Functional Medicine Health Coaching - Functional Medicine Principles are about seeking to find the roots of your health challenges. We can look at your diet and lifestyle choices, also look at the clues only you have about your life and health and how they contribute to your current and future health trajectory.

ShabdSangeet is a Reiki Master, Flower Essence Producer of Dancing Light Orchid Essences, and Practitioner, Kundalini Yoga Instructor and Functional Medicine Certified Health Coach."""]

vrbo_id = '36390811'
sojournID = f'SJ{vrbo_id}'
PMS = 'Airbnb'
pm_id = 'AR'
property_name = sql_safe_str('Waterfall Cabin with Swimming Hole')
country = 'US'
place_location = 'US, Maine, Monroe'
min_persons = 1
max_persons = 2
num_beds = 1
num_bedrooms = 1
num_bathrooms = 1
average_rating = 4.82
pets_allowed = 1
zip_code = '04951'
min_children = 0
number_of_pets = 0
house_type = 'Cabin'
latitude = round(44.608834349581024)
longitude = round(-69.04523866301075)
picture = picture_list[0]
currency = 'USD'
rate = 164
wifi = 0
iron = 1
air_conditioning = 0
hot_tub = 0
pool = 0
washer = 0
free_parking = 1
TV = 0
dryer = 0
breakfast = 0
hangers = 0
hair_dryer = 1
high_chair = 0
smoke_alarm = 0
kitchen = 0
heating = 1
fireplace = 0
laptop_friendly = 0
crib = 0
carbon_monoxide_alarm = 1
gym = 0
ski_in_out = 0
review_detail = """This rating was selected by an industry expert using industry standards"""
reviewed_by = """Sojourn API"""
review_summary = """Sojourn Estimated Rating"""
num_reviews = 32
star_rating = 2
meta_tags = "#SwimmingHole#Cabin#Remote"

sql_str = f"""insert into sojourn.places (sojournID, PMS, pm_id, property_name, country, place, min_persons, max_persons, min_children,
 number_of_pets, house_type, latitude, longitude, location, picture, currency, rate, wifi, iron, air_conditioning, hot_tub,
 pool, washer, free_parking, TV, dryer, breakfast, hangers, hair_dryer, high_chair, smoke_alarm, pets_allowed, kitchen,
 heating, fireplace, laptop_friendly, crib, carbon_monoxide_alarm, gym, ski_in_out, num_beds, num_bedrooms, num_bathrooms,
 average_rating, num_reviews, zip_code, star_rating, meta_tags) values ('{sojournID}', '{PMS}', '{pm_id}', {property_name},
'{country}', '{place_location}', {min_persons}, {max_persons}, {min_children}, {number_of_pets}, '{house_type}', {latitude}, {longitude}, 
'{place_location}', 
'{picture}', '{currency}', {rate}, {wifi}, {iron}, {air_conditioning}, {hot_tub},
{pool}, {washer}, {free_parking}, {TV}, {dryer}, {breakfast}, {hangers}, {hair_dryer}, {high_chair}, {smoke_alarm}, 
{pets_allowed}, {kitchen}, {heating}, {fireplace}, {laptop_friendly}, {crib}, {carbon_monoxide_alarm}, {gym}, {ski_in_out}, {num_beds}, 
{num_bedrooms}, {num_bathrooms}, {average_rating}, {num_reviews}, '{zip_code}', {star_rating}, '{meta_tags}')
"""

print(sql_str)
exec_result = cur.execute(sql_str)
print(f'\n{exec_result} row(s) affected...')
conn.commit()    

pic_count = 0
for pic in picture_list:
    pic_count += 1
    sql_str = f"""insert into sojourn.pictures (id, sojournID, file_url, picture_type) values
    ('{sojournID}.{pic_count:03d}', '{sojournID}', 
    '{pic}', 'Other')
    """
    print(sql_str)
    exec_result = cur.execute(sql_str)
    print(f'\n{exec_result} row(s) affected...')
    conn.commit()    

desc_count = 0
for description in description_list:
    desc_count += 1
    sql_str = f"""insert into sojourn.descriptions 
    (id, sojournID, language_code, description, description_type) values 
    ('{sojournID}.{desc_count:03d}', '{sojournID}', 'en', 
    {sql_safe_str(description)}, 
    'description')
    """
    print(sql_str)
    exec_result = cur.execute(sql_str)
    print(f'\n{exec_result} row(s) affected...')
    conn.commit()    

sql_str = f"""insert into sojourn.reviews 
(sojournID, rating, review_detail, reviewed_by, review_summary) values 
('{sojournID}', {average_rating}, 
{sql_safe_str(review_detail)}, {sql_safe_str(reviewed_by)}, 
{sql_safe_str(review_summary)})
"""
print(sql_str)
exec_result = cur.execute(sql_str)
print(f'\n{exec_result} row(s) affected...')
conn.commit()    


print('\n\n')

cur.close()
conn.close()

cur = None
