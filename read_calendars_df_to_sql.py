import xml.etree.ElementTree as ET
from datetime import datetime
from pandas import Series, DataFrame
import pandas as pd
import csv
import os
from sqlalchemy import create_engine
import pymysql 
from sqlalchemy.types import Integer, Text, String, Numeric, DateTime, Boolean, Date
from tabulate import tabulate
import random
from np_constants import pms_list

start_time = datetime.now()

engine = create_engine("mysql+pymysql://{user}:{passwd}@{host}/{db}".format(host='35.192.116.102', user='henry', passwd='finley', db='nextpax', port=3306))

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname) 
# change working directory to same as current python file
# if local machine
dname = 'Xml'
os.chdir(dname) 

###############################################################
calendars = []

for pm_code in pms_list:
    xml_filename = 'paxgenerator_calendars_{pms}.xml'.format(pms=pm_code)
    xmlp = ET.XMLParser(encoding="utf-8")
    tree = ET.parse(xml_filename,parser=xmlp)
    root = tree.getroot()
    property_mgmt_system = root.find('PropertyManagementSystem').text

    for calendar in root.iter('Calendar'):
        house_id = calendar.find('HouseID').text 
        property_mgr = calendar.find('PropertyManager').text
        
        for day in calendar.iter('Day'):

            # add property attributes to rate
            day.attrib['PropertyManagementSystem'] = property_mgmt_system
            day.attrib['HouseID'] = house_id
            day.attrib['PropertyManager'] = property_mgr

            calendars.append(day.attrib)

    xmlp = None
    tree = None
    root = None
    partner = None


calendars_df = DataFrame(calendars)

# cols = rates_df.columns.tolist() 
# cols = cols[-5:] + cols[:-5]
cols = ['PropertyManagementSystem', 'HouseID', 'PropertyManager', 'date', 'quantity', 
    'arrivalAllowed', 'departureAllowed', 'minimumStay', 'maximumStay']
calendars_df = calendars_df[cols]

calendars_df = calendars_df.replace({'arrivalAllowed':{'1': True, '0': False}, 'departureAllowed':{'1': True, '0': False}})

calendars_df = calendars_df.rename(columns={"date": "calendarDate"})
calendars_df.index = calendars_df.index.rename('id')

fieldschema={'PropertyManagementSystem': String(50), 'HouseID': String(50), 'PropertyManager': String(50),
	'calendarDate': Date, 'quantity': Integer, 'arrivalAllowed': Boolean, 'departureAllowed': Boolean,
	'minimumStay': Integer, 'maximumStay': Integer
}

calendars_df.to_sql('calendars', con = engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema)

print('\n# of Calendar records:')
print(calendars_df.count()['calendarDate'])
print('\n# by Property Mgr:')
#print(calendars_df.groupby('PropertyManagementSystem').count()['calendarDate'])
#print(additional_costs_df.groupby('Partner').count()['CostAmount'])
print(tabulate(pd.DataFrame(calendars_df.groupby('PropertyManagementSystem')['calendarDate'].count()), headers=['PropertyManagementSystem','Count']))


print('\nCalendars for ',pms_list,':')


num_rows = len(calendars_df)
num_samples = 45

random_index = []

i = 0
while i < num_samples:
    random_int = random.randint(0,num_rows-1)
    if random_int not in random_index:
        random_index.append(random_int)
        i += 1

random_index.sort()

print(f'(randomly sampling {num_samples} calendar records out of {num_rows}...)\nrow#')

#print(f'(sampling every {int(len(calendars_df)*0.033)} rows...)\n')

summary_fields = ['PropertyManagementSystem','HouseID','calendarDate','quantity','arrivalAllowed','departureAllowed','minimumStay','maximumStay']

# print(tabulate(calendars_df[calendars_df.index % int(len(calendars_df)*0.033) == 0][summary_fields], 
#     headers=summary_fields, showindex=False))
print("<pre><code>")
print(tabulate(calendars_df.iloc[random_index][summary_fields], 
    headers=summary_fields, showindex=True))
print("</code></pre>")
end_time = datetime.now()
print('Duration: {}'.format(end_time - start_time))