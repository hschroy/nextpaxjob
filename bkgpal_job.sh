echo 'starting booking-pal extract'
date
echo
echo

# source "/home/henry_schroy/venv/bin/activate"
# cd ~/daily/nextpaxjob;
if [ -f /.dockerenv ]; then
    echo "Running $0 inside a Docker container"
    cd /app/app/daily;
else
    echo "$0 (Not running inside a Docker container)"
    cd /home/henry_schroy/daily/nextpaxjob/;
    source /home/henry_schroy/daily/nextpaxjob/venv/venv/bin/activate;
fi

python bkgpal_sojourn_upsert.py;
#python update_stars.py;

echo 'ending booking-pal extract'
date
echo 'restarting docker container sojournapi on sojournapi-dev2:'
gcloud beta compute ssh --zone "us-central1-a" "sojournapi-dev2"  --project "sojourn-api-development" --command="docker restart sojournapi"
echo
echo 'restarting docker container sojournapi on sojournapi-20200905:'
gcloud beta compute ssh --zone "us-central1-a" "sojournapi-20200905"  --project "sojourn-api-development" --command="docker restart sojournapi"

echo
echo 'Batch run time:'