echo 'starting henry-test extract'
date
echo
echo

if [ -f /.dockerenv ]; then
    echo "Running $0 inside a Docker container"
    cd /app/app/daily;
else
    echo "$0 (Not running inside a Docker container)"
    cd /home/henry_schroy/daily/nextpaxjob/;
    source /home/henry_schroy/daily/nextpaxjob/venv/venv/bin/activate;
fi

python henry_test.py;
#python update_stars.py;

echo 'ending henry-test extract'
date

echo
echo 'Batch run time:'