# README #

the files in this repo are expected to reside in directory 

    ~/daily/nextpaxjob


the morning run is set off by running

    bash ~/daily/nextpaxjob/jobsendlog.sh

this runs another batch script, nextpaxjob.sh, which runs all of the main import python modules. the output is redirected into a nextpaxjob.log file which is emailed to dev (currently using socketlabs)

### What is this repository for? ###

nextpax morning jobs

### How do I get set up? ###

runs on cloud vm instance
launched by cron job

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

henry@furraylogic.com