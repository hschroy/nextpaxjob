import pymysql 
import pandas as pd
import datetime
import os
import json

conn2 = pymysql.connect(host='35.192.116.102', user='henry', passwd='finley', db='nextpax', port=3306)

cur = conn2.cursor()
cur.execute('USE nextpax')

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname) 
# change working directory to same as current python file
# if local machine
dname = 'Json'
os.chdir(dname) 

json_filename = 'nextpax_mapping_codes.json'

with open(json_filename) as f:
    mapping_codes = json.load(f)
    
# [": ".join((x['typeCode'],x['name']['en'])) for x in mapping_codes['data'][0]['propertyTypes']]
# nextpax._HouseType  HouseType, Description
print('\nnextpax._HouseType  HouseType, Description')
for x in mapping_codes['data'][0]['propertyTypes']:
    typeCode = x['typeCode']
    name_en = x['name']['en']

    print('\n\n')
    print(": ".join((typeCode,name_en)))

    sql_string = f"""SELECT HouseType, Description FROM nextpax._HouseType 
                    WHERE HouseType = '{typeCode}'"""
    print(sql_string)
    exec_result = cur.execute(sql_string)
    result = cur.fetchall()

    print(f'{exec_result} row(s) found.')

    if exec_result == 0:
        sql_string = f"""INSERT INTO nextpax._HouseType (HouseType, Description) 
                        VALUES ('{typeCode}', '{name_en}')"""
        print(sql_string)
        exec_result = cur.execute(sql_string)
        print(f'{exec_result} row(s) affected...')
        conn2.commit()

    for row in result:
        HouseType = row[0]
        Description = row[1]
        print(": ".join((HouseType, Description)))

        if Description != name_en:
            sql_string = f"""UPDATE nextpax._HouseType SET Description = '{name_en}'
                    WHERE HouseType = '{typeCode}'"""
            print(sql_string)
            exec_result = cur.execute(sql_string)
            print(f'{exec_result} row(s) affected...')
            conn2.commit()


# [": ".join((x['amenityCode'],x['name']['en'])) for x in mapping_codes['data'][0]['propertyAmenities']]
# nextpax._PropertyCode PropertyCode, Description
print('\nnextpax._PropertyCode PropertyCode, Description')
for x in mapping_codes['data'][0]['propertyAmenities']:
    amenityCode = x['amenityCode']
    name_en = x['name']['en']
    print('\n\n')
    print(": ".join((amenityCode,name_en)))
    sql_string = f"""SELECT PropertyCode, Description FROM nextpax._PropertyCode 
                    WHERE PropertyCode = '{amenityCode}'"""
    print(sql_string)
    exec_result = cur.execute(sql_string)
    result = cur.fetchall()

    print(f'{exec_result} row(s) found.')

    if exec_result == 0:
        sql_string = f"""INSERT INTO nextpax._PropertyCode (PropertyCode, Description) 
                    VALUES ('{amenityCode}', '{name_en}')"""
        print(sql_string)
        exec_result = cur.execute(sql_string)
        print(f'{exec_result} row(s) affected...')
        conn2.commit()

    for row in result:
        PropertyCode = row[0]
        Description = row[1]
        print(": ".join((PropertyCode, Description)))

        if Description != name_en:
            sql_string = f"""UPDATE nextpax._PropertyCode SET Description = '{name_en}'
                    WHERE PropertyCode = '{amenityCode}'"""
            print(sql_string)
            exec_result = cur.execute(sql_string)
            print(f'{exec_result} row(s) affected...')
            conn2.commit()

# [": ".join((x['typeCode'],x['name']['en'])) for x in mapping_codes['data'][0]['propertyNearestPlaces']]
# ???

# [": ".join((x['taxCode'],x['name']['en'])) for x in mapping_codes['data'][0]['propertyTaxes']]
# nextpax._TaxType TaxType, Description
# maybe it's still in dev, the "propertyTaxes" node is all taxCode: 'VAT'
# print('\nnextpax._TaxType TaxType, Description')
# for x in mapping_codes['data'][0]['propertyTaxes']:
#     print(": ".join((x['taxCode'],x['name']['en'])))

# [": ".join((x['typeCode'],x['name']['en'])) for x in mapping_codes['data'][0]['propertyImages']]
# ???

# [": ".join((x['typeCode'],x['name']['en'])) for x in mapping_codes['data'][0]['propertySubrooms']]
# ???

# [": ".join((x['typeCode'],x['name']['en'])) for x in mapping_codes['data'][0]['subroomItems']]
# ???

