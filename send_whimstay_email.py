import os
from pathlib import Path
import datetime
from datetime import date

from socketlabs.injectionapi import SocketLabsClient
from socketlabs.injectionapi.message.basicmessage import BasicMessage
from socketlabs.injectionapi.message.emailaddress import EmailAddress
import socket

my_host_name = socket.gethostname()

start_time = datetime.datetime.now()

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
# change working directory to same as current python file
os.chdir(dname) 

whimstaylog = "Hello from sojourn-api. This is the output from the latest whimstay extract:\n\n\n" + Path('whimstayjob.log').read_text()

serverId = 34540
injectionApiKey = "d8HTa5z3A7FfJe24LwSq"

client = SocketLabsClient(serverId, injectionApiKey)

message = BasicMessage()

message.subject = start_time.strftime(f"{my_host_name} - whimstay extract: %m/%d/%Y, %H:%M:%S")
message.html_body = ''.join(['<BODY><code>',whimstaylog.replace('\n','<br />'),'</code></BODY>'])

message.from_email_address = EmailAddress("henry@furraylogic.com")
# message.to_email_address.append(EmailAddress("henry@furraylogic.com"))
message.to_email_address.append(EmailAddress("support@furraylogic.com"))
message.to_email_address.append(EmailAddress("rob@sojournapi.com"))

response = client.send(message)
print(response)
