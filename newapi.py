import requests
import json
import pymysql 
import pandas as pd
import datetime
from tabulate import tabulate

conn = pymysql.connect(host='35.192.116.102', user='henry', passwd='finley', db='nextpax', port=3306)
cur = conn.cursor()
import_date = datetime.datetime.now().strftime("%Y-%m-%d")
token_start = datetime.datetime.now()
script_start = datetime.datetime.now()

client_secret='c96ac38a-c9d5-4e50-94cb-56a8e24a66e0'
client_id='client_SOJ358'
HTTP_HEADERS = {
    'Content-Type': 'application/x-www-form-urlencoded'
}
params = {
"client_secret":f"{client_secret}",
"client_id":f"{client_id}",
"grant_type":"client_credentials"
}


def safe_str(value):
    if value is not None:
        return f"""'{value.replace("'","''")}'"""
    else:
        return 'null'

def safe_num(value):
    if value is not None:
        return f"{value}"
    else:
        return 'null'

def get_val(dict, key):
    try:
        return dict[key]
    except:
        return None

def safe_ratio(first, second):
    try:
        return first/second
    except:
        return 0
    
redirect = lambda x : x.replace('nextpax','sojournapi')

def join_safe(list_to_join):
    join_safe = ''
    for item in list_to_join:
        if item is not None:
            join_safe += item
    return join_safe

def get_persons(persons_list):
    get_persons = ''
    if persons_list[0] is not None:
        lowest = persons_list[0]
        highest = persons_list[0]

        for item in persons_list:
            lowest = min(lowest,item)
            highest = max(highest,item)

        get_persons = f'{lowest}-{highest}'
    return get_persons

def get_weekdays(days_list):
    get_weekdays = ''
    if len(days_list)>0:
        get_weekdays = '|'.join([str(x) for x in days_list])
    return get_weekdays

def get_access_token():
        endpoint = 'https://distribution.nextpax.app/auth/realms/api/protocol/openid-connect/token'
        # get access_token for this session
        response = requests.post(endpoint, headers=HTTP_HEADERS, data=params)
        response_json = json.loads(response.content)
        access_token = response_json['access_token']
        tok_dur = response_json['expires_in']

        pm_head = {
            "accept": "application/json",
            "Authorization": f"Bearer {access_token}"
        }
        print(f"\n\nNew access token at script elapsed time: {datetime.datetime.now() - token_start}\n\n")

        return pm_head, tok_dur

# curl --location --request POST 'https://distribution.nextpax.app/auth/realms/api/protocol/openid-connect/token' \
# --header 'Content-Type: application/x-www-form-urlencoded' \
# --data-urlencode 'client_secret=c96ac38a-c9d5-4e50-94cb-56a8e24a66e0' \
# --data-urlencode 'client_id=client_SOJ358' \
# --data-urlencode 'grant_type=client_credentials'

pm_header, token_duration = get_access_token()

endpoint = "https://distribution.nextpax.app/api/v1/content/property-managers"
response = requests.get(endpoint, headers=pm_header)
pms_json = json.loads(response.content)

endpoint = "https://distribution.nextpax.app/api/v1/content/properties"

sql_string = "SET SQL_SAFE_UPDATES = 0;"
print('\n' + sql_string)
exec_result = cur.execute(sql_string)
print(f'\n{exec_result} row(s) affected...')
conn.commit()

print('\n# zap houses...')
sql_string = f"""delete FROM nextpax.houses_ where HouseID <> '';"""
print('\n' + sql_string)
exec_result = cur.execute(sql_string)
print(f'\n{exec_result} row(s) affected...')
conn.commit()

print('\n# zap additional costs...')
sql_string = f"""delete FROM nextpax.additional_costs_ where HouseID <> '';"""
print('\n' + sql_string)
exec_result = cur.execute(sql_string)
print(f'\n{exec_result} row(s) affected...')
conn.commit()

print('\n# zap taxes...')
sql_string = f"""delete from nextpax.taxes_ where HouseID <> '';"""
print('\n' + sql_string)
exec_result = cur.execute(sql_string)
print(f'\n{exec_result} row(s) affected...')
conn.commit()

print('\n# zap pictures...')
sql_string = f"""delete FROM nextpax.pictures_ where HouseID <> '';"""
print('\n' + sql_string)
exec_result = cur.execute(sql_string)
print(f'\n{exec_result} row(s) affected...')
conn.commit()

print('\n# zap amenities...')
sql_string = f"""delete FROM nextpax.amenities_ where HouseID <> '';"""
print('\n' + sql_string)
exec_result = cur.execute(sql_string)
print(f'\n{exec_result} row(s) affected...')
conn.commit()

print('\n# zap descriptions...')
sql_string = f"""delete FROM nextpax.descriptions_ where HouseID <> '';"""
print('\n' + sql_string)
exec_result = cur.execute(sql_string)
print(f'\n{exec_result} row(s) affected...')
conn.commit()

print('\n# zap rates...')
sql_string = f"""delete FROM nextpax.rates_ where HouseID <> '';"""
print('\n' + sql_string)
exec_result = cur.execute(sql_string)
print(f'\n{exec_result} row(s) affected...')
conn.commit()

print('\n# zap calendars...')
sql_string = f"""delete FROM nextpax.calendars_ where HouseID <> '';"""
print('\n' + sql_string)
exec_result = cur.execute(sql_string)
print(f'\n{exec_result} row(s) affected...')
conn.commit()

for brand in pms_json['data']:
    BrandID = brand['propertyManager']
    NP_Name = brand['name']
    PMS = brand['pms']
    num_places = 0
    fee_count = 0
    tax_count = 0
    picture_count = 0
    amenity_count = 0
    description_count = 0
    rate_count = 0
    calendar_count = 0
    print(f"\n{PMS} {BrandID} {NP_Name}")
    if PMS == 'PaxGenerator':
        list_count = 20
    else:
        list_count = 0
    while list_count == 20:
        # keep on offsetting
        if (datetime.datetime.now() - token_start).seconds > token_duration * .99:
            token_start = datetime.datetime.now()
            pm_header, token_duration = get_access_token()
        response = requests.get(f'{endpoint}/?offset={num_places}&propertyManager={BrandID}&extras=all', headers=pm_header)
        houselist_json = json.loads(response.content)
        if not houselist_json['data'] is None:
            list_count = 0
            for house in houselist_json['data']:
                list_count += 1
                House_ID = str(house['propertyId'])
                Property_Mgr = house['propertyManager']
                print(f"\n\n{PMS} {BrandID} {NP_Name} {Property_Mgr} {House_ID}")
                #print(house['propertyId'])
                # add house to table
                sql_string = f"""INSERT INTO nextpax.houses_ (HouseID, Brand, Partner, HouseName, HouseType, Country,
                Place, Address, ZipCode, Latitude, Longitude, MinPersons, MaxPersons, NumberOfPets) 
                VALUES ({safe_str(House_ID)}, {safe_str(Property_Mgr)}, {safe_str(PMS)}, 
                {safe_str(house['general']['name'])}, {safe_str(house['general']['typeCode'])}, {safe_str(house['general']['countryCode'])},
                {safe_str(house['general']['city'])}, {safe_str(house['general']['address'])}, {safe_str(get_val(get_val(house,'general'),'postalCode'))},
                {safe_num(house['general']['latitude'])}, {safe_num(house['general']['longitude'])}, {safe_num(house['general']['minOccupancy'])},
                {safe_num(house['general']['maxOccupancy'])}, {safe_num(house['general']['maxPets'])})"""

                #print('\n' + sql_string)
                exec_result = cur.execute(sql_string)
                #print(f'\n{exec_result} row(s) affected...')
                conn.commit()
                # now add all fees
                if not house['fees'] is None:
                    for fee in house['fees']:
                        fee_count += 1
                        sql_string = f"""INSERT INTO nextpax.additional_costs_ (Partner, HouseID, CostCode, CostType, CostAmount, CostAmountType,
                        CostCurrency, CostNumber, FromDate, UntilDate) VALUES ({safe_str(PMS)}, {safe_str(House_ID)},
                        {safe_str(fee['feeCode'])}, {safe_str(fee['chargeMode'])}, {safe_ratio(fee['amount'],100)}, {safe_str(fee['chargeType'])},
                        {safe_str(fee['currency'])}, {fee_count}, {safe_str(get_val(fee,'fromDate'))}, {safe_str(get_val(fee,'untilDate'))})
                        """
                        #print('\n' + sql_string)
                        exec_result = cur.execute(sql_string)
                        #print(f'\n{exec_result} row(s) affected...')
                        conn.commit()
                print(f"{PMS} {BrandID} {NP_Name} {Property_Mgr} {House_ID} fees:{fee_count}")

                # now add all taxes
                if not house['taxes'] is None:
                    for tax in house['taxes']:
                        tax_count += 1
                        sql_string = f"""INSERT INTO nextpax.taxes_ (Partner, HouseID, TaxType, Percentage, Included) 
                        VALUES ({safe_str(PMS)}, {safe_str(House_ID)},
                        {safe_str(tax['taxCode'])}, {safe_num(tax['percentage'])}, {safe_num(tax['included'])})
                        """
                        #print('\n' + sql_string)
                        exec_result = cur.execute(sql_string)
                        #print(f'\n{exec_result} row(s) affected...')
                        conn.commit()

                print(f"{PMS} {BrandID} {NP_Name} {Property_Mgr} {House_ID} taxes:{tax_count}")
                # now add all pictures
                if not house['images'] is None:
                    for picture in house['images']:
                        picture_count += 1
                        FileUrl = redirect(picture['url'])
                        sql_string = f"""INSERT INTO nextpax.pictures_ (Partner, HouseID, FileUrl, PictureType) 
                        VALUES ({safe_str(PMS)}, {safe_str(House_ID)},
                        {safe_str(FileUrl)}, {safe_str(picture['typeCode'])})
                        """
                        #print('\n' + sql_string)
                        exec_result = cur.execute(sql_string)
                        #print(f'\n{exec_result} row(s) affected...')
                        conn.commit()

                print(f"{PMS} {BrandID} {NP_Name} {Property_Mgr} {House_ID} images:{picture_count}")
                # now add all amenities
                if not house['amenities'] is None:
                    for amenity in house['amenities']:
                        amenity_count += 1
                        sql_string = f"""INSERT INTO nextpax.amenities_ (Partner, HouseID, PropertyCode, AmenityValue) 
                        VALUES ({safe_str(PMS)}, {safe_str(House_ID)},
                        {safe_str(amenity['typeCode'])}, {safe_str(join_safe(amenity['attribute']))})
                        """
                        #print('\n' + sql_string)
                        exec_result = cur.execute(sql_string)
                        #print(f'\n{exec_result} row(s) affected...')
                        conn.commit()

                print(f"{PMS} {BrandID} {NP_Name} {Property_Mgr} {House_ID} amenities:{amenity_count}")
                # now add all descriptions
                if not house['descriptions'] is None:
                    for description in house['descriptions']:
                        description_count += 1
                        sql_string = f"""INSERT INTO nextpax.descriptions_ (Partner, HouseID, LanguageCode, Description,
                        DescriptionType) 
                        VALUES ({safe_str(PMS)}, {safe_str(House_ID)},
                        {safe_str(description['language'])}, {safe_str(description['text'])}, {safe_str(description['typeCode'])})
                        """
                        #print('\n' + sql_string)
                        exec_result = cur.execute(sql_string)
                        #print(f'\n{exec_result} row(s) affected...')
                        conn.commit()

                print(f"{PMS} {BrandID} {NP_Name} {Property_Mgr} {House_ID} descriptions:{description_count}")
                # get rates
                if (datetime.datetime.now() - token_start).seconds > token_duration * .99:
                    token_start = datetime.datetime.now()
                    pm_header, token_duration = get_access_token()
                rates_endpoint = "https://distribution.nextpax.app/api/v1/availability/rates"
                rates_response = requests.get(f'{rates_endpoint}/{House_ID}', headers=pm_header)
                rates_json = json.loads(rates_response.content)
                if not rates_json['data'][0]['rates'] is None:
                    for rate in rates_json['data'][0]['rates']:
                        rate_count += 1
                        persons = get_persons(rate['persons'])
                        weekdays = get_weekdays(rate['weekdays'])
                        sql_string = f"""INSERT INTO nextpax.rates_ (PropertyManagementSystem, HouseID, PropertyManager, fromDate,
                        tillDate, duration, amount, currency, persons, weekdays, minimumStay, maximumStay,
                        extraPersonFeeAmount, extraPersonFeeCurrency) 
                        VALUES ({safe_str(PMS)}, {safe_str(House_ID)}, {safe_str(Property_Mgr)},
                        {safe_str(rate['fromDate'])}, {safe_str(rate['untilDate'])}, {safe_num(rate['duration'])},
                        {safe_ratio(rate['baseAmount'],100)}, {safe_str(rate['currency'])}, {safe_str(persons)},
                        {safe_str(weekdays)}, {safe_num(rate['minStay'])}, {safe_num(rate['maxStay'])}, 
                        {safe_ratio(rate['extraPersonFeeAmount'],100)}, {safe_str(rate['extraPersonFeeCurrency'])})
                        """
                        #print('\n' + sql_string)
                        exec_result = cur.execute(sql_string)
                        #print(f'\n{exec_result} row(s) affected...')
                        conn.commit()

                print(f"{PMS} {BrandID} {NP_Name} {Property_Mgr} {House_ID} rates:{rate_count}")
                # get calendars
                if (datetime.datetime.now() - token_start).seconds > token_duration * .99:
                    token_start = datetime.datetime.now()
                    pm_header, token_duration = get_access_token()
                calendars_endpoint = "https://distribution.nextpax.app/api/v1/availability/calendars"
                calendars_response = requests.get(f'{calendars_endpoint}/{House_ID}', headers=pm_header)
                calendars_json = json.loads(calendars_response.content)
                if not calendars_json['data'][0]['availability'] is None:
                    for calendar in calendars_json['data'][0]['availability']:
                        calendar_count += 1
                        sql_string = f"""INSERT INTO nextpax.calendars_ (PropertyManagementSystem, HouseID, PropertyManager, calendarDate,
                        quantity, arrivalAllowed, departureAllowed, minimumStay, maximumStay) 
                        VALUES ({safe_str(PMS)}, {safe_str(House_ID)}, {safe_str(Property_Mgr)},
                        {safe_str(calendar['date'])}, {safe_num(calendar['quantity'])}, {safe_num(calendar['restrictions']['arrivalAllowed'])},
                        {safe_num(calendar['restrictions']['departureAllowed'])}, {safe_num(calendar['restrictions']['minStay'])}, 
                        {safe_num(calendar['restrictions']['maxStay'])})
                        """
                        #print('\n' + sql_string)
                        exec_result = cur.execute(sql_string)
                        #print(f'\n{exec_result} row(s) affected...')
                        conn.commit()

                print(f"{PMS} {BrandID} {NP_Name} {Property_Mgr} {House_ID} calendars:{calendar_count}")
                print(f"Token elapsed time: {(datetime.datetime.now() - token_start).seconds}")
                print(f"Token duration:     {token_duration}      Num of places:{num_places+list_count}")
                print(f"Import script elapsed time: {datetime.datetime.now() - token_start}")
            num_places += list_count
        else:
            list_count = 0

    # update brands
    sql_string = f"""SELECT id FROM nextpax.brands_ WHERE BrandID = '{BrandID}'"""
    cur.execute(sql_string)
    result = cur.fetchone()
    if not result is None:
        brands_id = result[0]
        sql_string = f"""UPDATE nextpax.brands_ SET NP_Name = '{NP_Name}', PMS = '{PMS}', LatestImport = '{import_date}',
        Num_Places = {num_places}, Num_Fees = {fee_count}, Num_Taxes = {tax_count}, Num_Pictures = {picture_count},
        Num_Amenities = {amenity_count}, Num_Descriptions = {description_count}, Num_Rates = {rate_count},
        Num_Calendars = {calendar_count}
        WHERE id = {brands_id}"""
    else:
        sql_string = f"""INSERT INTO nextpax.brands_ (BrandID, NP_Name, PMS, LatestImport, Num_Places, Num_Fees, Num_Taxes,
        Num_Pictures, Num_Amenities, Num_Descriptions, Num_Rates, Num_Calendars) 
        VALUES ('{BrandID}', '{NP_Name}', '{PMS}', '{import_date}', {num_places}, {fee_count}, {tax_count}, {picture_count},
        {amenity_count}, {description_count}, {rate_count}, {calendar_count})"""

    print('\n' + sql_string)
    exec_result = cur.execute(sql_string)
    print(f'\n{exec_result} row(s) affected...')
    conn.commit()
    print(f"Token elapsed time: {datetime.datetime.now() - token_start}")


print(f"\n\nImport script elapsed time: {datetime.datetime.now() - token_start}")
