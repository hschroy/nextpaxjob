import os
from pathlib import Path
import datetime
from datetime import date

from socketlabs.injectionapi import SocketLabsClient
from socketlabs.injectionapi.message.basicmessage import BasicMessage
from socketlabs.injectionapi.message.emailaddress import EmailAddress
import socket

my_host_name = socket.gethostname()

start_time = datetime.datetime.now()

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
# change working directory to same as current python file
os.chdir(dname) 

bkgpal_log = """Hello, this script launches a stub with the full environment to run the bookingpal extract.  
    The script return code is 0 if succesfully completed, return code 1 means script is already running.
    Since this script runs very quickly, there is a 10 second wait to give time to test this feature.

    It can be triggered by running:\n\ncurl -X POST 35.192.45.223:80/launchbkgpal\n\n""" + Path('henry_test.log').read_text()

serverId = 34540
injectionApiKey = "d8HTa5z3A7FfJe24LwSq"

client = SocketLabsClient(serverId, injectionApiKey)

message = BasicMessage()

message.subject = start_time.strftime(f"{my_host_name} - HENRY-TEST extract: %m/%d/%Y, %H:%M:%S")
message.html_body = ''.join(['<BODY><code>',bkgpal_log.replace('\n','<br />'),'</code></BODY>'])

message.from_email_address = EmailAddress("henry@furraylogic.com")
message.to_email_address.append(EmailAddress("henry@furraylogic.com"))
message.to_email_address.append(EmailAddress("george@furraylogic.com"))

response = client.send(message)
print(response)
