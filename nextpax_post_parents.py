import pymysql 
import pandas as pd
import random
import datetime
from dotenv import load_dotenv
import os
load_dotenv()

SOJOURN_HOST = os.getenv('SOJOURN_HOST')
SOJOURN_USER = os.getenv('SOJOURN_USER')
SOJOURN_PWD = os.getenv('SOJOURN_PWD')


script_start = datetime.datetime.now()

print('\n**** NEXTPAX PARENTS POSTPROCESS... ****')

conn = pymysql.connect(host=SOJOURN_HOST, user=SOJOURN_USER, passwd=SOJOURN_PWD, db='sojourn', port=3306, charset='utf8mb4')

cur = conn.cursor()
cur.execute('USE sojourn')
top_parent_counter = 0
num_updates = 0

sql_string = f"SELECT DISTINCT top_parent FROM sojourn_db.places WHERE parent_id IS null and top_parent is not null"
# place_ids = pd.read_sql(sql_string,conn)
cur.execute(sql_string)
place_ids2 = cur.fetchall()
place_ids = pd.DataFrame(place_ids2,columns=['top_parent'])
for index, row in place_ids.iterrows():
    top_parent = row.top_parent
    top_parent_counter += 1
    sql_string = f'''select cast(sum(max_persons*max_units) as signed) as max_persons,
        cast(sum(num_beds*max_units) as signed) as num_beds,
        cast(sum(num_bedrooms*max_units) as signed) as num_bedrooms,
        round(sum(num_bathrooms*max_units),2) as num_bathrooms,
        round(avg(average_rating),4) as average_rating,
        if(sum(wifi) > 0,1,0) as wifi, if(sum(iron) > 0,1,0) as iron,
        if(sum(air_conditioning) > 0,1,0) as air_conditioning, if(sum(hot_tub) > 0,1,0) as hot_tub,
        if(sum(pool) > 0,1,0) as pool, if(sum(washer) > 0,1,0) as washer,
        if(sum(free_parking) > 0,1,0) as free_parking, if(sum(TV) > 0,1,0) as TV,
        if(sum(dryer) > 0,1,0) as dryer, if(sum(breakfast) > 0,1,0) as breakfast,
        if(sum(hangers) > 0,1,0) as hangers, if(sum(hair_dryer) > 0,1,0) as hair_dryer,
        if(sum(high_chair) > 0,1,0) as high_chair, if(sum(smoke_alarm) > 0,1,0) as smoke_alarm,
        if(sum(pets_allowed) > 0,1,0) as pets_allowed, if(sum(kitchen) > 0,1,0) as kitchen,
        if(sum(heating) > 0,1,0) as heating, if(sum(fireplace) > 0,1,0) as fireplace,
        if(sum(laptop_friendly) > 0,1,0) as laptop_friendly, if(sum(crib) > 0,1,0) as crib,
        if(sum(carbon_monoxide_alarm) > 0,1,0) as carbon_monoxide_alarm, if(sum(gym) > 0,1,0) as gym,
        if(sum(ski_in_out) > 0,1,0) as ski_in_out,
        min(min_stay) as min_stay
        from sojourn_db.places
        where top_parent = '{top_parent}' and sojournID <> top_parent'''
    cur.execute(sql_string)

    result = cur.fetchone()
    if not result is None and not result[0] is None:
        max_persons = result[0]
        num_beds = result[1]
        num_bedrooms = result[2]
        num_bathrooms = result[3]
        average_rating = result[4]
        wifi = result[5]
        iron = result[6]
        air_conditioning = result[7]
        hot_tub = result[8]
        pool = result[9]
        washer = result[10]
        free_parking = result[11]
        TV = result[12]
        dryer = result[13]
        breakfast = result[14]
        hangers = result[15]
        hair_dryer = result[16]
        high_chair = result[17]
        smoke_alarm = result[18]
        pets_allowed = result[19]
        kitchen = result[20]
        heating = result[21]
        fireplace = result[22]
        laptop_friendly = result[23]
        crib = result[24]
        carbon_monoxide_alarm = result[25]
        gym = result[26]
        ski_in_out = result[27]
        min_stay = result[28]

        sql_string = f'''SET SQL_SAFE_UPDATES = 0;'''
        exec_result = cur.execute(sql_string)
        sql_string = f'''update sojourn_db.places set house_type = classification, max_persons = {max_persons},
            num_beds = {num_beds}, num_bedrooms = {num_bedrooms}, num_bathrooms = {num_bathrooms},
            average_rating = {average_rating}, wifi = {wifi}, iron = {iron}, 
            air_conditioning = {air_conditioning}, hot_tub = {hot_tub}, pool = {pool},
            washer = {washer}, free_parking = {free_parking}, TV = {TV}, 
            dryer = {dryer}, breakfast = {breakfast}, hangers = {hangers},
            hair_dryer = {hair_dryer}, high_chair = {high_chair}, smoke_alarm = {smoke_alarm},
            pets_allowed = {pets_allowed}, kitchen = {kitchen}, heating = {heating},
            fireplace = {fireplace}, laptop_friendly = {laptop_friendly}, crib = {crib},
            carbon_monoxide_alarm = {carbon_monoxide_alarm}, gym = {gym},
            ski_in_out = {ski_in_out}, min_stay = {min_stay}
            where sojournID = '{top_parent}';'''
        exec_result = cur.execute(sql_string)
        #print('\n' + sql_string)
        #print(f'\n{exec_result} row(s) affected...')
        num_updates += 1
        conn.commit()
    else:
        max_persons = 0
        num_beds = 0
        num_bedrooms = 0
        num_bathrooms = 0
        average_rating = 0
        wifi = 0
        iron = 0
        air_conditioning = 0
        hot_tub = 0
        pool = 0
        washer = 0
        free_parking = 0
        TV = 0
        dryer = 0
        breakfast = 0
        hangers = 0
        hair_dryer = 0
        high_chair = 0
        smoke_alarm = 0
        pets_allowed = 0
        kitchen = 0
        heating = 0
        fireplace = 0
        laptop_friendly = 0
        crib = 0
        carbon_monoxide_alarm = 0
        gym = 0
        ski_in_out = 0
        min_stay = 0




print(f"{top_parent_counter} top parents")
print(f"{num_updates} updates.")

print(f"\n\nParents Postprocess (multi-units) script elapsed time: {datetime.datetime.now() - script_start}")
