import xml.etree.ElementTree as ET
from datetime import datetime
from pandas import Series, DataFrame
import pandas as pd
import csv
import os
from sqlalchemy import create_engine
import pymysql 
from sqlalchemy.types import Integer, Text, String, Numeric, DateTime, Boolean, Date
from tabulate import tabulate
import random
from np_constants import pms_list

start_time = datetime.now()

engine = create_engine("mysql+pymysql://{user}:{passwd}@{host}/{db}".format(host='35.192.116.102', user='henry', passwd='finley', db='nextpax', port=3306))

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
# change working directory to same as current python file
# if local machine
os.chdir(dname) 

dname = 'Xml'
os.chdir(dname) 

###############################################################
house_additional_costs = []

for pm_code in pms_list:
    xml_filename = 'paxgenerator_house_additional_costs_{pms}.xml'.format(pms=pm_code)
    xmlp = ET.XMLParser(encoding="utf-8")
    tree = ET.parse(xml_filename,parser=xmlp)
    root = tree.getroot()
    partner = root.find('Partner').text

    for additional_costs in root.iter('AdditionalCosts'):
        house_id = additional_costs.find('HouseID').text 

        for additional_cost in additional_costs.iter('AdditionalCost'):

            # add property attributes to additional_cost
            additional_cost.attrib['Partner'] = partner
            additional_cost.attrib['HouseID'] = house_id
            i = 0
            while i < len(additional_cost):
                additional_cost.attrib[additional_cost[i].tag] = additional_cost[i].text
                i += 1

            house_additional_costs.append(additional_cost.attrib)

    xmlp = None
    tree = None
    root = None
    partner = None


additional_costs_df = DataFrame(house_additional_costs)

additional_costs_df = additional_costs_df.rename(columns={"From": "FromDate", "Until": "UntilDate",
    "Number": "CostNumber"})
additional_costs_df.index = additional_costs_df.index.rename('id')

fieldschema={
	'Partner': String(50), 'HouseID': String(50), 'CostCode': String(50), 'CostType': String(50),
	'CostAmount': Numeric(10,2), 'CostAmountType': String(50), 'CostCurrency': String(50),
	'CostNumber': Integer, 'FromDate': Date, 'UntilDate': Date
}

# drop duplicates
additional_costs_df = additional_costs_df.drop_duplicates()

additional_costs_df.to_sql('additional_costs', con = engine, if_exists = 'replace', 
    chunksize = 1000, dtype=fieldschema)

print('\n# of Additional Costs:')
print(additional_costs_df.count()['CostAmount'])
print('\n# by Property Mgr:')
#print(additional_costs_df.groupby('Partner').count()['CostAmount'])
print(tabulate(pd.DataFrame(additional_costs_df.groupby('Partner')['CostAmount'].count()), headers=['Partner','Count']))
print('\nAdditional Costs for ',pms_list,':')


num_rows = len(additional_costs_df)
num_samples = 45

random_index = []

i = 0
while i < num_samples:
    random_int = random.randint(0,num_rows-1)
    if random_int not in random_index:
        random_index.append(random_int)
        i += 1

random_index.sort()

print(f'(randomly sampling {num_samples} additional costs out of {num_rows} rows...)\nrow#')

#print(f'(sampling every {int(len(additional_costs_df)*0.033)} rows...)\n')

summary_fields = ['Partner','HouseID','CostCode','CostType','CostAmount','CostAmountType','CostCurrency','FromDate','UntilDate']

# print(tabulate(additional_costs_df[additional_costs_df.index % int(len(additional_costs_df)*0.033) == 0][summary_fields], 
#     headers=summary_fields, showindex=False, tablefmt="html"))
print("<pre><code>")
print(tabulate(additional_costs_df.iloc[random_index][summary_fields], 
    headers=summary_fields, showindex=True))
print("</code></pre>")

end_time = datetime.now()
print('Duration: {}'.format(end_time - start_time))