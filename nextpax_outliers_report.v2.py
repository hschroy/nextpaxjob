import pymysql 
import pandas as pd
from email_warn import send_warning
import datetime
from datetime import date
from tabulate import tabulate
from dotenv import load_dotenv
import os
load_dotenv()


NEXTPAX_HOST = os.getenv('NEXTPAX_HOST')
NEXTPAX_USER = os.getenv('NEXTPAX_USER')
NEXTPAX_PWD = os.getenv('NEXTPAX_PWD')

script_start = datetime.datetime.now()

print('\n**** NEXTPAX OUTLIERS REPORT... ****')
print('\nNextpax outliers report...')
check_date = datetime.datetime.now().strftime("%Y-%m-%d")
print(check_date)

conn = pymysql.connect(host=NEXTPAX_HOST, user=NEXTPAX_USER, passwd=NEXTPAX_PWD, db='nextpax', port=3306, charset='utf8mb4')

cur = conn.cursor()
cur.execute('USE nextpax')

def do_table(tablename):
    # **PLACES
    print(f'\n**** checking {tablename} ****')
    print(f'houses in {tablename} but not in houses')
    sql_string = f'SELECT DISTINCT HouseID FROM nextpax.{tablename} WHERE HouseID NOT IN (SELECT DISTINCT HouseID FROM nextpax.houses)'
    print(sql_string)
    num_rows = cur.execute(sql_string)
    print(f'{num_rows} row(s) returned')
    if num_rows > 0:
        #house_ids = pd.read_sql(sql_string,conn)
        house_ids2 = cur.fetchall()
        house_ids = pd.DataFrame(house_ids2,columns=['HouseID'])
        for index, row in house_ids.iterrows(): 
            sql_string=f'''
            INSERT INTO nextpax.outliers(checked_date, HouseID, {tablename})
            SELECT ol.* FROM
            (SELECT '{check_date}' AS checked_date, '{row['HouseID']}' AS HouseID, 1 AS {tablename}) AS ol
            ON DUPLICATE KEY UPDATE checked_date=ol.checked_date, HouseID=ol.HouseID, {tablename}=ol.{tablename}
            ''' 
            cur.execute(sql_string)
        conn.commit()
    print(f'houses in houses but not in {tablename}')
    sql_string = f"""SELECT DISTINCT HouseID FROM nextpax.houses WHERE 
            HouseID NOT IN (SELECT DISTINCT HouseID FROM nextpax.{tablename})"""
    print(sql_string)
    num_rows = cur.execute(sql_string)
    print(f'{num_rows} row(s) returned')
    if num_rows > 0:
        #house_ids = pd.read_sql(sql_string,conn)
        house_ids2 = cur.fetchall()
        house_ids = pd.DataFrame(house_ids2,columns=['HouseID'])
        for index, row in house_ids.iterrows(): 
            sql_string=f'''
            INSERT INTO nextpax.outliers(checked_date, HouseID, houses)
            SELECT ol.* FROM
            (SELECT '{check_date}' AS checked_date, '{row['HouseID']}' AS HouseID, 1 AS houses) AS ol
            ON DUPLICATE KEY UPDATE checked_date=ol.checked_date, HouseID=ol.HouseID, houses=ol.houses
            ''' 
            cur.execute(sql_string)
        conn.commit()


dependent_tables = ['amenities','calendars','descriptions','pictures','rates']
for table_name in dependent_tables:
    do_table(table_name)


print(f"\n**** removing any outlier houses from houses table: ****")
sql_string = '''ALTER TABLE `nextpax`.`houses` 
CHANGE COLUMN `HouseID` `HouseID` VARCHAR(50) NOT NULL ,
ADD PRIMARY KEY (`HouseID`)'''
#print('\n' + sql_string)

try:
    exec_result = cur.execute(sql_string)
    #print(f'\n{exec_result} row(s) affected...')
    conn.commit()
except pymysql.Error as Error:
    msg = Error.args[1]
    print('\n' + msg)
    print('\n** (this routine has already removed outlier houses earlier today) **')


sql_string = f'''SELECT HouseID FROM nextpax.outliers WHERE checked_date = '{check_date}' AND houses > 0 AND HouseID IN (SELECT HouseID FROM nextpax.houses where HouseID <> TopParent or TopParent is null)'''
print('\n' + sql_string)
num_rows = cur.execute(sql_string)
print(f'{num_rows} row(s) returned')
houses_deleted = 0
deleted_list = []

if num_rows > 0:
    #house_ids = pd.read_sql(sql_string,conn)
    house_ids2 = cur.fetchall()
    house_ids = pd.DataFrame(house_ids2,columns=['HouseID'])
    for index, row in house_ids.iterrows(): 
    # for row in house_ids: 
        sql_string=f'''DELETE FROM nextpax.houses WHERE HouseID = '{row['HouseID']}'
        ''' 
        #print('\n' + sql_string)
        exec_result = cur.execute(sql_string)
        #print(f'\n{exec_result} row(s) affected...')
        houses_deleted += 1
        deleted_list.append(row.HouseID)

    conn.commit()


print('\n*** run outliers again, this fills out summary table below... ***')
for table_name in dependent_tables:
    do_table(table_name)


field_list = '''checked_date, HouseID, IF(houses,'x','') AS houses, 
IF(amenities,'x','') AS amenities, IF(calendars,'x','') AS calendars, IF(descriptions,'x','') AS descriptions, 
IF(pictures,'x','') AS pictures, IF(rates,'x','') AS rates, IF(taxes,'x','') AS taxes'''
sql_string = f"SELECT {field_list} FROM nextpax.outliers WHERE checked_date = '{check_date}'"
# todays_house_ids = pd.read_sql(sql_string,conn)
todays_house_ids2 = cur.fetchall()
summary_fields = ['checked_date','HouseID','houses','amenities','calendars','descriptions','pictures','rates','taxes']
todays_house_ids = pd.DataFrame(todays_house_ids2,columns=summary_fields)

print(f"\n**** today's {len(todays_house_ids)} outliers (x's indicate HouseID exists in table): ****")
print("<pre><code>")
print(tabulate(todays_house_ids, headers=summary_fields, showindex=False))
print("</code></pre>")
cur.close()
conn.close()
cur = None


print(f"\n{houses_deleted} property IDs deleted.  \nHouses deleted: {', '.join(deleted_list)}")
print(f"\nOutliers report script elapsed time: {datetime.datetime.now() - script_start}")
