import pymysql 
import pandas as pd
from email_warn import send_warning
import datetime
from geopy.geocoders import GoogleV3
import time
from dotenv import load_dotenv
import os
# import warnings
# warnings.simplefilter("error")

load_dotenv()
SOJOURN_HOST = os.getenv('SOJOURN_HOST')
SOJOURN_USER = os.getenv('SOJOURN_USER')
SOJOURN_PWD = os.getenv('SOJOURN_PWD')
GEO_GOOG_API = os.getenv('GEO_GOOG_API')

geolocator = GoogleV3(api_key=GEO_GOOG_API, domain='maps.googleapis.com')

script_start = datetime.datetime.now()

conn = pymysql.connect(host=SOJOURN_HOST, user=SOJOURN_USER, passwd=SOJOURN_PWD, db='sojourn', port=3306, charset='utf8mb4')

print(f"\nUsing database server {SOJOURN_HOST}.\n")

cur = conn.cursor()
cur.execute('USE sojourn')

class module_object:
    pass
# create search_area variable global to this module
_m = module_object()
_m.num_geolocate_calls = 0
_m.continue_run = True
_m.limit_exceeded = ''

warn_list = []
warn_limit = 99

def add_warning(msg):
    if len(warn_list)+1 > warn_limit:
        _m.limit_exceeded = f"\n(Warning Limit Exceeded.)\n"
    else:
        warn_list.append(f"\nWarning No. {len(warn_list)+1}\n{msg}")

# before continuing, check that evolve inventory hasn't been changed > 20%
sql_string = f"""SELECT ev.*, sj.*, 100*ABS(sj.soj_cnt-ev.evolve_cnt)/ev.evolve_cnt AS abs_diff_pct FROM
    (SELECT COUNT(sojournID) as evolve_cnt FROM evolve.places) AS ev,
    (SELECT COUNT(sojournID) as soj_cnt FROM sojourn.places WHERE sojournID LIKE 'FX%') AS sj"""
cur.execute(sql_string)
result = cur.fetchone()
if not result is None:
    print(f"Evolve inventory count:\nEvolve places: {result[0]}; Sojourn-Evolve places: {result[1]}; Percent change: {result[2]}%")
    if result[2] >= 20:
        # change in number of places is greater than 20%
        msg = f"WARNING!!! EVOLVE SOJOURN UPSERT WILL NOT RUN TODAY:\nThere has been a change > 20% in Evolve inventory:\nEvolve places: {result[0]}; Sojourn-Evolve places: {result[1]}; Percent change {result[2]}%"
        add_warning(msg)
        print(msg)
        _m.continue_run = False

def sql_safe_str(value):
    if value is not None:
        sql_safe_str = value.replace("ā","a").replace("ʻ","''").replace("'","''").strip()
        return f"'{sql_safe_str}'"
    else:
        return 'null'

def find_location(lookup_point):
    try:
        # use cached latitude and longitude to return location
        sql_string = f"""SELECT location FROM sojourn.geo_cache
        WHERE latitude = {lookup_point[0]} AND longitude = {lookup_point[1]}
        """
        cur.execute(sql_string)
        result = cur.fetchone()
        if not result is None:
            return result[0]
        else:
            return None
    except:
        return None

def find_sojournID(sojournID):
    try:
        # use cached House information to bypass geolookup
        sql_string = f"""SELECT location, latitude, longitude, city, state, country,
        street, place, zip_code
        FROM sojourn.geo_cache WHERE sojournID = '{sojournID}'"""
        cur.execute(sql_string)
        result = cur.fetchone()
        if not result is None:
            return {'location':result[0], 'latitude':result[1], 'longitude':result[2],
            'city':result[3],'state':result[4],'country':result[5], 'street':result[6],
            'place':result[7], 'zip_code':result[8]}
        else:
            return None
    except:
        return None

def join_safe(list_to_join):
    join_safe = ''
    for item in list_to_join:
        if item is not None:
            join_safe += item
    return join_safe

def do_table(databasename, tablename, command_list):
    # **PLACES
    #sql_string = f'select count(id) AS num_records from evolve.{tablename}'
    sql_string = command_list[0]
    num_rows = 0

    try:
        # see if it exists, then drop it
        cur.execute(sql_string)
        # sojourn_db.{tablename} exists
        num_rows = cur._rows[0][0]
    except pymysql.Error as Error:
        msg = f"Evolve update for sojourn.{tablename} will not be processed.\n\nError: {Error.args}\n\nsql_string: {sql_string}"
        add_warning(msg)
        print('\n' + msg)

    if num_rows > 0:
        # we have {tablename}, let's update main sojourn database
        print(f'\n******* ********* **********       upsert from evolve.{tablename}, {num_rows} rows:')
        print(f"\n\nEVOLVE UPSERT elapsed time: {datetime.datetime.now() - script_start}")

        try:
            for sql_command in command_list[1:]:
                print('\n' + sql_command)
                exec_result = cur.execute(sql_command)
                print(f'\n{exec_result} row(s) affected...')
                conn.commit()
        except pymysql.Error as Error:
            msg = f'{sql_command}\n\n{Error.args[1]}'
            add_warning(msg)
            print('\n' + msg)

    else:
        msg = f"Evolve update for sojourn.{tablename} will not be processed.  The following query returned no records:\n\n{sql_string}"
        add_warning(msg)
        print('\n' + msg)

if _m.continue_run:
    print('\n**** EVOLVE PREPROCESS - CONVERT TO SOJOURN DB ****')

    sql_string = f"""SELECT sojournID, location, latitude, longitude, city, state, street, place, zip_code, country
            FROM evolve.places"""
    cur.execute(sql_string)
    result = cur.fetchall()
    if not result is None:
        print('\nchecking geolocation of places....')
        error_retries = 0
        place_count = 0
        for evolve_place in result:
            place_count += 1
            sojournID = evolve_place[0]
            # print(f'\ngeolocating place {sojournID} - error retries: {error_retries} - num places: {place_count}')
            state = ''
            city = ''
            location = ''
            country = ''
            place = ''
            zip_code = ''
            street = ''
            street_number = ''
            route = ''
            latitude = None
            longitude = None
            geo_success = False
            geo_cached = find_sojournID(sojournID)
            if geo_cached is not None:
                # print('geo cache found...')
                location = geo_cached['location']
                latitude = geo_cached['latitude']
                longitude = geo_cached['longitude']
                city = geo_cached['city']
                state = geo_cached['state']
                country = geo_cached['country']
                street = geo_cached['street']
                place = geo_cached['place']
                zip_code = geo_cached['zip_code']
            else:
                while not geo_success and error_retries < 200:
                    geo_success = False
                    try:
                        housepoint = evolve_place[2], evolve_place[3] 
                        myplace = geolocator.reverse(housepoint, exactly_one=True)
                        _m.num_geolocate_calls += 1
                        geo_success = True
                        if myplace is not None:
                            for add_comp in myplace.raw['address_components']:
                                if add_comp['types'][0] == 'street_number':
                                    street_number = add_comp['short_name']
                                if add_comp['types'][0] == 'route':
                                    route = add_comp['short_name']
                                if add_comp['types'][0] == 'administrative_area_level_1':
                                    state = add_comp['long_name']
                                if add_comp['types'][0] in ['locality','neighborhood']:
                                    city = add_comp['long_name']
                                if add_comp['types'][0] == 'country':
                                    country = add_comp['short_name']
                                if add_comp['types'][0] == 'postal_code':
                                    zip_code = add_comp['short_name']

                            #location = myplace.address
                            if evolve_place[2] is None or evolve_place[2] != myplace.point.latitude:
                                latitude = myplace.point.latitude
                            if evolve_place[3] is None or evolve_place[3] != myplace.point.longitude:
                                longitude = myplace.point.longitude

                        #street = f'{street_number} {route}' if street_number != '' else f'{route}'
                        street = f'{route}'
                        place = f'{country}, {state}, {city}'
                        location = place

                    except:
                        print(f'geo coding failure on {sojournID}: wait half a second, try again...')
                        time.sleep(.5)
                        error_retries += 1
                        if error_retries >= 200:
                            print('stop')

                        print(f"Preprocessing elapsed time: {datetime.datetime.now() - script_start}")
                        print(f'error retries: {error_retries}')

            # print(f'{location}')
            # print(f'{street}, {place}')
            if not latitude is None:
                sql_string = f"""update evolve.places set location = {sql_safe_str(location)}, latitude = {round(latitude,13)}, 
                    longitude = {round(longitude,12)}, city = {sql_safe_str(city)}, state = {sql_safe_str(state)}, 
                    street = {sql_safe_str(street)}, place = {sql_safe_str(place)},
                    zip_code = {sql_safe_str(zip_code)}, country = {sql_safe_str(country)} where sojournID = '{sojournID}'"""
                # print(f'\n{sql_string}')
                try:
                    exec_result = cur.execute(sql_string)
                    # print(f'\n{exec_result} row(s) affected...')
                    conn.commit()
                except pymysql.Error as Error:
                    msg = f'{sql_string}\n\n{Error.args[1]}'
                    add_warning(msg)
                    print('\n' + msg)

    print(f'\n# of geolocate calls: {_m.num_geolocate_calls}')
    print(f'\n# of error retries: {error_retries}')
    print(f"\nPreprocessing elapsed time: {datetime.datetime.now() - script_start}")

    print('\n ****** upsert into the sojourn api database ... ******')
    db_name_list = ['sojourn']
    for db_name in db_name_list:

        print(f'\n**** EVOLVE UPSERT into {db_name.upper()} production database... ****')

        tables = {'places':[
            f'select count(id) AS num_records FROM evolve.places where rate > 2;',
            f"SET SQL_SAFE_UPDATES = 0;",
            f'''DELETE FROM sojourn.places WHERE sojournID like 'FX%';
            '''
            ,
            f'''INSERT INTO sojourn.places (sojournID, property_name, PMS, pm_id, country, place, zip_code, min_stay, 
            min_persons, max_persons, min_children, number_of_pets, house_type, latitude, longitude, location, picture, 
            currency, rate, wifi, iron, air_conditioning, hot_tub, pool, washer, free_parking, TV, dryer, breakfast, 
            hangers, hair_dryer, high_chair, smoke_alarm, pets_allowed, kitchen, heating, fireplace, laptop_friendly, 
            crib, carbon_monoxide_alarm, gym, ski_in_out, num_beds, num_bedrooms, num_bathrooms, average_rating, star_rating, last_updated)
            SELECT sojournID, property_name, 'Evolve', 'EV', country, place, zip_code, min_stay, min_persons, max_persons, min_children,
            number_of_pets, house_type, latitude, longitude, location, picture, currency, rate * 1.1, wifi, iron,
            air_conditioning, hot_tub, pool, washer, free_parking, TV, dryer, breakfast, hangers, hair_dryer, high_chair,
            smoke_alarm, pets_allowed, kitchen, heating, fireplace, laptop_friendly, crib, carbon_monoxide_alarm,
            gym, ski_in_out, num_beds, num_bedrooms, num_bathrooms, average_rating, 0, '{script_start}' FROM evolve.places where rate > 2;
            '''
            ,
            f'''UPDATE sojourn.places SET num_reviews = 0 where num_reviews is null;
            ''']
            ,'rates':[
            f'select count(id) AS num_records FROM evolve.daily_rates;',
            f'''DELETE FROM sojourn.rates WHERE sojournID like 'FX%';''',
            f'''INSERT INTO sojourn.rates (id, sojournID, from_date, till_date, duration, amount, currency, persons, weekdays, minimum_stay,
            maximum_stay, extra_person_fee_amount, extra_person_fee_currency)
            SELECT concat(sojournID,'.',id), sojournID, day, date_add(day, INTERVAL 1 DAY), 1, amount, 'USD',
            '', 1, min_stay, 365, 0, null FROM evolve.daily_rates;
            '''
            ]
            ,'calendars':[
            f'''select count(cal.id) AS num_records FROM evolve.calendars as cal, evolve.daily_rates as rates
            where cal.sojournID = rates.sojournID AND cal.calendar_date = rates.day;''',
            f'''DELETE FROM sojourn.calendars WHERE sojournID like 'FX%';
            '''
            ,
            f'''INSERT INTO sojourn.calendars
            (id, sojournID, calendar_date, quantity, arrival_allowed, departure_allowed, minimum_stay, maximum_stay, days_available)
            SELECT concat(cal.sojournID,'.',cal.id), cal.sojournID, cal.calendar_date, cal.quantity, cal.arrival_allowed,
            cal.departure_allowed, rates.min_stay, 60, days_available FROM evolve.calendars as cal, evolve.daily_rates as rates
            where cal.sojournID = rates.sojournID AND cal.calendar_date = rates.day;
            ''']
            ,'descriptions':[
            f'''select count(id) AS num_records FROM evolve.descriptions WHERE sojournID like 'FX%';''',
            f'''DELETE FROM sojourn.descriptions WHERE sojournID like 'FX%';
            '''
            ,
            f'''INSERT INTO sojourn.descriptions (id, sojournID, language_code, description, description_type)
            SELECT concat(sojournID,'.',id), sojournID, language_code, description, description_type
            FROM evolve.descriptions WHERE sojournID like 'FX%' and description <> 'null';
            ''']
            ,'pictures':[
            f'select count(id) AS num_records FROM evolve.pictures;',
            f'''DELETE FROM sojourn.pictures WHERE sojournID like 'FX%';
            '''
            ,
            f'''INSERT INTO sojourn.pictures (id, sojournID, file_url, picture_type)
            SELECT concat(sojournID,'.',id), sojournID, file_url, "Other" FROM evolve.pictures;
            ''']
            ,'reviews':[
            f'select count(id) AS num_records FROM evolve.reviews;',
            f'''DELETE FROM sojourn.reviews WHERE sojournID like 'FX%';
            '''
            ,
            f'''INSERT INTO sojourn.reviews (sojournID, booking, rating, rating_reporting, created_at_api, review_detail,
            review_source, reviewed_by, review_summary, travel_date)
            SELECT sojournID, booking, rating, rating_reporting, created_at_api, review_detail, review_source,
            reviewed_by, review_summary, travel_date FROM evolve.reviews;
            '''
            ,
            f'''UPDATE sojourn.places p
            INNER JOIN (SELECT sojournID, count(sojournID) countReviews FROM sojourn.reviews GROUP BY sojournID) t
            ON t.sojournID = p.sojournID SET p.num_reviews = t.countReviews;
            '''
            ,
            f'''UPDATE sojourn.places SET num_reviews = 0 WHERE num_reviews is null;
            ''']
        }

        # ie., to run one table only: 
        # do_table(db_name, 'pictures',tables['pictures'])

        [do_table(db_name, x, tables[x]) for x in tables]

    sql_command = f'''insert into sojourn.geo_cache(sojournID, country, city, state, street, place, zip_code, latitude, longitude, location)
            select pl.* from
            (select sojournID, country, city, state, street, place, zip_code, latitude, longitude, location from evolve.places) as pl
            on duplicate key update sojournID=pl.sojournID, country=pl.country, city=pl.city, state=pl.state, street=pl.street,
            place=pl.place, zip_code=pl.zip_code, latitude=pl.latitude, longitude=pl.longitude;
            '''
    print('\n**** upserting from preprocess:  evolve.places --> sojourn.geo_cache ****')
    print('\n' + sql_command)
    try:
        exec_result = cur.execute(sql_command)
        print(f'\n{exec_result} row(s) affected...')
        conn.commit()
    except pymysql.Error as Error:
            msg = f'{sql_command}\n\n{Error.args[1]}'
            add_warning(msg)
            print('\n' + msg)


    cur.close()
    conn.close()

    cur = None

    print(f"\n\nEVOLVE UPSERT elapsed time: {datetime.datetime.now() - script_start}")


if len(warn_list) > 0:
    message = f'\nevolve_sojourn_upsert.py\n\n{len(warn_list)} Warnings...\n{_m.limit_exceeded}'+'\n'.join(warn_list)
    print(message)
    send_warning(message)
else:
    print('\nNo Warnings...')
