import xml.etree.ElementTree as ET
from datetime import datetime
from pandas import Series, DataFrame
import pandas as pd
import csv
import os
from sqlalchemy import create_engine
import pymysql 
from sqlalchemy.types import Integer, Text, String, Numeric, DateTime, Boolean, Date
from tabulate import tabulate
import random
from np_constants import pms_list

start_time = datetime.now()

engine = create_engine("mysql+pymysql://{user}:{passwd}@{host}/{db}".format(host='35.192.116.102', user='henry', passwd='finley', db='nextpax', port=3306))

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
# change working directory to same as current python file
# if local machine
os.chdir(dname) 
dname = 'Xml'
os.chdir(dname) 
###############################################################
house_rates = []

print('\nReading Rates: ')

for pm_code in pms_list:
        xml_filename = 'paxgenerator_rates_{pms}.xml'.format(pms=pm_code)
        xmlp = ET.XMLParser(encoding="utf-8")
        tree = ET.parse(xml_filename,parser=xmlp)
        root = tree.getroot()
        print(f"{pm_code} {len(root)}")
        property_mgmt_system = root.find('PropertyManagementSystem').text

        for rates in root.iter('Rates'):
                house_id = rates.find('HouseID').text 
                property_mgr = rates.find('PropertyManager').text

                for rate in rates.iter('Rate'):
                        # add property attributes to rate
                        rate.attrib['PropertyManagementSystem'] = property_mgmt_system
                        rate.attrib['HouseID'] = house_id
                        rate.attrib['PropertyManager'] = property_mgr

                        house_rates.append(rate.attrib)

        xmlp = None
        tree = None
        root = None
        property_mgmt_system = None


rates_df = DataFrame(house_rates)

# cols = rates_df.columns.tolist() 
# cols = cols[-5:] + cols[:-5]
cols = ['PropertyManagementSystem', 'HouseID', 'PropertyManager', 'from', 'till', 'duration', 'amount', 
        'currency', 'persons', 'weekdays', 'minimumStay', 'maximumStay', 'extraPersonFeeAmount', 
        'extraPersonFeeCurrency']

rates_df = rates_df[cols]
rates_df.index = rates_df.index.rename('id')
rates_df = rates_df.rename(columns={"from": "fromDate", "till": "tillDate"})

fieldschema={
        "PropertyManagementSystem": String(50), "HouseID": String(50), "PropertyManager": String(50),
        "fromDate":  Date, "tillDate": Date, 'duration': Integer, 'amount': Numeric(10,2), 'currency': String(50),
        'persons': String(50), 'weekdays': String(50), 'minimumStay': Integer, 'maximumStay': Integer,
        'extraPersonFeeAmount': Numeric(10,2), 'extraPersonFeeCurrency': String(50)
}

# rate amounts are in cents (integer)
rates_df['amount'] = rates_df['amount'].fillna(0).astype(int)/100
rates_df['extraPersonFeeAmount'] = rates_df['extraPersonFeeAmount'].fillna(0).astype(int)/100

rates_df.to_sql('rates', con = engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema)

print('\n# of Rates:')
print(rates_df.count()['amount'])
print('\n# by Property Mgr:')
# print(rates_df.groupby('PropertyManagementSystem').count()['amount'])
# print('\nRates for ',pms_list,':\n')
# print(rates_df)

print(tabulate(pd.DataFrame(rates_df.groupby('PropertyManagementSystem')['amount'].count()), headers=['PropertyManagementSystem','Count']))

print('\nRates for ',pms_list,':')

num_rows = len(rates_df)
num_samples = 45

random_index = []

i = 0
while i < num_samples:
    random_int = random.randint(0,num_rows-1)
    if random_int not in random_index:
        random_index.append(random_int)
        i += 1

random_index.sort()

print(f'(randomly sampling {num_samples} rates out of {num_rows}...)\nrow#')

# print(f'(sampling every {int(len(rates_df)*0.033)} rows...)\n')

summary_fields = ['PropertyManagementSystem','HouseID','fromDate','tillDate','duration','amount','currency','minimumStay']
print("<pre><code>")
print(tabulate(rates_df.iloc[random_index][summary_fields], 
    headers=summary_fields, showindex=True))
print("</code></pre>")
# print(tabulate(rates_df[rates_df.index % int(len(rates_df)*0.033) == 0][summary_fields], 
#     headers=summary_fields, showindex=False))



end_time = datetime.now()
print('Duration: {}'.format(end_time - start_time))
