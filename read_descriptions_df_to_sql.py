import xml.etree.ElementTree as ET
from datetime import datetime
from pandas import Series, DataFrame
import pandas as pd
import csv
import os
import re
from sqlalchemy import create_engine
import pymysql 
#import mysql.connector
from sqlalchemy.types import Integer, Text, String, Numeric, DateTime, Boolean, UnicodeText
from tabulate import tabulate
import random
from np_constants import pms_list


start_time = datetime.now()

engine = create_engine("mysql+pymysql://{user}:{passwd}@{host}/{db}?charset=utf8mb4".format(host='35.192.116.102', user='henry', passwd='finley', db='nextpax', port=3306))
#engine = create_engine("mysql+mysqlconnector://{user}:{passwd}@{host}/{db}?charset=utf8mb4".format(host='35.192.116.102', user='henry', passwd='finley', db='nextpax', port=3306))

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname) 
# change working directory to same as current python file
# if local machine
dname = 'Xml'
os.chdir(dname) 

###############################################################
house_descriptions = []
#var XML_RETURN = new RegExp('(&#13;|&#13)', 'g')


for pm_code in pms_list:
    xml_filename = 'paxgenerator_house_descriptions_{pms}.xml'.format(pms=pm_code)
    xmlp = ET.XMLParser(encoding="utf-8")
    tree = ET.parse(xml_filename,parser=xmlp)
    root = tree.getroot()
    partner = root.find('Partner').text

    for descriptions in root.iter('Descriptions'):
        house_id = descriptions.find('HouseID').text 
        for description in descriptions.iter('Description'):

            # add property attributes to rate
            description.attrib['Partner'] = partner
            description.attrib['HouseID'] = house_id
            # if house_id == '62414804':
            # # if house_id == '6465273':

            #     print('stop')

            i = 0
            while i < len(description):
                try:
                    if description[i].text is not None:
                        # first remove tabs, carriage returns, and non-breaking spaces
                        first_pass = description[i].text.replace('\r','').replace('\t','').replace('\xa0','')
                        # then remove double and triple line feeds, if too many
                        if first_pass.count('\n\n') > 10:
                            first_pass = first_pass.replace('\n\n','\n').replace('\n\n\n','\n')

                        if description[i].text == 'GetDescriptionsPlainText: Error - The description is blank':
                            first_pass = ''

                    description.attrib[description[i].tag] = None if description[i].text is None else first_pass
                    # description.attrib[description[i].tag] = None if description[i].text is None else description[i].text.replace('\r','').replace('\t','').replace('\xa0','').replace('\n\n','\n').replace('\n\n\n','\n')
                except:
                    print('error in ln 58 of read_descriptions...')
                i += 1

            house_descriptions.append(description.attrib)

    xmlp = None
    tree = None
    root = None
    partner = None

descriptions_df = DataFrame(house_descriptions)
descriptions_df.index = descriptions_df.index.rename('id')
descriptions_df = descriptions_df.rename(columns={"Language": "LanguageCode", "Text":"Description", "Type": "TypeID"})

fieldschema={
	'Partner': String(50), 'HouseID': String(50), 'LanguageCode': String(5), 'Description': UnicodeText, 'TypeID': String(5)
}

#descriptions_df[2:4].to_sql('descriptions', con = engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema)
descriptions_df.to_sql('descriptions', con = engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema)

print('\n# of Descriptions:')
print(descriptions_df.count()['Description'])
print('\n# by Property Mgr:')
# print(descriptions_df.groupby('Partner').count()['Description'])
# print('\Descriptions for ',pms_list,':\n')
# print(descriptions_df)

print(tabulate(pd.DataFrame(descriptions_df.groupby('Partner')['Description'].count()), headers=['Partner','Count']))


print('\nDescriptions for ',pms_list,':')

num_rows = len(descriptions_df)

num_samples = 5

random_index = []

i = 0
while i < num_samples:
    random_int = random.randint(0,num_rows-1)
    if random_int not in random_index:
        random_index.append(random_int)
        i += 1

random_index.sort()

print(f'(randomly sampling {num_samples} descriptions out of {num_rows}...)\nrow#')

summary_fields = ['Partner','HouseID','Description']
print("<pre><code>")
print(tabulate(descriptions_df.iloc[random_index][summary_fields],headers=summary_fields,tablefmt='plain',showindex=True))
print("</code></pre>")
end_time = datetime.now()
print('Duration: {}'.format(end_time - start_time))
