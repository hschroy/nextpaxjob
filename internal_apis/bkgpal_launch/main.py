from fastapi import Security, FastAPI
import subprocess
import os
# import uvicorn

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
# change working directory to same as current python file
os.chdir(dname) 

app = FastAPI(
    title="joblauncher",
    description="Welcome to the joblauncher",
    version="1.0.0",
)

@app.post('/launchbkgpal')
def launchbkgpal():
    # docker volume 
    # -v configure 
    # https://docs.docker.com/storage/volumes/
    if os.path.exists("/.dockerenv"):
        # is in docker container
        daily_path = "daily"
    else:
        daily_path = "/home/henry_schroy/daily/nextpaxjob"


    if os.path.exists(f"{daily_path}/bkgpal.is.running"):
        # launcher is already running, leave without launching
        return 1
    else:
        run_result = subprocess.Popen(["bash", f"{daily_path}/bkgpal_sendlog.sh"])
        # run_result = subprocess.Popen(["bash", f"{daily_path}/henry_test_sendlog.sh"])
        return 0

# if __name__ == "__main__":
#       uvicorn.run(app, host="127.0.0.1", port=8000)
