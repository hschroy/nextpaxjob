from fastapi import Security, FastAPI
import subprocess
from dotenv import load_dotenv
import os
load_dotenv()
import pymysql 
import pandas as pd
import time
from time import time
from pydantic import BaseModel

import uvicorn

class Info(BaseModel):
    id : int
    name : str


SOJOURN_HOST = os.getenv('SOJOURN_HOST')
SOJOURN_USER = os.getenv('SOJOURN_USER')
SOJOURN_PWD = os.getenv('SOJOURN_PWD')

conn = pymysql.connect(host=SOJOURN_HOST, user=SOJOURN_USER, passwd=SOJOURN_PWD, db='sojourn', port=3306)
print(f"\nUsing database server {SOJOURN_HOST}.\n")

cur = conn.cursor()
cur.execute('USE sojourn')

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
# change working directory to same as current python file
os.chdir(dname) 

app = FastAPI(
    title="operations_api",
    description="Welcome to the operations_api",
    version="1.0.0",
)

def get_this_table(tablename, clause):

    sql_string = f"""select * from sojourn.{tablename} {clause}"""

    tic = time()
    exec_result = cur.execute(sql_string)
    toc = time()

    fields = [field_md[0] for field_md in cur.description]
    result = {"results":f"{exec_result}","data":[dict(zip(fields,row)) for row in cur.fetchall()]}

    return result

@app.post('/managers')
def managers(info : Info):
    return get_this_table('managers','order by channel_mgr, pm_name')

    # sql_string = f"""select * from sojourn.managers order by channel_mgr, pm_name"""

    # tic = time()
    # exec_result = cur.execute(sql_string)
    # toc = time()

    # fields = [field_md[0] for field_md in cur.description]
    # result = {"results":f"{exec_result}","data":[dict(zip(fields,row)) for row in cur.fetchall()]}

    # return result

    #select * from sojourn.dedupes order by 1
@app.post('/dedupes')
def dedupes(info : Info):
    return get_this_table('dedupes','order by 1')

    # sql_string = f"""select * from sojourn.dedupes order by 1"""

    # tic = time()
    # exec_result = cur.execute(sql_string)
    # toc = time()

    # fields = [field_md[0] for field_md in cur.description]
    # result = {"results":f"{exec_result}","data":[dict(zip(fields,row)) for row in cur.fetchall()]}

    # return result


@app.post('/account_pms')
def account_pms(info : Info):
    return get_this_table('account_pms','order by userid, pm_id')

    # sql_string = f"""select userid, pm_id from sojourn.account_pms order by userid, pm_id"""

    # tic = time()
    # exec_result = cur.execute(sql_string)
    # toc = time()

    # fields = [field_md[0] for field_md in cur.description]
    # result = {"results":f"{exec_result}","data":[dict(zip(fields,row)) for row in cur.fetchall()]}

    # return result

@app.post('/star_ratings')
def star_ratings(info : Info):
    return get_this_table('star_ratings','order by star_rating desc')

    # sql_string = f"""select * from sojourn.star_ratings order by star_rating desc"""

    # tic = time()
    # exec_result = cur.execute(sql_string)
    # toc = time()

    # fields = [field_md[0] for field_md in cur.description]
    # result = {"results":f"{exec_result}","data":[dict(zip(fields,row)) for row in cur.fetchall()]}

    # return result


@app.post('/reservations')
def reservations(info : Info):
    return get_this_table('reservations','order by booking_date desc')

    # sql_string = f"""select * from sojourn.reservations order by booking_date desc"""

    # tic = time()
    # exec_result = cur.execute(sql_string)
    # toc = time()

    # fields = [field_md[0] for field_md in cur.description]
    # result = {"results":f"{exec_result}","data":[dict(zip(fields,row)) for row in cur.fetchall()]}

    # return result

@app.post('/meta_tags')
def meta_tags(info : Info):
    return get_this_table('meta_tags','order by sojournID')

    # sql_string = f"""select * from sojourn.meta_tags order by sojournID"""

    # tic = time()
    # exec_result = cur.execute(sql_string)
    # toc = time()

    # fields = [field_md[0] for field_md in cur.description]
    # result = {"results":f"{exec_result}","data":[dict(zip(fields,row)) for row in cur.fetchall()]}

    # return result

@app.post('/hash_alias')
def hash_alias(info : Info):
    return get_this_table('hash_alias','')

    # sql_string = f"""select * from sojourn.hash_alias"""

    # tic = time()
    # exec_result = cur.execute(sql_string)
    # toc = time()

    # fields = [field_md[0] for field_md in cur.description]
    # result = {"results":f"{exec_result}","data":[dict(zip(fields,row)) for row in cur.fetchall()]}

    # return result


@app.post('/users')
def users(info : Info):
    return get_this_table('users','order by userid')

    # sql_string = f"""select * from sojourn.users order by userid"""

    # tic = time()
    # exec_result = cur.execute(sql_string)
    # toc = time()

    # fields = [field_md[0] for field_md in cur.description]
    # result = {"results":f"{exec_result}","data":[dict(zip(fields,row)) for row in cur.fetchall()]}

    # return result


if __name__ == "__main__":
      uvicorn.run(app, host="127.0.0.1", port=8000)
