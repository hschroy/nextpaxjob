#!/bin/bash
echo 'restarting docker container sojournapi on sojournapi-20200905:'
gcloud beta compute ssh --zone "us-central1-a" "sojournapi-20200905"  --project "sojourn-api-development" --command="docker restart sojournapi"

echo 'restarting docker container sojournapi on sojournapi-dev2:'
gcloud beta compute ssh --zone "us-central1-a" "sojournapi-dev2"  --project "sojourn-api-development" --command="docker restart sojournapi"
