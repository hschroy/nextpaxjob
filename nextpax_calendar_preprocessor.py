import pymysql 
import pandas as pd
import datetime
import time
from dotenv import load_dotenv
import os
load_dotenv()

# debug mode run: NEXTPAX calendar preprocess elapsed time: 8:57:31.550103

SOJOURN_HOST = os.getenv('SOJOURN_HOST')
SOJOURN_USER = os.getenv('SOJOURN_USER')
SOJOURN_PWD = os.getenv('SOJOURN_PWD')

script_start = datetime.datetime.now()

conn = pymysql.connect(host=SOJOURN_HOST, user=SOJOURN_USER, passwd=SOJOURN_PWD, db='sojourn', port=3306, charset='utf8mb4')

cur = conn.cursor()
cur.execute('USE sojourn')
print(f"\n\nNEXTPAX calendar preprocess elapsed time: {datetime.datetime.now() - script_start}")

try:
    run_this = f"""ALTER TABLE `sojourn_db`.`calendars` 
        DROP INDEX `ix_calendars_id` ,
        ADD INDEX `ix_calendars_id` (`sojournID` ASC, `calendar_date` ASC);
        ;"""
    print(run_this)
    exec_result = cur.execute(run_this)
    print(f'\n{exec_result} row(s) returned...')
    print(f"\nNEXTPAX calendar preprocess elapsed time: {datetime.datetime.now() - script_start}")
except:
    print('failed...')
    pass

sql_string = f"""SELECT sojournID, calendar_date, (quantity+arrival_allowed>=2) as available
FROM sojourn_db.calendars order by 1, 2 desc"""
print(sql_string)
print('\n\nchecking availability matrix....')
exec_result = cur.execute(sql_string)
print(f'\n{exec_result} row(s) returned...')
print(f"\nNEXTPAX calendar preprocess elapsed time: {datetime.datetime.now() - script_start}")

result = cur.fetchall()
if not result is None:
    place_count = 0
    this_place = ''
    last_place = this_place
    max_count = len(result)
    counter = 0
    for place_date in result:
        counter += 1
        this_date = place_date[1]
        this_allowed = place_date[2]
        this_place = place_date[0]
        if this_place != last_place:
            # new sojournID
            place_count += 1
            last_place = this_place
            days_avail = 0

        if this_allowed:
            days_avail += 1
        else:
            days_avail = 0

        sql_string = f"""update sojourn_db.calendars set days_available = {days_avail} where sojournID = '{this_place}' and calendar_date = '{this_date}'"""
        # print(f'\n{sql_string}')
        #print(f'{counter} of {max_count}:  {datetime.datetime.now() - script_start}:  {sql_string}')
        exec_result = cur.execute(sql_string)
        # print(f'\n{exec_result} row(s) affected...')
        conn.commit()


print(f"\nNEXTPAX calendar preprocess elapsed time: {datetime.datetime.now() - script_start}")


