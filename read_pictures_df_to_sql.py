import xml.etree.ElementTree as ET
from datetime import datetime
from pandas import Series, DataFrame
import pandas as pd
import csv
import os
from sqlalchemy import create_engine
import pymysql 
from sqlalchemy.types import Integer, Text, String, Numeric, DateTime, Boolean
from tabulate import tabulate
import random
from np_constants import pms_list

start_time = datetime.now()

engine = create_engine("mysql+pymysql://{user}:{passwd}@{host}/{db}".format(host='35.192.116.102', user='henry', passwd='finley', db='nextpax', port=3306))

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
# change working directory to same as current python file
# if local machine
os.chdir(dname) 

dname = 'Xml'
os.chdir(dname) 
###############################################################
house_pictures = []

for pm_code in pms_list:
    xml_filename = 'paxgenerator_house_pictures_{pms}.xml'.format(pms=pm_code)
    xmlp = ET.XMLParser(encoding="utf-8")
    tree = ET.parse(xml_filename,parser=xmlp)
    root = tree.getroot()
    partner = root.find('Partner').text

    for pictures in root.iter('Pictures'):
        house_id = pictures.find('HouseID').text
        for picture in pictures.iter('Picture'):
            # add property attributes to picture
            picture.attrib['Partner'] = partner
            picture.attrib['HouseID'] = house_id
            picture.attrib['pms'] = pm_code
            i = 0
            while i < len(picture):
                picture.attrib[picture[i].tag] = picture[i].text
                i += 1

            house_pictures.append(picture.attrib)

    xmlp = None
    tree = None
    root = None
    partner = None


pictures_df = DataFrame(house_pictures)

# basically the images are redirected, to save some time you can use the following URL to skip our internal redirect:
# https://img.nextpax.com/lr/62414795/94d7abeb-901e-4936-b09a-4e792d5fb736.jpg
# redirects to
# https://img.nextpax.com/core/94d/94d7abeb/901e/4936/b09a/4e792d5fb736/94d7abeb-901e-4936-b09a-4e792d5fb736.jpg
# PHP example:
# $redirect = 'https://img.nextpax.com/core/' . substr($basename, 0, 3) . '/' . str_replace('-', '/', substr($basename, 0, strpos($basename, '.'))) . '/' . $basename;
# so basically we ignore the whole /lr/62414795/ part and only use the basename 94d7abeb-901e-4936-b09a-4e792d5fb736.jpg.
# We take the first 3 characters, then replace all dashes with slashes and finally add the full basename again.
# 'https://img.nextpax.com/core/' + basename[0:3] + '/' + basename[0:basename.find('.')].replace('-','/') + '/' + basename

# redirect = lambda x : 'https://img.nextpax.com/core/' + x[0:3] + '/' + x[0:x.find('.')].replace('-','/') + '/' + x
redirect = lambda x : 'https://img.sojournapi.com/core/' + x[0:3] + '/' + x[0:x.find('.')].replace('-','/') + '/' + x
pictures_df['Name'] = pictures_df['Name'].apply(redirect)

# pictures_df['Name'][8044]
# '94d7abeb-901e-4936-b09a-4e792d5fb736.jpg'
# redirect(pictures_df['Name'][8044])
# 'https://img.nextpax.com/core/94d/94d7abeb/901e/4936/b09a/4e792d5fb736/94d7abeb-901e-4936-b09a-4e792d5fb736.jpg'

# pictures_df['Name'] = 'https://img.nextpax.com/' + pictures_df['pms'] + '/' + pictures_df['HouseID'] + '/' + pictures_df['Name']

pictures_df.index = pictures_df.index.rename('id')
pictures_df = pictures_df.rename(columns={"Name": "FileUrl", "Type": "TypeID"})

fieldschema={
	'Partner': String(50), 'HouseID': String(50), 'FileUrl': Text, 'TypeID': String(5)
}

cols = ['Partner', 'HouseID', 'FileUrl', 'TypeID']
pictures_df = pictures_df[cols]

pictures_df.to_sql('pictures', con = engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema)

print('\n# of Pictures:')
print(pictures_df.count()['FileUrl'])
print('\n# by Property Mgr:')
# print(pictures_df.groupby('Partner').count()['FileUrl'])
# print('\nPictures for ',pms_list,':\n')
# print('(a sampling 1/128)')
# for i in range(0,pictures_df['FileUrl'].count()-1,128):
#     print(pictures_df['FileUrl'][i])


print(tabulate(pd.DataFrame(pictures_df.groupby('Partner')['FileUrl'].count()), headers=['Partner','Count']))

print('\nPictures for ',pms_list,':')


num_rows = len(pictures_df)
num_samples = 45

random_index = []

i = 0
while i < num_samples:
    random_int = random.randint(0,num_rows-1)
    if random_int not in random_index:
        random_index.append(random_int)
        i += 1

random_index.sort()

print(f'(randomly sampling {num_samples} pictures out of {num_rows}...)\nrow#')

# print(f'(sampling every {int(len(pictures_df)*0.033)} rows...)\n')

summary_fields = ['Partner','HouseID','FileUrl','TypeID']

image_prefix = '<p><img src="'
image_postfix = '" alt="" width="500"></p>'
pictures_df['FileUrl'] = pictures_df['FileUrl'] + image_prefix + pictures_df['FileUrl'] + image_postfix

# print("<pre><code>")
print(tabulate(pictures_df.iloc[random_index][summary_fields], 
    headers=summary_fields, showindex=True))
# print("</code></pre>")
# print(tabulate(pictures_df[pictures_df.index % int(len(pictures_df)*0.033) == 0][summary_fields], 
#     headers=summary_fields, showindex=False))


end_time = datetime.now()
print('Duration: {}'.format(end_time - start_time))
