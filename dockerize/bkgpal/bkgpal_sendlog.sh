#!/bin/bash

# PATH=$PATH:/home/henry_schroy/google-cloud-sdk/bin

# if [ -f /.dockerenv ]; then
echo "Running $0 inside a Docker container"
cd /usr/src/;
# else
#     echo "$0 (Not running inside a Docker container)"
#     cd /home/henry_schroy/daily/nextpaxjob/dockerize/bkgpal;
#     source /home/henry_schroy/daily/nextpaxjob/dockerize/venv/bin/activate;
# fi

if [ -f "bkgpal.is.running" ]; then
    echo "bkgpal extract is running already!"
    exit 1
else
    touch "bkgpal.is.running";
fi


# rm bkgpal_job.log;
if [ -f "bkgpal_job.log" ]; then
    rm -f bkgpal_job.log;
fi

(time bash bkgpal_job.sh) > bkgpal_job.log 2>&1

# if [ -f /.dockerenv ]; then
#     echo "Running $0 inside a Docker container"
# else
#     echo "$0 (Not running inside a Docker container)"
#     cd /home/henry_schroy/daily/nextpaxjob/dockerize/bkgpal;
#     source /home/henry_schroy/daily/nextpaxjob/dockerize/venv/bin/activate;
# fi


python send_bkgpal_email.py;

if [ -f "bkgpal.is.running" ]; then
    echo "Removing bkgpal.is.running file"
    rm -f "bkgpal.is.running";
fi
