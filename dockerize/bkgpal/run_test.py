import pymysql 
import pandas as pd
from enum import Enum
from email_warn import send_warning
import datetime
from geopy.geocoders import GoogleV3
import time
#from time import time
from dotenv import load_dotenv
import os
load_dotenv()
SOJOURN_HOST = os.getenv('SOJOURN_HOST')
SOJOURN_USER = os.getenv('SOJOURN_USER')
SOJOURN_PWD = os.getenv('SOJOURN_PWD')
GEO_GOOG_API = os.getenv('GEO_GOOG_API')

geolocator = GoogleV3(api_key=GEO_GOOG_API, domain='maps.googleapis.com')

script_start = datetime.datetime.now()
import_date = datetime.datetime.now().strftime("%Y-%m-%d")

conn = pymysql.connect(host=SOJOURN_HOST, user=SOJOURN_USER, passwd=SOJOURN_PWD, db='sojourn', port=3306)

print(f"\nUsing database server {SOJOURN_HOST}.\n")

cur = conn.cursor()
cur.execute('USE sojourn')

class module_object:
    pass
# create search_area variable global to this module
_m = module_object()
_m.num_geolocate_calls = 0
_m.continue_run = True
_m.limit_exceeded = ''

warn_list = []
warn_limit = 8

def add_warning(msg):
    if len(warn_list)+1 > warn_limit:
        _m.limit_exceeded = f"\n(Warning Limit Exceeded.)\n"
    else:
        warn_list.append(f"\nWarning No. {len(warn_list)+1}\n{msg}")

for i in range(1, 11):
    add_warning(f'It happened! {i}')

if len(warn_list) > 0:
    message = f'\n{len(warn_list)} Warnings...\n{_m.limit_exceeded}'+'\n'.join(warn_list)
    print(message)
    send_warning(message)
else:
    print('\nNo Warnings...')
