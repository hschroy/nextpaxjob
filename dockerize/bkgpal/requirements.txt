certifi==2022.12.7
charset-normalizer==3.0.1
geographiclib==2.0
geopy==2.3.0
idna==3.4
numpy==1.24.1
pandas==1.5.3
PyMySQL==1.0.2
python-dateutil==2.8.2
python-dotenv==0.21.1
pytz==2022.7.1
requests==2.28.2
six==1.16.0
socketlabs==0.1.1
socketlabs-injectionapi==1.4.0
urllib3==1.26.14
