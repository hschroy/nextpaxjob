import pymysql 
import pandas as pd
from enum import Enum
from email_warn import send_warning
import datetime
from geopy.geocoders import GoogleV3
import time
#from time import time
from dotenv import load_dotenv
import os
load_dotenv()
SOJOURN_HOST = os.getenv('SOJOURN_HOST')
SOJOURN_USER = os.getenv('SOJOURN_USER')
SOJOURN_PWD = os.getenv('SOJOURN_PWD')
GEO_GOOG_API = os.getenv('GEO_GOOG_API')

geolocator = GoogleV3(api_key=GEO_GOOG_API, domain='maps.googleapis.com')

script_start = datetime.datetime.now()
import_date = datetime.datetime.now().strftime("%Y-%m-%d")

conn = pymysql.connect(host=SOJOURN_HOST, user=SOJOURN_USER, passwd=SOJOURN_PWD, db='sojourn', port=3306)

print(f"\nUsing database server {SOJOURN_HOST}.\n")

cur = conn.cursor()
cur.execute('USE sojourn')

class module_object:
    pass
# create search_area variable global to this module
_m = module_object()
_m.num_geolocate_calls = 0
_m.continue_run = True
_m.limit_exceeded = ''

warn_list = []
warn_limit = 99

def add_warning(msg):
    if len(warn_list)+1 > warn_limit:
        _m.limit_exceeded = f"\n(Warning Limit Exceeded.)\n"
    else:
        warn_list.append(f"\nWarning No. {len(warn_list)+1}\n{msg}")


# remove all places marked INACTIVE
sql_string = f"""DELETE a from bkgpal_evolve.places a
    LEFT JOIN bkgpal_evolve.listing_status b
    ON a.sojournID = b.sojournID 
    WHERE  b.activate = 0;"""
print('\nremove all places marked INACTIVE:\n\n' + sql_string)

tic = time.time()
exec_result = cur.execute(sql_string)
toc = time.time()
print(f"\nExecution time: {toc-tic:.3f} seconds.")
print(f'{exec_result} row(s) affected...')
conn.commit()

sql_string = f"""select distinct concat('BP',lpad(pm_id,9,'0')) as sojourn_pm_id, pm_id, pm_name
    from bkgpal_evolve.places where pm_id not in
    (select convert(mid(pm_id,3),unsigned) as pm_id from sojourn.managers where PMS = 'BookingPal')
    and pm_id > 250 and rate > 2"""
print('\nNew managers previously not in Sojourn:\n\n' + sql_string)
tic = time.time()
exec_result = cur.execute(sql_string)
toc = time.time()
print(f"\nExecution time: {toc-tic:.3f} seconds.")
print(f'{exec_result} row(s) affected...')
result = cur.fetchall()
if not result is None:
    for pm in result:
        unit_cnt = 1
        msg = f"""\n\nALERT!!! BOOKINGPAL HAS RECEIVED A NEW PM: {pm[0]} | {pm[1]} | {pm[2]}\n\n"""
        sql_string = f"""select sojournID, property_name, place, average_per_night, currency from bkgpal_evolve.places where pm_id = '{pm[1]}'"""
        cur.execute(sql_string)
        result2 = cur.fetchall()
        for unit in result2:
            msg += f"""\n{unit_cnt}: {unit[0]} | {unit[1]} | {unit[2]} | {'' if unit[3] is None else unit[3]} {'' if unit[4] is None else unit[4]}"""
            unit_cnt += 1


        add_warning(msg)
        print(msg)
    # we have a new PM

# # before continuing, check that bkgpal_evolve.inventory hasn't been changed > 20%
sql_string = f"""SELECT bp.*, sj.*, 100*(bp.bkgpal_cnt-sj.soj_cnt)/sj.soj_cnt AS abs_diff_pct FROM
    (SELECT COUNT(sojournID) as bkgpal_cnt FROM bkgpal_evolve.places where rate > 2 and pm_id > 250) AS bp,
    (SELECT COUNT(sojournID) as soj_cnt FROM sojourn.places WHERE sojournID LIKE 'CR%') AS sj"""
print("\nCheck that bkgpal_evolve.inventory hasn't been changed > 20%:\n\n" + sql_string)
tic = time.time()
exec_result = cur.execute(sql_string)
toc = time.time()
print(f"\nExecution time: {toc-tic:.3f} seconds.")
print(f'{exec_result} row(s) affected...')
result = cur.fetchone()

print(f"Bkgpal inventory count:\nBkgpal places: {result[0]}; Sojourn-Bkgpal places: {result[1]}; Percent change: {result[2]}%")

if result[2] is None:
    msg = f"ATTENTION!!! Data is missing on BOOKINGPAL SOJOURN UPSERT:\nBkgpal places: {result[0]}; Sojourn-Bkgpal places: {result[1]}; \nEXTRACT WILL NOT RUN!"
    add_warning(msg)
    print(msg)
    _m.continue_run = False
elif result[2] >= 20:
    # increase in 20% - let it through
    msg = f"ATTENTION!!! BOOKINGPAL SOJOURN UPSERT:\nThere has been a POSITIVE change > 20% in Bkgpal inventory:\nBkgpal places: {result[0]}; Sojourn-Bkgpal places: {result[1]}; Percent change {result[2]}%"
    add_warning(msg)
    print(msg)
elif result[2] <= -20:
    # decrease in 20% - don't let it through
    msg = f"ATTENTION!!! BOOKINGPAL SOJOURN UPSERT:\nThere has been a NEGATIVE change > 20% in Bkgpal inventory:\nBkgpal places: {result[0]}; Sojourn-Bkgpal places: {result[1]}; Percent change {result[2]}%" #\nEXTRACT WILL NOT RUN!"
    add_warning(msg)
    print(msg)
    # _m.continue_run = False



def sql_safe_str(value):
    if value is not None:
        sql_safe_str = value.replace("'","''").strip()
        return f"'{sql_safe_str}'"
    else:
        return 'null'

def find_location(lookup_point):
    # use cached latitude and longitude to return location
    sql_string = f"""SELECT location FROM sojourn.geo_cache
    WHERE latitude = {lookup_point[0]} AND longitude = {lookup_point[1]}
    """
    cur.execute(sql_string)
    result = cur.fetchone()
    if not result is None:
        return result[0]
    else:
        return None

def find_sojournID(sojournID):
    try:
        # use cached House information to bypass geolookup
        sql_string = f"""SELECT location, latitude, longitude, city, state, country,
        street, place, zip_code
        FROM sojourn.geo_cache WHERE sojournID = '{sojournID}'"""
        cur.execute(sql_string)
        result = cur.fetchone()
        if not result is None:
            return {'location':result[0], 'latitude':result[1], 'longitude':result[2],
            'city':result[3],'state':result[4],'country':result[5], 'street':result[6],
            'place':result[7], 'zip_code':result[8]}
        else:
            return None
    except:
        return None

def join_safe(list_to_join):
    join_safe = ''
    for item in list_to_join:
        if item is not None:
            join_safe += item
    return join_safe

def do_table(databasename, tablename, command_list):
    # **PLACES
    sql_string = f'select count(id) AS num_records from bkgpal_evolve.{tablename}'
    num_rows = 0

    try:
        # see if it exists, then drop it
        cur.execute(sql_string)
        # sojourn_db.{tablename} exists
        num_rows = cur._rows[0][0]
    except pymysql.Error as Error:
        msg = f"bkgpal_evolve.{tablename} doesn't exist."
        add_warning(msg)
        print('\n' + msg)

    if num_rows > 0:
        # we have {tablename}, let's update main sojourn database
        print(f'\n******* ********* **********       upsert from bkgpal_evolve.{tablename}, {num_rows} rows:')
        print(f"\n\nBOOKINGPAL UPSERT elapsed time: {datetime.datetime.now() - script_start}")

        try:
            for sql_command in command_list:
                print('\n' + sql_command)
                tic = time.time()
                exec_result = cur.execute(sql_command)
                toc = time.time()
                print(f"\nExecution time: {toc-tic:.3f} seconds.")
                print(f'{exec_result} row(s) affected...')

                conn.commit()
        except pymysql.Error as Error:
            msg = f'{sql_command}\n\n{Error.args[1]}'
            add_warning(msg)
            print('\n' + msg)

    else:
        msg = f"no records in bkgpal_evolve.{tablename}, skipping this {databasename} database table update..."
        add_warning(msg)
        print('\n' + msg)

if _m.continue_run:
    print('\n**** BOOKINGPAL PREPROCESS - CONVERT TO SOJOURN DB ****')

    sql_string = f"""SELECT sojournID, location, latitude, longitude, city, state, street, place, zip_code, country
            FROM bkgpal_evolve.places"""
    cur.execute(sql_string)
    result = cur.fetchall()
    if not result is None:
        print('\nchecking geolocation of places....')
        error_retries = 0
        place_count = 0
        for evolve_place in result:
            place_count += 1
            sojournID = evolve_place[0]
            # print(f'\ngeolocating place {sojournID} - error retries: {error_retries} - num places: {place_count}')
            state = ''
            city = ''
            location = ''
            country = ''
            place = ''
            zip_code = ''
            street = ''
            street_number = ''
            route = ''
            latitude = None
            longitude = None
            geo_success = False
            geo_cached = find_sojournID(sojournID)
            if geo_cached is not None:
                # print('geo cache found...')
                location = geo_cached['location']
                latitude = geo_cached['latitude']
                longitude = geo_cached['longitude']
                city = geo_cached['city']
                state = geo_cached['state']
                country = geo_cached['country']
                street = geo_cached['street']
                place = geo_cached['place']
                zip_code = geo_cached['zip_code']
            else:
                while not geo_success and error_retries < 200:
                    geo_success = False
                    try:
                        housepoint = evolve_place[2], evolve_place[3] 
                        latitude = housepoint[0]
                        longitude = housepoint[1]
                        myplace = geolocator.reverse(housepoint, exactly_one=True)
                        _m.num_geolocate_calls += 1
                        geo_success = True
                        if myplace is not None:
                            for add_comp in myplace.raw['address_components']:
                                if add_comp['types'][0] == 'street_number':
                                    street_number = add_comp['short_name']
                                if add_comp['types'][0] == 'route':
                                    route = add_comp['short_name']
                                if add_comp['types'][0] == 'administrative_area_level_1':
                                    state = add_comp['long_name']
                                if add_comp['types'][0] in ['locality','neighborhood']:
                                    city = add_comp['long_name']
                                if add_comp['types'][0] == 'country':
                                    country = add_comp['short_name']
                                if add_comp['types'][0] == 'postal_code':
                                    zip_code = add_comp['short_name']

                            #location = myplace.address
                            if evolve_place[2] is None or evolve_place[2] != myplace.point.latitude:
                                latitude = myplace.point.latitude
                            if evolve_place[3] is None or evolve_place[3] != myplace.point.longitude:
                                longitude = myplace.point.longitude

                        #street = f'{street_number} {route}' if street_number != '' else f'{route}'
                        street = f'{route}'
                        if state == '':
                            place = f'{country}, {city}'
                        else:   
                            place = f'{country}, {state}, {city}'
   
                        location = place

                    except:
                        print(f'geo coding failure on {sojournID}: wait half a second, try again...')
                        time.sleep(.5)
                        error_retries += 1
                        if error_retries >= 200:
                            print('stop')

                        print(f"Preprocessing elapsed time: {datetime.datetime.now() - script_start}")
                        print(f'error retries: {error_retries}')

            # print(f'{location}')
            # print(f'{street}, {place}')
            if not latitude is None:
                sql_string = f"""update bkgpal_evolve.places set location = {sql_safe_str(location)}, latitude = {round(latitude,13)}, 
                    longitude = {round(longitude,12)}, city = {sql_safe_str(city)}, state = {sql_safe_str(state)}, 
                    street = {sql_safe_str(street)}, place = {sql_safe_str(place)},
                    zip_code = {sql_safe_str(zip_code)}, country = {sql_safe_str(country)} where sojournID = '{sojournID}'"""
                # print(f'\n{sql_string}')
                exec_result = cur.execute(sql_string)
                # print(f'\n{exec_result} row(s) affected...')
                conn.commit()

    print(f'\n# of geolocate calls: {_m.num_geolocate_calls}')
    print(f'\n# of error retries: {error_retries}')
    print(f"\nPreprocessing elapsed time: {datetime.datetime.now() - script_start}")

    print('\n ****** upsert into the sojourn api database ... ******')
    db_name_list = ['sojourn']
    for db_name in db_name_list:

        print(f'\n**** BOOKINGPAL UPSERT into {db_name.upper()} production database... ****')

        tables = {'places':[
            f"SET SQL_SAFE_UPDATES = 0;",
            f'''DELETE FROM sojourn.places WHERE sojournID like 'CR%';
            '''
            ,
            f'''INSERT INTO sojourn.places (sojournID, property_name, PMS, pm_id, country, place, zip_code, min_stay, 
            min_persons, max_persons, min_children, number_of_pets, house_type, latitude, longitude, location, picture, 
            currency, rate, wifi, iron, air_conditioning, hot_tub, pool, washer, free_parking, TV, dryer, breakfast, 
            hangers, hair_dryer, high_chair, smoke_alarm, pets_allowed, kitchen, heating, fireplace, laptop_friendly, 
            crib, carbon_monoxide_alarm, gym, ski_in_out, num_beds, num_bedrooms, num_bathrooms, meta_tags, average_rating, star_rating, last_updated)
            SELECT a.sojournID, a.property_name, 'BookingPal' as PMS, concat('BP',lpad(a.pm_id,9,'0')) as pm_id, 
            a.country, if(a.place is null,a.location,a.place) as place, if(a.zip_code is null,'',a.zip_code) as zip_code, 
            a.min_stay, a.min_persons, a.max_persons, a.min_children, a.number_of_pets, b.description as house_type, a.latitude, a.longitude, 
            a.location, a.picture, a.currency, a.rate, a.wifi, a.iron, a.air_conditioning, a.hot_tub, a.pool, a.washer, a.free_parking, 
            a.TV, a.dryer, a.breakfast, a.hangers, a.hair_dryer, a.high_chair, a.smoke_alarm, a.pets_allowed, a.kitchen, a.heating, 
            a.fireplace, a.laptop_friendly, a.crib, a.carbon_monoxide_alarm, a.gym, a.ski_in_out, a.num_beds, a.num_bedrooms, 
            a.num_bathrooms, concat('|BookingPal|',replace(a.pm_name,' ','')) as meta_tags, a.average_rating, 0, '{script_start}' 
            FROM bkgpal_evolve.places a, bkgpal_evolve.listing_type b
            where a.house_type = b.house_type and a.rate > 2 and a.pm_id > 250
            order by sojournID;
            '''
            ,
            f'''UPDATE sojourn.places SET num_reviews = 0 where num_reviews is null;
            ''',
            f'''INSERT INTO sojourn.managers (pm_id, pm_name, pm_short_name, PMS, channel_mgr) 
                select distinct concat('BP',lpad(pm_id,9,'0')) as pm_id, pm_name, pm_name as pm_short_name,
                'BookingPal' as PMS, 'BookingPal' as channel_mgr 
                from bkgpal_evolve.places where pm_id in
                (select distinct convert(mid(pm_id,3),unsigned) as pm_id from sojourn.places where pm_id not in
                (SELECT pm_id FROM sojourn.managers) and PMS = 'BookingPal');
                ''',
            f'''insert into sojourn.account_pms (userid, pm_id) 
                select distinct 'sojournapidev.wpengine.com' as userid, pm_id from sojourn.places where pm_id not in
                (SELECT pm_id FROM sojourn.account_pms where userid = 'sojournapidev.wpengine.com');
                ''',
            f'''insert into sojourn.account_pms (userid, pm_id) 
                select distinct 'henry' as userid, pm_id from sojourn.places where pm_id not in
                (SELECT pm_id FROM sojourn.account_pms where userid = 'henry');
                ''',
            f'''insert into sojourn.account_pms (userid, pm_id) 
                select distinct 'portal.sojournapi.com' as userid, pm_id from sojourn.places where pm_id not in
                (SELECT pm_id FROM sojourn.account_pms where userid = 'portal.sojournapi.com') and pm_id <> 'WS';
                ''',
            f'''insert into sojourn.account_pms (userid, pm_id) 
                select distinct 'FBNB' as userid, pm_id from sojourn.places where pm_id not in
                (SELECT pm_id FROM sojourn.account_pms where userid = 'FBNB') and pm_id <> 'WS';
                ''',
            f'''INSERT INTO bkgpal_evolve.daily_places (import_date, sojournID)
                SELECT a.* FROM (SELECT '{import_date}' AS import_date, sojournID 
                FROM bkgpal_evolve.places WHERE rate > 2 and pm_id > 250) a 
                ON DUPLICATE KEY UPDATE import_date=a.import_date, sojournID=a.sojournID;
                ''',
            f'''insert into bkgpal_evolve.places_persist (sojournID, last_refresh, property_name, 
                country, zip_code, max_persons, house_type, latitude, longitude, location, picture, 
                currency, rate, city, state, street, pm_id, pm_name, pricing_model)
                select ps.* from
                (select sojournID, curdate() as last_refresh, property_name, country, zip_code, max_persons,
                house_type, latitude, longitude, location, picture, currency, rate,
                city, state, street, pm_id, pm_name, pricing_model from bkgpal_evolve.places
                where rate > 2 and pm_id > 250) ps
                on duplicate key update sojournID=ps.sojournID, last_refresh=ps.last_refresh, 
                property_name=ps.property_name, country=ps.country, zip_code=ps.zip_code, 
                max_persons=ps.max_persons, house_type=ps.house_type, latitude=ps.latitude, 
                longitude=ps.longitude, location=ps.location, picture=ps.picture, 
                currency=ps.currency, rate=ps.rate, city=ps.city, state=ps.state, 
                street=ps.street, pm_id=ps.pm_id, pm_name=ps.pm_name, pricing_model=ps.pricing_model;
                '''
            ]
            ,'calendars':[
            f'''delete from bkgpal_evolve.cal_temp;''',
            f'''insert into bkgpal_evolve.cal_temp
                SELECT c.number  as id, DATE_ADD(CURDATE(), INTERVAL c.number DAY) AS cal_date, 0 as available
                FROM (SELECT singles + tens + hundreds number FROM (SELECT 0 singles
                        UNION ALL SELECT   1 UNION ALL SELECT   2 UNION ALL SELECT   3
                        UNION ALL SELECT   4 UNION ALL SELECT   5 UNION ALL SELECT   6
                        UNION ALL SELECT   7 UNION ALL SELECT   8 UNION ALL SELECT   9
                    ) singles JOIN 
                    (SELECT 0 tens
                        UNION ALL SELECT  10 UNION ALL SELECT  20 UNION ALL SELECT  30
                        UNION ALL SELECT  40 UNION ALL SELECT  50 UNION ALL SELECT  60
                        UNION ALL SELECT  70 UNION ALL SELECT  80 UNION ALL SELECT  90
                    ) tens  JOIN 
                    (SELECT 0 hundreds
                        UNION ALL SELECT  100 UNION ALL SELECT  200 UNION ALL SELECT  300
                        UNION ALL SELECT  400 UNION ALL SELECT  500 UNION ALL SELECT  600
                        UNION ALL SELECT  700 UNION ALL SELECT  800 UNION ALL SELECT  900
                    ) hundreds
                    ORDER BY number DESC
                ) c  WHERE c.number BETWEEN 0 AND 500 order by 1''',
            f'''delete from bkgpal_evolve.cal_merge;''',
            f'''insert into bkgpal_evolve.cal_merge
                select concat(a.sojournID,'.',b.id) as id, a.sojournID, b.calendar_date, 1 as quantity,
                0 as arrival_allowed, 0 as departure_allowed, 1 as minimum_stay,
                365 as maximum_stay, 0 as days_available from bkgpal_evolve.places a,
                bkgpal_evolve.cal_temp b''',
            f'''UPDATE bkgpal_evolve.cal_merge as a,
                (select sojournID, calendar_date, quantity, arrival_allowed, departure_allowed,
                minimum_stay, if(maximum_stay is null,365,maximum_stay) as maximum_stay, days_available from bkgpal_evolve.calendars order by 1, 2) as b
                set a.quantity = b.quantity,
                a.arrival_allowed = b.arrival_allowed,
                a.departure_allowed = b.departure_allowed,
                a.minimum_stay = b.minimum_stay,
                a.maximum_stay = b.maximum_stay,
                a.days_available = b.days_available
                where a.sojournID = b.sojournID
                and a.calendar_date = b.calendar_date;
                ''',
            f'''DELETE FROM sojourn.calendars WHERE sojournID like 'CR%';
            '''
            ,
            f'''INSERT INTO sojourn.calendars
                (id, sojournID, calendar_date, quantity, arrival_allowed, departure_allowed, minimum_stay, maximum_stay, days_available)
                SELECT id, sojournID, calendar_date, quantity, arrival_allowed,
                departure_allowed, minimum_stay, maximum_stay, days_available
                FROM bkgpal_evolve.cal_merge;
            ''']
            ,'descriptions':[
            f'''DELETE FROM sojourn.descriptions WHERE sojournID like 'CR%';
            '''
            ,
            f'''INSERT INTO sojourn.descriptions (id, sojournID, language_code, description, description_type)
            SELECT concat(id,'.1'), sojournID, language_code, description, description_type
            FROM bkgpal_evolve.descriptions;
            ''']
            ,'pictures':[
            f'''DELETE FROM sojourn.pictures WHERE sojournID like 'CR%';
            '''
            ,
            f'''INSERT INTO sojourn.pictures (id, sojournID, file_url, picture_type)
            SELECT id, sojournID, file_url, picture_type FROM bkgpal_evolve.pictures;
            ''']
        }

        # ie., to run one table only: 
        # do_table(db_name, 'pictures',tables['pictures'])

        [do_table(db_name, x, tables[x]) for x in tables]

    sql_command = f'''insert into sojourn.geo_cache(sojournID, country, city, state, street, place, zip_code, latitude, longitude, location)
            select pl.* from
            (select sojournID, country, city, state, street, place, zip_code, latitude, longitude, location from bkgpal_evolve.places) as pl
            on duplicate key update sojournID=pl.sojournID, country=pl.country, city=pl.city, state=pl.state, street=pl.street,
            place=pl.place, zip_code=pl.zip_code, latitude=pl.latitude, longitude=pl.longitude;
            '''
    print('\n**** upsert from bkgpal_evolve.places --> sojourn.geo_cache ****')
    print('\n' + sql_command)
    tic = time.time()
    exec_result = cur.execute(sql_command)
    toc = time.time()
    print(f"\nExecution time: {toc-tic:.3f} seconds.")
    print(f'{exec_result} row(s) affected...')


    conn.commit()


    # now add hash_tag aliases
    sql_string = f"""select * from sojourn.hash_alias;"""
    print('\nAdd hash_tag aliases...')
    exec_result = cur.execute(sql_string)
    result = cur.fetchall()
    if not result is None:
        for alias in result:
            msg = f"""hash_tag: #{alias[0]} | alias: #{alias[1]}"""
            print(msg)
            sql_command = f"""set sql_safe_updates = 0;"""
            exec_result = cur.execute(sql_command)

            sql_command =f"""update sojourn.places set meta_tags = concat(meta_tags,'|{alias[1]}') WHERE meta_tags like '%{alias[0]}%' and meta_tags not like '%|{alias[1]}%';"""
            print('\n' + sql_command)
            tic = time.time()
            exec_result = cur.execute(sql_command)
            conn.commit()
            toc = time.time()
            print(f"\nExecution time: {toc-tic:.3f} seconds.")
            print(f'{exec_result} row(s) affected...')


    # remove all bookingpal aws dead image links...
    print('\nRemove all booking-pal dead image links on aws...')
    sql_command = f"""delete from sojourn.pictures where left(file_url,locate('/',file_url,10)) = 'https://s3.amazonaws.com/' and sojournID like 'CR%';"""
    print('\n' + sql_command)
    tic = time.time()
    exec_result = cur.execute(sql_command)
    conn.commit()
    toc = time.time()
    print(f"\nExecution time: {toc-tic:.3f} seconds.")
    print(f'{exec_result} row(s) affected...')

    sql_command = f"""delete from sojourn.places where left(picture,locate('/',picture,10)) = 'https://s3.amazonaws.com/' and sojournID like 'CR%';"""
    print('\n' + sql_command)
    tic = time.time()
    exec_result = cur.execute(sql_command)
    conn.commit()
    toc = time.time()
    print(f"\nExecution time: {toc-tic:.3f} seconds.")
    print(f'{exec_result} row(s) affected...')

    cur.close()
    conn.close()

    cur = None

    print(f"\n\nBOOKINGPAL UPSERT elapsed time: {datetime.datetime.now() - script_start}")

if len(warn_list) > 0:
    message = f'\n{len(warn_list)} Warnings...\n{_m.limit_exceeded}'+'\n'.join(warn_list)
    print(message)
    send_warning(message)
else:
    print('\nNo Warnings...')


