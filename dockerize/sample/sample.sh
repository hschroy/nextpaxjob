#!/bin/bash
echo Hello World!

if [ -f /.dockerenv ]; then
    echo "Running $0 inside a Docker container"
else
    echo "$0 (Not running inside a Docker container)"
fi
