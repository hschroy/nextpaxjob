# PATH=$PATH:/home/henry_schroy/google-cloud-sdk/bin
#cd ~/daily/nextpaxjob;
echo "Running $0 inside a Docker container"
cd /usr/src/;

if [ -f "evolve.is.running" ]; then
    echo "evolve extract is running already!"
    exit 1
else
    touch "evolve.is.running";
fi

# rm evolvejob.log;
if [ -f "evolvejob.log" ]; then
    rm -f evolvejob.log;
fi

(time bash evolvejob.sh) > evolvejob.log 2>&1


# source "/home/henry_schroy/venv/bin/activate"
# cd ~/daily/nextpaxjob;

python send_evolve_email.py;

if [ -f "evolve.is.running" ]; then
    echo "Removing evolve.is.running file"
    rm -f "evolve.is.running";
fi
