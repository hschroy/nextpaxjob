# using socketlabs
import os
####
# 
from pathlib import Path
import datetime
from datetime import date

from socketlabs.injectionapi import SocketLabsClient
from socketlabs.injectionapi.message.basicmessage import BasicMessage
from socketlabs.injectionapi.message.emailaddress import EmailAddress

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
# change working directory to same as current python file
os.chdir(dname) 

serverId = 34266
injectionApiKey = "n6LYr27Dap3GFi8s5NBw"

client = SocketLabsClient(serverId, injectionApiKey)

def send_warning(msg):
    start_time = datetime.datetime.now()

    message = BasicMessage()

    message.subject = start_time.strftime("EVOLVE EXTRACT WARNINGS: %m/%d/%Y, %H:%M:%S")
    #message.html_body = "<html>This is the Html Body of my message.</html>"
    message.plain_text_body = msg

    message.from_email_address = EmailAddress("henry@furraylogic.com")
    message.to_email_address.append(EmailAddress("support@furraylogic.com"))
    message.to_email_address.append(EmailAddress("rob@sojournapi.com"))

    response = client.send(message)
    print(response)


#send_warning("this is a test.")
