echo 'starting evolve extract'
date
# Use the curl command to retrieve the external IP address from a website that displays it
IP=$(curl -s http://checkip.dyndns.org | sed -e 's/.*Current IP Address: //' -e 's/<.*$//')

# Echo the IP address
echo "Current external IP address: $IP"
echo

echo "Running $0 inside a Docker container"

cd /usr/src/;

# source "/home/henry_schroy/venv/bin/activate"
# cd ~/daily/nextpaxjob;

python evolve_sojourn_upsert.py;
# python update_stars.py;

echo 'ending evolve extract'
date

echo
echo 'Batch run time:'