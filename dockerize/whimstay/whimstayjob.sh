echo 'starting whimstay extract'
date
# Use the curl command to retrieve the external IP address from a website that displays it
IP=$(curl -s http://checkip.dyndns.org | sed -e 's/.*Current IP Address: //' -e 's/<.*$//')

# Echo the IP address
echo "Current external IP address: $IP"
echo

echo "Running $0 inside a Docker container"

cd /usr/src/;

python whimstay_sojourn_upsert.py;

echo 'ending whimstay extract'
date

echo
echo 'Batch run time:'