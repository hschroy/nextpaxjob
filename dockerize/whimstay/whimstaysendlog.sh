echo "Running $0 inside a Docker container"
cd /usr/src/;

if [ -f "whimstay.is.running" ]; then
    echo "whimstay extract is running already!"
    exit 1
else
    touch "whimstay.is.running";
fi

if [ -f "whimstayjob.log" ]; then
    rm -f whimstayjob.log;
fi

(time bash whimstayjob.sh) > whimstayjob.log 2>&1

python send_whimstay_email.py;

if [ -f "whimstay.is.running" ]; then
    echo "Removing whimstay.is.running file"
    rm -f "whimstay.is.running";
fi

