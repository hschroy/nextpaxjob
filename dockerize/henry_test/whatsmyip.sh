#!/bin/bash

# Use the curl command to retrieve the external IP address from a website that displays it
IP=$(curl -s http://checkip.dyndns.org | sed -e 's/.*Current IP Address: //' -e 's/<.*$//')

# Echo the IP address
echo "Current external IP address: $IP"
