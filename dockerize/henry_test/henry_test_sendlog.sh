#!/bin/bash

# PATH=$PATH:/home/henry_schroy/google-cloud-sdk/bin

if [ -f /.dockerenv ]; then
    echo "Running $0 inside a Docker container"
    cd /usr/src/;
fi


if [ -f "launcher.is.running" ]; then
    echo "Launcher is running already!"
    exit 1
else
    touch "launcher.is.running";
    # sleep 5
fi

if [ -f "henry_test.log" ]; then
    rm -f henry_test.log;
fi


(time bash henry_test.sh) > henry_test.log 2>&1
if [ -f /.dockerenv ]; then
    echo "Running $0 inside a Docker container"
else
    echo "$0 (Not running inside a Docker container)"
    cd /home/henry_schroy/daily/nextpaxjob/dockerize/henry_test;
    source /home/henry_schroy/daily/nextpaxjob/venv/venv/bin/activate;
fi

python henry_test_email.py;

if [ -f "launcher.is.running" ]; then
    echo "Removing launcher.is.running file"
    rm -f "launcher.is.running";
fi
