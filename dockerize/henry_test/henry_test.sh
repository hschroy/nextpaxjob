#!/bin/bash

echo 'starting henry-test extract'
date
# Use the curl command to retrieve the external IP address from a website that displays it
IP=$(curl -s http://checkip.dyndns.org | sed -e 's/.*Current IP Address: //' -e 's/<.*$//')

# Echo the IP address
echo "Current external IP address: $IP"
echo



if [ -f /.dockerenv ]; then
    echo "Running $0 inside a Docker container"

    cd /usr/src/;
else
    echo "$0 (Not running inside a Docker container)"
    cd /home/henry_schroy/daily/nextpaxjob/dockerize/henry_test;
    source /home/henry_schroy/daily/nextpaxjob/venv/venv/bin/activate;
fi

python henry_test.py;
#python update_stars.py;

echo 'ending henry-test extract'
date

echo
echo 'Batch run time:'