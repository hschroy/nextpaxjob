import pymysql 
import pandas as pd
from email_warn import send_warning
import datetime
from datetime import date
from tabulate import tabulate
from dotenv import load_dotenv
import os
load_dotenv()

NEXTPAX_HOST = os.getenv('NEXTPAX_HOST')
NEXTPAX_USER = os.getenv('NEXTPAX_USER')
NEXTPAX_PWD = os.getenv('NEXTPAX_PWD')

script_start = datetime.datetime.now()

print('\n**** NEXTPAX PROCESS PARENTS (MULTI-UNITS)... ****')
check_date = datetime.datetime.now().strftime("%Y-%m-%d")
print(check_date)

conn = pymysql.connect(host=NEXTPAX_HOST, user=NEXTPAX_USER, passwd=NEXTPAX_PWD, db='nextpax', port=3306, charset='utf8mb4')

cur = conn.cursor()
cur.execute('USE nextpax')

def sql_safe_str(value):
    if value is not None:
        if isinstance(value, datetime.date):
            sql_safe_str = value.strftime("%Y-%m-%d")
        else:
            sql_safe_str = value.replace("'","''").strip()
        return f"'{sql_safe_str}'"
    else:
        return 'null'

def safe_num(value):
    if value is not None:
        return value
    else:
        return 'null'

def get_parent(HouseID):
    sql_string = f"SELECT ParentId FROM nextpax.houses WHERE HouseID = '{HouseID}'"
    cur.execute(sql_string)
    result = cur.fetchone()
    if not result is None:
        return result[0]
    else:
        return None

def is_parent(HouseID):
    sql_string = f"SELECT HouseID FROM nextpax.houses WHERE ParentId = '{HouseID}'"
    num_rows = cur.execute(sql_string)
    if num_rows > 0:
        return True
    else:
        return False

def get_top_parent(HouseID):
    my_parent = get_parent(HouseID)
    if my_parent is None:
        return HouseID
    else:
        return get_top_parent(my_parent)

num_updates = 0

running_again = False

sql_string = '''ALTER TABLE `nextpax`.`houses` 
    ADD COLUMN `TopParent` VARCHAR(50) NULL DEFAULT NULL AFTER `Classification`,
    ADD COLUMN `max_units` DECIMAL(10,5) NULL DEFAULT NULL AFTER `TopParent`,
    ADD COLUMN `avg_units` DECIMAL(10,5) NULL DEFAULT NULL AFTER `max_units`;'''
print('\n' + sql_string)
try:
    exec_result = cur.execute(sql_string)
    print(f'\n{exec_result} row(s) affected...')
    conn.commit()
except pymysql.Error as Error:
    msg = Error.args[1]
    print('\n' + msg)
    print('\n** (this routine has already added fields TopParent, max_units and avg_units to houses earlier today) **')
    running_again = True

num_top_parents = 0

if not running_again:
    sql_string = f"SELECT HouseID, ParentId FROM nextpax.houses"
    # house_ids = pd.read_sql(sql_string,conn)

    cur.execute(sql_string)
    house_ids2 = cur.fetchall()
    house_ids = pd.DataFrame(house_ids2,columns=['HouseID','ParentId'])

    for index, row in house_ids.iterrows(): 
        if is_parent(row.HouseID) or not row.ParentId is None:
            #
            sql_string =f"select max(quantity) as max_units, avg(quantity) as avg_units from nextpax.calendars where HouseID = '{row.HouseID}';"
            cur.execute(sql_string)
            result = cur.fetchone()
            if not result is None and not result[0] is None:
                max_units = result[0]
                avg_units = result[1]
            else:
                max_units = 0
                avg_units = 0
            #
            top_parent = get_top_parent(row.HouseID)
            sql_string=f"UPDATE nextpax.houses SET TopParent = '{top_parent}', max_units = {max_units}, avg_units = {avg_units} WHERE HouseID = '{row.HouseID}'"
            # print('\n' + sql_string)
            exec_result = cur.execute(sql_string)
            num_updates += 1
            conn.commit()


    sql_string = f'''select TopParent, sum(max_units) as sum_max, sum(avg_units) as sum_avg from nextpax.houses
    WHERE TopParent is not null GROUP BY TopParent order by cast(TopParent as unsigned)'''
    # house_ids = pd.read_sql(sql_string,conn)
    cur.execute(sql_string)
    house_ids2 = cur.fetchall()
    house_ids = pd.DataFrame(house_ids2,columns=['TopParent','sum_max','sum_avg'])
    for index, row in house_ids.iterrows(): 
        sql_string=f"UPDATE nextpax.houses SET max_units = {row.sum_max}, avg_units = {row.sum_avg} WHERE HouseID = '{row.TopParent}'"
        # print('\n' + sql_string)
        exec_result = cur.execute(sql_string)
        num_top_parents += 1
        conn.commit()

########


def has_table(HouseID, table_name):
    sql_string = f"SELECT * FROM nextpax.{table_name} WHERE HouseID = '{HouseID}'"
    num_rows = cur.execute(sql_string)
    if num_rows > 0:
        return True
    else:
        return False

def house_exists(HouseID):
    sql_string = f"SELECT * FROM nextpax.houses WHERE HouseID = '{HouseID}'"
    num_rows = cur.execute(sql_string)
    if num_rows > 0:
        return True
    else:
        return False

def get_new_id(table_name):
    sql_string = f"SELECT MAX(id)+1 AS new_id FROM nextpax.{table_name}"
    cur.execute(sql_string)
    result = cur.fetchone()
    if not result is None:
        return result[0]
    else:
        return None

def get_partner_etc(HouseID):
    sql_string = f"SELECT Partner, Brand, max_units, Classification FROM nextpax.houses WHERE HouseID = '{HouseID}'"
    cur.execute(sql_string)
    result = cur.fetchone()
    if not result is None:
        return result[0], result[1], result[2], result[3]
    else:
        return None, None, None, None

def get_currency(HouseID):
    sql_string = f'''select  ac.CostCurrency from nextpax.houses hs, 
        nextpax.additional_costs ac
        where hs.HouseID = ac.HouseID and TopParent = '{HouseID}' limit 1'''
    cur.execute(sql_string)
    result = cur.fetchone()
    if not result is None:
        return result[0]
    else:
        return None

def get_language(HouseID):
    sql_string = f'''select  ds.LanguageCode from nextpax.houses hs, nextpax.descriptions ds
        where hs.HouseID = ds.HouseID and TopParent = '{HouseID}' limit 1'''
    cur.execute(sql_string)
    result = cur.fetchone()
    if not result is None:
        return result[0]
    else:
        return None

def get_picture(HouseID):
    sql_string = f'''select ps.FileUrl from nextpax.houses hs, nextpax.pictures ps
        where hs.HouseID = ps.HouseID and hs.TopParent = '{HouseID}' limit 1'''
    cur.execute(sql_string)
    result = cur.fetchone()
    if not result is None:
        return result[0]
    else:
        return None

def get_rate_defaults(HouseID):
    sql_string = f'''select min(fromDate) as fromDate, max(tillDate) as tillDate, 
        round(avg(amount),2) as amount, max(persons) as persons, 
        max(weekdays) as weekdays, min(minimumStay) as minimumStay, 
        max(maximumStay) as maximumStay, round(avg(extraPersonFeeAmount),2) as extraPersonFeeAmount,
        currency from nextpax.houses hs, nextpax.rates rt
        where hs.HouseID = rt.HouseID and hs.TopParent = '{HouseID}' and rt.duration = 1 
        group by currency limit 1'''
    cur.execute(sql_string)
    result = cur.fetchone()
    if not result is None:
        return result[0], result[1], result[2], result[3], result[4], result[5], result[6], result[7], result[8]
    else:
        return None, None, None, None, None, None, None, None, None

def get_my_children(HouseID):
    sql_string = f"SELECT distinct HouseID FROM nextpax.houses where TopParent = '{HouseID}' and HouseID <> '{HouseID}';"
    children = []
    # child_ids = pd.read_sql(sql_string, conn)
    cur.execute(sql_string)
    child_ids2 = cur.fetchall()
    child_ids = pd.DataFrame(child_ids2,columns=['HouseID'])
    for index, row in child_ids.iterrows():
        children.append('OR' + row.HouseID)

    return children

def get_first_description(HouseID):
    sql_string = f"select id, Description from nextpax.descriptions where HouseID = '{HouseID}';"
    cur.execute(sql_string)
    result = cur.fetchone()
    if not result is None:
        return result[0], result[1]
    else:
        return None, None

def update_description(description_id, description):
    sql_string = f"UPDATE nextpax.descriptions SET Description = {sql_safe_str(description)} WHERE id = {description_id};"
    cur.execute(sql_string)
    conn.commit()

num_additional_costs = 0
num_amenities = 0
num_calendars = 0
num_descriptions = 0
num_pictures = 0
num_rates = 0
num_taxes = 0

print(f"\n\nParents (multi-units) script elapsed time: {datetime.datetime.now() - script_start}")

sql_string = f'''select DISTINCT TopParent from nextpax.houses WHERE TopParent is not null order by cast(TopParent as unsigned)'''
cur.execute(sql_string)
house_ids = cur.fetchall()

# house_ids = pd.read_sql(sql_string,conn)
top_parent_count = len(house_ids)
print(f"{top_parent_count} TopParent properties...")
top_parent_counter = 0
for row in house_ids: 
    house_id = row[0]
    top_parent_counter += 1
    # print(f"house_id: {house_id} | {top_parent_counter} / {top_parent_count}")
    if True: #not running_again:
        if house_exists(house_id):
            partner, brand, max_units, classification = get_partner_etc(house_id)
            currency = get_currency(house_id)
            language_code = get_language(house_id)
            picture = get_picture(house_id)

            if not has_table(house_id, 'additional_costs'):
                # add an additional_cost row
                new_id = get_new_id('additional_costs')
                sql_string = f'''INSERT INTO nextpax.additional_costs (id, Partner, HouseID, 
                    CostCode, CostType, CostAmount, CostAmountType, CostCurrency, CostNumber)
                    VALUES ({new_id}, '{partner}', '{house_id}', 'EXT', 'OPT', 0, 'NOR',
                    '{currency}', 1)
                    '''
                # print('\n' + sql_string)
                exec_result = cur.execute(sql_string)
                num_additional_costs += 1
                conn.commit()
                pass

            if not has_table(house_id, 'amenities'):
                # add an amenities row
                new_id = get_new_id('amenities')
                sql_string = f'''INSERT INTO nextpax.amenities (id, Partner, HouseID, 
                    PropertyCode, AmenityValue, AmenityNumber)
                    VALUES ({new_id}, '{partner}', '{house_id}', 'L06', 'Y', 1)
                    '''
                # print('\n' + sql_string)
                exec_result = cur.execute(sql_string)
                num_amenities += 1
                conn.commit()
                pass

            if not has_table(house_id, 'calendars'):
                # add a calendars row
                new_id = get_new_id('calendars')
                sql_string = f'''INSERT INTO nextpax.calendars (id, PropertyManagementSystem, HouseID, 
                    PropertyManager, calendarDate, quantity, arrivalAllowed, departureAllowed, 
                    minimumStay, maximumStay, CalendarNumber)
                    VALUES ({new_id}, '{partner}', '{house_id}', '{brand}', '{check_date}', {max_units},
                    0, 0, 0, 0, 1)
                    '''
                # print('\n' + sql_string)
                exec_result = cur.execute(sql_string)
                num_calendars += 1
                conn.commit()
                pass

            if not has_table(house_id, 'descriptions'):
                # add a descriptions row
                new_id = get_new_id('descriptions')
                description = f"This {classification} consists of {int(max_units)} units."
                sql_string = f'''INSERT INTO nextpax.descriptions (id, Partner, HouseID, 
                    LanguageCode, Description, DescriptionType, DescriptionNumber)
                    VALUES ({new_id}, '{partner}', '{house_id}', '{language_code}', '{description}', 'headline', 1)
                    '''
                # print('\n' + sql_string)
                exec_result = cur.execute(sql_string)
                num_descriptions += 1
                conn.commit()
                pass

            if not has_table(house_id, 'pictures'):
                # add a pictures row
                new_id = get_new_id('pictures')
                sql_string = f'''INSERT INTO nextpax.pictures (id, Partner, HouseID, 
                    FileUrl, PictureType, PictureNumber)
                    VALUES ({new_id}, '{partner}', '{house_id}', '{picture}', 'other', 1)
                    '''
                # print('\n' + sql_string)
                exec_result = cur.execute(sql_string)
                num_pictures += 1
                conn.commit()
                pass

            if not has_table(house_id, 'rates'):
                # add a rates row
                new_id = get_new_id('rates')
                from_date, till_date, amount, persons, weekdays, min_stay, max_stay, extra_person, currency =  get_rate_defaults(house_id)
                sql_string = f'''INSERT INTO nextpax.rates (id, PropertyManagementSystem, HouseID, 
                    PropertyManager, fromDate, tillDate, duration, amount, currency, persons, weekdays, minimumStay, maximumStay, 
                    extraPersonFeeAmount, extraPersonFeeCurrency, RateNumber)
                    VALUES ({new_id}, '{partner}', '{house_id}', '{brand}', {sql_safe_str(from_date)}, {sql_safe_str(till_date)}, 1, {safe_num(amount)}, 
                    {sql_safe_str(currency)}, {sql_safe_str(persons)}, {sql_safe_str(weekdays)}, {safe_num(min_stay)}, {safe_num(max_stay)}, 
                    {safe_num(extra_person)}, {sql_safe_str(currency)}, 1)
                    '''
                # print('\n' + sql_string)
                exec_result = cur.execute(sql_string)
                num_rates += 1
                conn.commit()
                pass

            if not has_table(house_id, 'taxes'):
                # add a taxes row
                new_id = get_new_id('taxes')
                sql_string = f'''INSERT INTO nextpax.taxes (id, Partner, HouseID, 
                    TaxType, Percentage, Included, TaxNumber)
                    VALUES ({new_id}, '{partner}', '{house_id}', 'TAX', 0, 0, 1)
                    '''
                # print('\n' + sql_string)
                exec_result = cur.execute(sql_string)
                num_taxes += 1
                conn.commit()
                pass

            desc_id, desc = get_first_description(house_id)
            childs = get_my_children(house_id)
            if not '** TOP PARENT **' in desc:
                desc = f"** TOP PARENT ** \nThis property contains a total of {int(max_units)} subunits.\nChild properties:  {', '.join(childs)}\n\n" + desc
                update_description(desc_id, desc)



print(f"{num_updates} rows with a TopParent updated.")
print(f"{num_additional_costs} additional costs added.")
print(f"{num_amenities} amenities added.")
print(f"{num_calendars} calendars added.")
print(f"{num_descriptions} descriptions added.")
print(f"{num_pictures} pictures added.")
print(f"{num_rates} rates added.")
print(f"{num_taxes} taxes added.")

print(f"{num_top_parents} top parents.")

print(f"\n\nParents (multi-units) script elapsed time: {datetime.datetime.now() - script_start}")
