import xml.etree.ElementTree as ET
from datetime import datetime
from pandas import Series, DataFrame
import pandas as pd
import csv
import os
from sqlalchemy import create_engine
import pymysql 
from sqlalchemy.types import Integer, Text, String, Numeric, DateTime, Boolean
from tabulate import tabulate
import random
from np_constants import pms_list

start_time = datetime.now()

engine = create_engine("mysql+pymysql://{user}:{passwd}@{host}/{db}".format(host='35.192.116.102', user='henry', passwd='finley', db='nextpax', port=3306))
#engine = create_engine("mysql+pymysql://{user}:{passwd}@{host}/{db}".format(host='34.70.178.230', user='henry', passwd='finley', db='sojourndb', port=3306))

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
# change working directory to same as current python file
os.chdir(dname) 

# now change to Xml folder to read in houses xml
dname = 'Xml'

os.chdir(dname) 

###############################################################
houses = []

#['paxgenerator_houses_{pms}.xml'.format(pms=pm_code) for pm_code in pms_list]
for pm_code in pms_list:
    xml_filename = 'paxgenerator_houses_{pms}.xml'.format(pms=pm_code)
    xmlp = ET.XMLParser(encoding="utf-8")
    tree = ET.parse(xml_filename,parser=xmlp)
    root = tree.getroot()
    partner = root.find('Partner').text

    for house in root.iter('House'):
        # add property attributes to rate
        house.attrib['Partner'] = partner
        i = 0
        while i < len(house):
            if house[i].tag == 'Place':
                # we saw a "Cabo San Lucas," (with a comma at the end)
                if not house[i].text is None:
                    house[i].text = house[i].text.replace(',','')

            #  Terra Verde Resort Townhome -  Merida 
            if house[i].tag == 'Name':
                house[i].text = house[i].text.strip()

            house.attrib[house[i].tag] = house[i].text
            i += 1

        if not (house.attrib['Longitude'] is None and house.attrib['Latitude'] is None and house.attrib['Place'] is None 
                and house.attrib['ZipCode'] is None):
            houses.append(house.attrib)
        else:
            print(f'\nSkipping property with no location info:  {house.attrib}')
    
    xmlp = None
    tree = None
    root = None
    partner = None


house_df = DataFrame(houses)


# cols = rates_df.columns.tolist() 
# cols = cols[-5:] + cols[:-5]
cols =['Partner', 'Brand', 'HouseID', 'Country', 'Place', 'ZipCode', 'Name', 'MinPersons', 'MaxPersons', 
    'MinChildren', 'NumberOfPets', 'Type', 'Latitude', 'Longitude', 'Currency', 'LicenseNumber']
house_df = house_df[cols]
house_df = house_df.rename(columns={"Type": "HouseType", "Name": "HouseName"})

fieldschema={
	'Partner': String(50), 'Brand': String(50), 'HouseID': String(50), 'Country': String(50), 
    'Place': Text, 'ZipCode': String(50), 'HouseName': Text,	'MinPersons': Integer, 
    'MaxPersons': Integer, 'MinChildren': Integer,	'NumberOfPets': Integer, 'HouseType': String(20), 
    'Latitude': Numeric(18,13), 'Longitude': Numeric(17,12), 'Currency': String(50), 'LicenseNumber': String(50)
}

house_df.index = house_df.index.rename('id')
house_df.to_sql('houses', con = engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema)

print('\n# of Houses:')
print(house_df.count()['Place'])
print('\n# by Property Mgr:')
print(tabulate(pd.DataFrame(house_df.groupby('Partner')['Place'].count()), headers=['Partner','Count']))
print('\nHouses for ',pms_list,':')

num_rows = len(house_df)
num_samples = 45

random_index = []

i = 0
while i < num_samples:
    random_int = random.randint(0,num_rows-1)
    if random_int not in random_index:
        random_index.append(random_int)
        i += 1

random_index.sort()

print(f'(randomly sampling {num_samples} houses out of {num_rows}...)\nrow#')

#print(f'(sampling every {int(len(house_df)*0.033)} rows...)\n')

summary_fields = ['Partner','HouseID','Place','HouseName','Country','Latitude','Longitude']
print("<pre><code>")
#print(tabulate(house_df[house_df.index % int(len(house_df)*0.033) == 0][summary_fields], headers=summary_fields, showindex=False, tablefmt="html"))
print(tabulate(house_df.iloc[random_index][summary_fields], headers=summary_fields, showindex=True))
print("</code></pre>")
end_time = datetime.now()
print('\nDuration: {}'.format(end_time - start_time))