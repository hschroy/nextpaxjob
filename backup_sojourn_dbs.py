import pymysql 
import pandas as pd
from email_warn import send_warning

print('\nNow, only copy tables from staging into production if they look ok...')

conn = pymysql.connect(host='34.70.178.230', user='henry', passwd='finley', db='sojourn', port=3306)

cur = conn.cursor()
cur.execute('USE sojourn')

def do_table(tablename):
    # **PLACES
    print('\n**** refreshing from preprocess:  sojourn_db.{tablename} --> sojourn.{tablename} ****'.format(tablename=tablename))
    sql_string = 'select count(id) AS num_records from sojourn_db.{tablename}'.format(tablename=tablename)
    num_rows = 0

    try:
        # see if it exists, then drop it
        cur.execute(sql_string)
        # sojourn_db.{tablename} exists
        num_rows = cur._rows[0][0]
    except pymysql.Error as Error:
        msg = "sojourn_db.{tablename} doesn't exist.".format(tablename=tablename)
        send_warning(msg)
        print('\n' + msg)

    if num_rows > 0:
        # we have {tablename}, let's update main sojourn database
        print('\nFlash Update sojourn.{tablename}, {num_rows} rows:'.format(tablename=tablename,num_rows=num_rows))
        try:
            sql_string = 'DROP TABLE IF EXISTS sojourn.{tablename}'.format(tablename=tablename)
            cur.execute(sql_string)
            print('\n' + sql_string)
            sql_string = 'CREATE TABLE sojourn.{tablename} LIKE sojourn_db.{tablename}'.format(tablename=tablename)
            cur.execute(sql_string)
            print('\n' + sql_string)
            sql_string = 'INSERT sojourn.{tablename} SELECT * FROM sojourn_db.{tablename}'.format(tablename=tablename)
            cur.execute(sql_string)
            print('\n' + sql_string)
            conn.commit()
        except pymysql.Error as Error:
            msg = Error.args[1]
            send_warning(msg)
            print('\n' + msg)

    else:
        msg = "no records in sojourn_db.{tablename}, skipping this sojourn database table update...".format(tablename=tablename)
        send_warning(msg)
        print('\n' + msg)


for table_name in ['places','additional_costs','amenities','calendars','descriptions','pictures','rates','taxes']:
    do_table(table_name)



cur.close()
conn.close()

cur = None

