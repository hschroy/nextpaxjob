import pymysql 
import pandas as pd
from email_warn import send_warning


conn = pymysql.connect(host='34.70.178.230', user='henry', passwd='finley', db='sojourn', port=3306)

cur = conn.cursor()
cur.execute('USE sojourn')

def do_table(databasename, tablename, command_list):
    # **PLACES
    print(f'\n**** upserting from preprocess:  sojourn_db.{tablename} --> {databasename}.{tablename} ****')
    sql_string = f'select count(id) AS num_records from sojourn_db.{tablename}'
    num_rows = 0

    try:
        # see if it exists, then drop it
        cur.execute(sql_string)
        # sojourn_db.{tablename} exists
        num_rows = cur._rows[0][0]
    except pymysql.Error as Error:
        msg = f"sojourn_db.{tablename} doesn't exist."
        send_warning(msg)
        print('\n' + msg)

    if num_rows > 0:
        # we have {tablename}, let's update main sojourn database
        print(f'\nupsert into {databasename}.{tablename}, {num_rows} rows:')
        try:
            for sql_command in command_list:
                print('\n' + sql_command)
                exec_result = cur.execute(sql_command)
                print(f'\n{exec_result} row(s) affected...')
                conn.commit()
        except pymysql.Error as Error:
            msg = Error.args[1]
            send_warning(msg)
            print('\n' + msg)

    else:
        msg = f"no records in sojourn_db.{tablename}, skipping this {databasename} database table update..."
        send_warning(msg)
        print('\n' + msg)

#print('\n ****** upsert into two databases, the sojourn api database and the sojourn_django website database ... ******')
# db_name_list = ['sojourn','sojourn_django']
print('\n ****** upsert into the sojourn api database ... ******')
db_name_list = ['sojourn']
for db_name in db_name_list:

    print(f'\n**** NEXTPAX UPSERT into {db_name.upper()} production database... ****')

    tables = {'places':[
        f'''
        DELETE from {db_name}.places WHERE sojournID LIKE "NP%";
        '''
        ,
        f'''
        INSERT INTO `{db_name}`.`places`(sojournID, property_name, country, place, zip_code, min_persons, max_persons, 
        min_children, number_of_pets, house_type, latitude, longitude, location, picture, currency, rate, wifi, 
        iron, air_conditioning, hot_tub, pool, washer, free_parking, TV, dryer, breakfast, hangers,
        hair_dryer, high_chair, smoke_alarm, pets_allowed, kitchen, heating, fireplace, laptop_friendly,
        crib, carbon_monoxide_alarm, gym, ski_in_out, num_beds, num_bedrooms, num_bathrooms)
        SELECT pl.* FROM
        (SELECT sojournID, property_name, country, place, zip_code, min_persons, max_persons, min_children, 
        number_of_pets, house_type, latitude, longitude, location, picture, currency, rate, wifi, 
        iron, air_conditioning, hot_tub, pool, washer, free_parking, TV, dryer, breakfast, hangers,
        hair_dryer, high_chair, smoke_alarm, pets_allowed, kitchen, heating, fireplace, laptop_friendly,
        crib, carbon_monoxide_alarm, gym, ski_in_out, num_beds, num_bedrooms, num_bathrooms
        FROM `sojourn_db`.`places`) AS pl
        ON DUPLICATE KEY UPDATE sojournID=pl.sojournID, property_name=pl.property_name, country=pl.country, 
        place=pl.place, zip_code=pl.zip_code, min_persons=pl.min_persons, max_persons=pl.max_persons, 
        min_children=pl.min_children, number_of_pets=pl.number_of_pets, house_type=pl.house_type, 
        latitude=pl.latitude, longitude=pl.longitude, location=pl.location, picture=pl.picture, 
        currency=pl.currency, rate=pl.rate, wifi=pl.wifi, iron=pl.iron, air_conditioning=pl.air_conditioning, 
        hot_tub=pl.hot_tub, pool=pl.pool, washer=pl.washer, free_parking=pl.free_parking, TV=pl.TV, dryer=pl.dryer, 
        breakfast=pl.breakfast, hangers=pl.hangers, hair_dryer=pl.hair_dryer, high_chair=pl.high_chair, 
        smoke_alarm=pl.smoke_alarm, pets_allowed=pl.pets_allowed, kitchen=pl.kitchen, heating=pl.heating, 
        fireplace=pl.fireplace, laptop_friendly=pl.laptop_friendly, crib=pl.crib, 
        carbon_monoxide_alarm=pl.carbon_monoxide_alarm, gym=pl.gym, ski_in_out=pl.ski_in_out, num_beds=pl.num_beds, 
        num_bedrooms=pl.num_bedrooms, num_bathrooms=pl.num_bathrooms
        ''']
        ,'additional_costs':[
        f'''
        DELETE from {db_name}.additional_costs WHERE sojournID LIKE "NP%";
        '''
        ,
        f'''
        INSERT INTO {db_name}.additional_costs(sojournID, cost_code, cost_type, cost_currency, 
        cost_amount, amount_type, cost_number, from_date, until_date)
        SELECT ac.* FROM
        (SELECT sojournID, cost_code, cost_type, cost_currency, cost_amount, amount_type, cost_number, 
        from_date, until_date FROM sojourn_db.additional_costs) AS ac
        ON DUPLICATE KEY UPDATE sojournID=ac.sojournID, cost_code=ac.cost_code, cost_type=ac.cost_type, 
        cost_currency=ac.cost_currency, cost_amount=ac.cost_amount, amount_type=ac.amount_type, 
        cost_number=ac.cost_number, from_date=ac.from_date, until_date=ac.until_date;
        ''']
        ,'amenities':[
        f'''
        DELETE from {db_name}.amenities WHERE sojournID LIKE "NP%";
        '''
        ,
        f'''
        INSERT INTO {db_name}.amenities(sojournID, property_code, amenity_value)
        SELECT am.* FROM
        (SELECT sojournID, property_code, amenity_value FROM sojourn_db.amenities) as am
        ON DUPLICATE KEY UPDATE sojournID=am.sojournID, property_code=am.property_code, amenity_value=am.amenity_value
        ''']
        ,'calendars':[
        f'''
        DELETE from {db_name}.calendars WHERE sojournID LIKE "NP%";
        '''
        ,
        f'''
        INSERT INTO {db_name}.calendars(sojournID, calendar_date, quantity, arrival_allowed, departure_allowed, 
        minimum_stay, maximum_stay)
        SELECT cs.* FROM
        (SELECT sojournID, calendar_date, quantity, arrival_allowed, departure_allowed, 
        minimum_stay, maximum_stay FROM sojourn_db.calendars) as cs
        ON DUPLICATE KEY UPDATE sojournID=cs.sojournID, calendar_date=cs.calendar_date, quantity=cs.quantity, 
        arrival_allowed=cs.arrival_allowed, departure_allowed=cs.departure_allowed, minimum_stay=cs.minimum_stay, 
        maximum_stay=cs.maximum_stay
        ''']
        ,'descriptions':[
        f'''
        DELETE from {db_name}.descriptions WHERE sojournID LIKE "NP%";
        '''
        ,
        f'''
        INSERT INTO {db_name}.descriptions(sojournID, language_code, description, description_type)
        SELECT ds.* FROM
        (SELECT sojournID, language_code, description, description_type FROM sojourn_db.descriptions) AS ds
        ON DUPLICATE KEY UPDATE sojournID=ds.sojournID, language_code=ds.language_code, description=ds.description, 
        description_type=ds.description_type
        ''']
        ,'pictures':[
        f'''
        DELETE from {db_name}.pictures WHERE sojournID LIKE "NP%";
        '''
        ,
        f'''
        INSERT INTO {db_name}.pictures(sojournID, file_url, picture_type)
        SELECT ps.* FROM
        (SELECT sojournID, file_url, picture_type FROM sojourn_db.pictures) AS ps
        ON DUPLICATE KEY UPDATE sojournID=ps.sojournID, file_url=ps.file_url, picture_type=ps.picture_type
        ''' ]
        ,'rates':[
        f'''
        DELETE from {db_name}.rates WHERE sojournID LIKE "NP%";
        '''
        ,
        f'''
        INSERT INTO {db_name}.rates(sojournID, from_date, till_date, duration, amount, currency, persons, 
        weekdays, minimum_stay, maximum_stay, extra_person_fee_amount, extra_person_fee_currency)
        SELECT rs.* FROM
        (SELECT sojournID, from_date, till_date, duration, amount, currency, persons, 
        weekdays, minimum_stay, maximum_stay, extra_person_fee_amount, extra_person_fee_currency FROM sojourn_db.rates) as rs
        ON DUPLICATE KEY UPDATE sojournID=rs.sojournID, from_date=rs.from_date, till_date=rs.till_date, 
        duration=rs.duration, amount=rs.amount, currency=rs.currency, persons=rs.persons, 
        weekdays=rs.weekdays, minimum_stay=rs.minimum_stay, maximum_stay=rs.maximum_stay, 
        extra_person_fee_amount=rs.extra_person_fee_amount, extra_person_fee_currency=rs.extra_person_fee_currency
        ''']
        ,'taxes':[
        f'''
        DELETE from {db_name}.taxes WHERE sojournID LIKE "NP%";
        '''
        ,
        f'''
        INSERT INTO {db_name}.taxes(sojournID, tax_type, percentage, included)
        SELECT tx.* FROM
        (SELECT sojournID, tax_type, percentage, included FROM sojourn_db.taxes) AS tx
        ON DUPLICATE KEY UPDATE sojournID=tx.sojournID, tax_type=tx.tax_type, percentage=tx.percentage, 
        included=tx.included
        ''']
    }

    # ie., to run one table only: 
    # do_table(db_name, 'pictures',tables['pictures'])

    [do_table(db_name, x, tables[x]) for x in tables]

cur.close()
conn.close()

cur = None

