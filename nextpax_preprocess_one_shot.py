from flask import Flask, request, render_template, render_template_string, session, redirect, url_for, flash, Markup, g
from flask_bootstrap import Bootstrap
from flask_moment import Moment 
import datetime
from datetime import date
from flask_wtf import FlaskForm 
from wtforms import StringField, SubmitField, DateField, IntegerField
import timestring
import json
from wtforms.validators import DataRequired, Length 
import os
import pandas as pd
from flask_sqlalchemy import SQLAlchemy 
from sqlalchemy import create_engine, func, or_
import pymysql 
from sqlalchemy.types import Integer, Text, String, Numeric, DateTime, Boolean, Date, UnicodeText
from flask_table import Table, Col, LinkCol
from geopy.geocoders import GoogleV3
from geopy import distance
import ccy

# moving data from nextpax staging (ingest) to sojourn dev api db

geolocator = GoogleV3(api_key="AIzaSyA_gzlbNDzI-CfN0rvNlpL9UFv1vkLVGjY",
     domain='maps.googleapis.com')

start_time = datetime.datetime.now()

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
app.config['SECRET_KEY'] = 'obiandfinley2020!'
app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://{user}:{passwd}@{host}/{db}?charset=utf8mb4".format(host='35.192.116.102', user='henry', passwd='finley', db='nextpax', port=3306)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
bootstrap = Bootstrap(app)
moment = Moment(app)

# if local machine
# 35.192.116.102 (new sojourn-staging-db) db:nextpax
#engine = create_engine("mysql+pymysql://{user}:{passwd}@{host}/{db}".format(host='35.192.116.102', user='henry', passwd='finley', db='nextpax', port=3306))

#sojourn_engine = create_engine("mysql+pymysql://{user}:{passwd}@{host}/{db}?charset=utf8mb4".format(host='34.70.178.230', user='henry', passwd='finley', db='sojourn', port=3306))
# instead of creating into sojourn database directly, stage into sojourn_db, in case something goes wrong...
sojourn_engine = create_engine("mysql+pymysql://{user}:{passwd}@{host}/{db}?charset=utf8mb4".format(host='34.70.178.230', user='henry', passwd='finley', db='sojourn_db', port=3306))

class module_object:
    pass
# create search_area variable global to this module
_m = module_object()
_m.search_area = None
_m.check_in = date.today()
_m.date_today = date.today()
_m.check_out = date.today() + datetime.timedelta(1)
_m.num_guests = 1


# Nextpax data objects:
# lookup table
class tax_type(db.Model):
    __tablename__ = '_TaxType'
    TaxType = db.Column(db.String(50), primary_key=True)
    Description = db.Column(db.Text)
    taxes = db.relationship('tax', backref='tax_type', lazy='dynamic')

    def __repr__(self):
        return '<TaxType %r %r>' % (self.TaxType, self.Description)

# lookup table
class house_type(db.Model):
    __tablename__ = '_HouseType'
    HouseType = db.Column(db.String(50), primary_key=True)
    Description = db.Column(db.Text)
    houses = db.relationship('house', backref='house_type', lazy='dynamic')

    def __repr__(self):
        return self.Description

# lookup table
class property_code(db.Model):
    __tablename__ = '_PropertyCode'
    PropertyCode = db.Column(db.String(50), primary_key=True)
    Description = db.Column(db.Text)
    amenities = db.relationship('amenity', backref='property_code', lazy='dynamic')

    def __repr__(self):
        return self.Description

# lookup table
class cost_code(db.Model):
    __tablename__ = '_CostCode'
    CostCode = db.Column(db.String(50), primary_key=True)
    Description = db.Column(db.Text)
    additional_costs = db.relationship('additional_cost', backref='cost_code', lazy='dynamic')

    def __repr__(self):
        return self.Description

# lookup table
class cost_type(db.Model):
    __tablename__ = '_CostType'
    CostType = db.Column(db.String(50), primary_key=True)
    Description = db.Column(db.Text)
    additional_costs = db.relationship('additional_cost', backref='cost_type', lazy='dynamic')

    def __repr__(self):
        return self.Description

# lookup table
class amount_type(db.Model):
    __tablename__ = '_AmountType'
    AmountType = db.Column(db.String(50), primary_key=True)
    Description = db.Column(db.Text)
    additional_costs = db.relationship('additional_cost', backref='amount_type', lazy='dynamic')

    def __repr__(self):
        return self.Description

# lookup table
class description_type(db.Model):
    __tablename__ = '_DescriptionType'
    TypeID = db.Column(db.String(50), primary_key=True)
    Description = db.Column(db.Text)
    descriptions = db.relationship('description', backref='description_type', lazy='dynamic')

    def __repr__(self):
        return self.Description

# lookup table
class picture_type(db.Model):
    __tablename__ = '_PictureType'
    TypeID = db.Column(db.String(50), primary_key=True)
    Description = db.Column(db.Text)
    pictures = db.relationship('picture', backref='picture_type', lazy='dynamic')

    def __repr__(self):
        return self.Description

# xml: paxgenerator_house_additional_costs
class additional_cost(db.Model):
    __tablename__ = 'additional_costs'
    id = db.Column(db.BigInteger, primary_key=True)
    Partner = db.Column(db.String(50))
    HouseID = db.Column(db.String(50), db.ForeignKey('houses.HouseID'))
    CostCode = db.Column(db.String(50), db.ForeignKey('_CostCode.CostCode'))
    CostType = db.Column(db.String(50), db.ForeignKey('_CostType.CostType'))
    CostAmount = db.Column(db.Numeric(10,2), index=True)
    CostAmountType = db.Column(db.String(50), db.ForeignKey('_AmountType.AmountType'))
    CostCurrency = db.Column(db.String(50))
    CostNumber = db.Column(db.Integer)
    FromDate = db.Column(db.Date, index=True)
    UntilDate = db.Column(db.Date, index=True)

    def __repr__(self):
        return '<additional_cost %r %r %r %r>' % (self.HouseID, self.CostCode, self.CostType, self.CostAmount)

# xml: paxgenerator_house_pictures
class picture(db.Model):
    __tablename__ = 'pictures'
    id = db.Column(db.BigInteger, primary_key=True)
    Partner = db.Column(db.String(50))
    HouseID = db.Column(db.String(50), db.ForeignKey('houses.HouseID'))
    FileUrl = db.Column(db.Text)
    TypeID = db.Column(db.String(50), db.ForeignKey('_PictureType.TypeID'))

    def __repr__(self):
        return self.FileUrl
        #return "<img src='{}' style='object-fit: cover;'></img>".format(self.FileUrl)
        
# xml: paxgenerator_rates
class rate(db.Model):
    __tablename__ = 'rates'
    id = db.Column(db.BigInteger, primary_key=True)
    PropertyManagementSystem = db.Column(db.String(50))
    HouseID = db.Column(db.String(50), db.ForeignKey('houses.HouseID'))
    PropertyManager = db.Column(db.String(50))
    fromDate = db.Column(db.Date, index=True)
    tillDate = db.Column(db.Date, index=True)
    duration = db.Column(db.Integer)
    amount = db.Column(db.Numeric(10,2))
    currency = db.Column(db.String(50))
    persons = db.Column(db.String(50))
    weekdays = db.Column(db.String(50))
    minimumStay = db.Column(db.Integer)
    maximumStay = db.Column(db.Integer)
    extraPersonFeeAmount = db.Column(db.Numeric(10,2))
    extraPersonFeeCurrency = db.Column(db.String(50))


    def __repr__(self):
        return 'HouseID: %r fromDate: %r duration: %r amount: %r currency: %r persons: %r extraPersonFeeAmount: %r' % \
            (self.HouseID, self.fromDate, self.duration, self.amount, self.currency, self.persons, self.extraPersonFeeAmount)

# xml: paxgenerator_house_descriptions
class description(db.Model):
    __tablename__ = 'descriptions'
    id = db.Column(db.BigInteger, primary_key=True)
    Partner = db.Column(db.String(50))
    HouseID = db.Column(db.String(50), db.ForeignKey('houses.HouseID'))
    LanguageCode = db.Column(db.String(5))
    Description = db.Column(db.UnicodeText)
    TypeID = db.Column(db.String(50), db.ForeignKey('_DescriptionType.TypeID'))

    def __repr__(self):
        return self.Description
    

# xml: paxgenerator_house_properties
class amenity(db.Model):
    __tablename__ = 'amenities'
    id = db.Column(db.BigInteger, primary_key=True)
    Partner = db.Column(db.String(50))
    HouseID = db.Column(db.String(50), db.ForeignKey('houses.HouseID'))
    PropertyCode = db.Column(db.String(50), db.ForeignKey('_PropertyCode.PropertyCode'))
    AmenityValue = db.Column(db.String(50))

    def __repr__(self):
        return self.property_code.Description + ' ' + self.AmenityValue
    
# xml: paxgenerator_house_taxes
class tax(db.Model):
    __tablename__ = 'taxes'
    id = db.Column(db.BigInteger, primary_key=True)
    Partner = db.Column(db.String(50))
    HouseID = db.Column(db.String(50), db.ForeignKey('houses.HouseID'))
    TaxType = db.Column(db.String(50), db.ForeignKey('_TaxType.TaxType'))
    Percentage = db.Column(db.Numeric(11,8))
    Included = db.Column(db.Boolean)

    def __repr__(self):
        return '<tax %r %r %r>' % (self.TaxType, self.Percentage, self.tax_type.Description)

# xml: paxgenerator_calendars
class calendar(db.Model):
    __tablename__ = 'calendars'
    id = db.Column(db.BigInteger, primary_key=True)
    PropertyManagementSystem = db.Column(db.String(50))
    HouseID = db.Column(db.String(50), db.ForeignKey('houses.HouseID'))
    PropertyManager = db.Column(db.String(50))
    calendarDate = db.Column(db.Date, index=True)
    quantity = db.Column(db.Integer)
    arrivalAllowed = db.Column(db.Boolean)
    departureAllowed = db.Column(db.Boolean)
    minimumStay = db.Column(db.Integer)
    maximumStay = db.Column(db.Integer)

    def __repr__(self):
        return 'HouseID: %r calendarDate: %r quantity: %r arrivalAllowed: %r departureAllowed: %r minimumStay: %r maximumStay: %r' % \
            (self.HouseID, self.calendarDate, self.quantity, self.arrivalAllowed, self.departureAllowed, self.minimumStay, self.maximumStay)

# xml: paxgenerator_houses
class house(db.Model):
    __tablename__ = 'houses'
    id = db.Column(db.BigInteger, primary_key=True)
    Partner = db.Column(db.String(50))
    Brand = db.Column(db.String(50))
    HouseID = db.Column(db.String(50), nullable=False, unique=True, index=True)
    Country = db.Column(db.String(50))
    Place = db.Column(db.Text)
    ZipCode = db.Column(db.String(50))
    HouseName = db.Column(db.Text)
    MinPersons = db.Column(db.Integer)
    MaxPersons = db.Column(db.Integer)
    MinChildren = db.Column(db.Integer)
    NumberOfPets = db.Column(db.Integer)
    HouseType = db.Column(db.Integer, db.ForeignKey('_HouseType.HouseType'))
    Latitude = db.Column(db.Numeric(10,8))
    Longitude = db.Column(db.Numeric(11,8))
    Currency = db.Column(db.String(50))
    LicenseNumber = db.Column(db.String(50))
    taxes = db.relationship('tax', backref='house', lazy='dynamic')
    calendars = db.relationship('calendar', backref='house', lazy='dynamic')
    amenities = db.relationship('amenity', backref='house', lazy='dynamic')
    additional_costs = db.relationship('additional_cost', backref='house', lazy='dynamic')
    descriptions = db.relationship('description', backref='house', lazy='dynamic')
    pictures = db.relationship('picture', backref='house', lazy='dynamic')
    rates = db.relationship('rate', backref='house', lazy='dynamic')

    def house_latitude(self):
        if self.Latitude == 0:
            mylookup = self.Place + ' ' + self.Country + ' ' + self.ZipCode
            myplace = geolocator.geocode(mylookup)

            return myplace.point.latitude
        else:
            return self.Latitude

    def house_longitude(self):
        if self.Longitude == 0:
            mylookup = self.Place + ' ' + self.Country + ' ' + self.ZipCode
            myplace = geolocator.geocode(mylookup)

            return myplace.point.longitude
        else:
            return self.Longitude

    def house_additional_costs(self):
        try:
            return  ', '.join([(str(addcost.cost_code) + "| " + str(addcost.cost_type) + "| " + str(addcost.amount_type) + \
                "| " + str(addcost.CostCurrency) + " {0:.2f}".format(addcost.CostAmount)) for addcost in self.additional_costs])
        except:
            return ''

    def amenity_list(self):
        try:
            return  ', '.join([(amenity.property_code.Description + ': ' \
                + amenity.AmenityValue) for amenity in self.amenities])
        except :
            return ''

    def house_descriptions(self):
        try:
            return  ', '.join([(description.description_type.Description + ': ' \
                + description.Description) for description in self.descriptions])
        except :
            return ''

    def house_pictures(self):
        try:
            return  ', '.join([(picture.picture_type.Description + ': ' + picture.FileUrl) \
                for picture in self.pictures])
        except :
            return ''

    def house_taxes(self):
        try:
            return  ', '.join([tax.tax_type.Description + ' {:.1%}'.format(tax.Percentage/100) \
                for tax in self.taxes])
        except :
            return ''

    def rate_value(self):
        rate_duration = 1  # get day rate
        rates_query = self.rates.filter(rate.fromDate >= _m.date_today, rate.duration == rate_duration)

        if rates_query.count() > 0:
            rates_query_first = rates_query.first()

            rate_amount = rates_query_first.amount
            rate_currency = rates_query_first.currency
            return rate_amount
        else:
            return 0
        '''
        #  house1.rates.filter(rate.fromDate == date(2020,6,19), rate.persons == '3-8', rate.duration == 6).all() 
        #  (_m.check_out - _m.check_in).days
        # _m.num_guests
        if _m.num_guests > 2:
            person_filter = '3-8'
        else:
            person_filter = '1-2'

        rate_duration = (_m.check_out - _m.check_in).days   
        rates_query = self.rates.filter(rate.fromDate >= _m.check_in, 
            rate.persons == person_filter, rate.duration == rate_duration)

        if rates_query.count() > 0:
            rates_query_first = rates_query.first()

            rate_amount = rates_query_first.amount
            rate_currency = rates_query_first.currency
            rate_amount_per_extra = rates_query_first.extraPersonFeeAmount

            if _m.num_guests > 2:
                rate_amount = rate_amount + (_m.num_guests - 2) * rate_amount_per_extra

            return rate_currency + " {:20,.2f}".format(rate_amount) + " / " + str(rate_duration) + ' day' + \
                ('s' if rate_duration > 1 else '')
        else:
            return ''
        '''


    def calendars_all(self):
        date_from = self.calendars.with_entities(func.min(calendar.calendarDate)).scalar().strftime('%x')
        date_to = self.calendars.with_entities(func.max(calendar.calendarDate)).scalar().strftime('%x')
        return date_from + ' to '+ date_to

    def calendars_booked(self):
        bookings_from = self.calendars.with_entities(func.min(calendar.calendarDate)).filter(calendar.arrivalAllowed == False).scalar().strftime('%x')
        bookings_to = self.calendars.with_entities(func.max(calendar.calendarDate)).filter(calendar.arrivalAllowed == False).scalar().strftime('%x')
        return bookings_from + ' to ' + bookings_to

    def has_wifi(self):
        wifi = self.amenities.filter(amenity.PropertyCode == 'W07')
        if wifi.count() > 0:
            return wifi.first().AmenityValue == 'Y'
        else:
            return False

    def has_iron(self):
        iron = self.amenities.filter(amenity.PropertyCode == 'I03')
        if iron.count() > 0:
            return iron.first().AmenityValue == 'Y'
        else:
            return False

    def has_air_conditioning(self):
        air_conditioning = self.amenities.filter(or_(amenity.PropertyCode == 'A01', amenity.PropertyCode == 'A05', \
            amenity.PropertyCode == 'A06'))
        #  * . Air conditioning A01 Air conditioning ('Y' = available) A05	Air conditioning in living room ('Y' = available) 
        # A06 Air conditioning in bedroom(s) ('Y' = available)  
        if air_conditioning.count() > 0:
            return air_conditioning.first().AmenityValue == 'Y'
        else:
            return False

    def has_hot_tub(self):
        hot_tub = self.amenities.filter(or_(amenity.PropertyCode == 'W04', amenity.PropertyCode == 'J01')) 
        # whirlpool | jacuzzi 
        # ('Y' = available, 'I' = indoor available, 'O' = outdoor available)
        if hot_tub.count() > 0:
            firstValue = hot_tub.first().AmenityValue
            return firstValue == 'Y' or firstValue == 'I' or firstValue == 'O'
        else:
            return False

    def has_pool(self):
        house_pool = self.amenities.filter(or_(amenity.PropertyCode == 'P18', amenity.PropertyCode == 'P04', \
            amenity.PropertyCode == 'P10', amenity.PropertyCode == 'P05', amenity.PropertyCode == 'P09', \
                amenity.PropertyCode == 'C16'))
        #  # (P18 Swimming pool ('Y' = available)), P04 Indoor swimming pool ('Y' = available, 'P' = private, 'C' = communal), 
        # P05 Outdoor swimming pool ('Y' = available, 'P' = private, 'C' = communal), P09 Private swimming pool, can be inside 
        # or outside ('Y' = available) , P10 Heated swimming pool, can be inside or outside ('Y' = available), C16	Children 
        # swimming Pool ('Y' = available)
        if house_pool.count() > 0:
            firstValue = house_pool.first().AmenityValue
            return firstValue == 'Y' or firstValue == 'P' or firstValue == 'C'
        else:
            return False

    def has_washer(self):
        house_washer = self.amenities.filter(or_(amenity.PropertyCode == 'L03', amenity.PropertyCode == 'W01'))
        # (L03 Laundromat ('Y' = available, 'C' = in the city, 'P' = park facility, 'S' = service)), W01	
        # Washing machine ('Y' = available)             
        if house_washer.count() > 0:
            firstValue = house_washer.first().AmenityValue
            return firstValue == 'Y'
        else:
            return False

    def has_free_parking(self):
        house_park_free = self.amenities.filter(or_(amenity.PropertyCode == 'P21', amenity.PropertyCode == 'G01'))
        # . Free parking on premises (P21	Parking (reservation needed) ('Y' = Free, 'S' = Surcharge)), G01 Garage or 
        # parking place ('Y' = Free, 'C' = Carport, 'G' = Garage, 'P' = Private Parking Place, 'O' = Outside the plot, 
        # 'S' = Street parking/nearby parking) 
        if house_park_free.count() > 0:
            firstValue = house_park_free.first().AmenityValue
            return firstValue == 'Y'
        else:
            return False

    def has_TV(self):
        house_TV = self.amenities.filter(or_(amenity.PropertyCode == 'L12', amenity.PropertyCode == 'F10', amenity.PropertyCode == 'S05', amenity.PropertyCode == 'T01'))
        # * . TV L12 Shared lounge/TV area F10 Flat-screen TV ('Y' = available) S05 Satellite TV ('Y' = available)  
        if house_TV.count() > 0:
            firstValue = house_TV.first().AmenityValue
            return firstValue == 'Y'
        else:
            return False

    def has_dryer(self):
        house_dryer = self.amenities.filter(amenity.PropertyCode == 'D08')
        # * . Dryer (D08 Dryer ('Y' = available))
        if house_dryer.count() > 0:
            firstValue = house_dryer.first().AmenityValue
            return firstValue == 'Y'
        else:
            return False

    def has_breakfast(self):
        house_breakfast = self.amenities.filter(amenity.PropertyCode == 'B23')
        #. Breakfast (B23 Breakfast ('Y' = Free, 'S' = Surcharge))
        if house_breakfast.count() > 0:
            firstValue = house_breakfast.first().AmenityValue
            return firstValue == 'Y' or firstValue == 'S'
        else:
            return False

    def has_hangers(self):
        house_hangers = self.amenities.filter(amenity.PropertyCode == 'H42')
        # . Hangers (H42 Hangers ('Y' = available))
        if house_hangers.count() > 0:
            firstValue = house_hangers.first().AmenityValue
            return firstValue == 'Y'
        else:
            return False

    def has_hair_dryer(self):
        house_hair_dryer = self.amenities.filter(amenity.PropertyCode == 'H41')
        # . Hair dryer (H41 Hair dryer ('Y' = available))
        if house_hair_dryer.count() > 0:
            firstValue = house_hair_dryer.first().AmenityValue
            return firstValue == 'Y'
        else:
            return False

    def has_smoke_alarm(self):
        house_smoke_alarm = self.amenities.filter(amenity.PropertyCode == 'S44')
        # . Smoke alarm (S44 Smoke alarm ('Y' = available))
        if house_smoke_alarm.count() > 0:
            firstValue = house_smoke_alarm.first().AmenityValue
            return firstValue == 'Y'
        else:
            return False

    def has_high_chair(self):
        house_high_chair = self.amenities.filter(amenity.PropertyCode == 'C14')
        # . High chair (C14 Baby chair(s)/highchair(s) on request)
        return house_high_chair.count() > 0

    def pets_allowed(self):
        house_pets_ok = self.amenities.filter(amenity.PropertyCode == 'P02')
        # * . P02 House is suitable for pets ('Y' = applicable to this house, 'R' = on request)
        if house_pets_ok.count() > 0:
            firstValue = house_pets_ok.first().AmenityValue
            return firstValue == 'Y' or firstValue == 'R'
        else:
            return False

    def has_kitchen(self):
        house_kitchen = self.amenities.filter(amenity.PropertyCode == 'K04')
        # . Kitchen (K04 Kitchen ('Y' = available))
        if house_kitchen.count() > 0:
            firstValue = house_kitchen.first().AmenityValue
            return firstValue == 'Y'
        else:
            return False

    def has_heating(self):
        house_heating = self.amenities.filter(amenity.PropertyCode == 'K04')
        # . Heating (H40	Heating ('C' = central heating, 'E' = electric, 'F' = floor, 'G' = gas,  
        # 'H' = hot air, 'S' = stove, 'Y' = available))
        if house_heating.count() > 0:
            firstValue = house_heating.first().AmenityValue
            return firstValue == 'C' or firstValue == 'E' or firstValue == 'F' or firstValue == 'G' \
                or firstValue == 'H' or firstValue == 'S' or firstValue == 'Y'
        else:
            return False

    def has_fireplace(self):
        house_fireplace = self.amenities.filter(amenity.PropertyCode == 'F02')
        # . Indoor fireplace (F02	Fireplace or heater ('Y' = available))
        if house_fireplace.count() > 0:
            firstValue = house_fireplace.first().AmenityValue
            return firstValue == 'Y'
        else:
            return False

    def laptop_friendly(self):
        house_laptop = self.amenities.filter(amenity.PropertyCode == 'L06')
        # . Laptop-friendly workspace (L06 Laptop friendly ('Y' = available))
        if house_laptop.count() > 0:
            firstValue = house_laptop.first().AmenityValue
            return firstValue == 'Y'
        else:
            return False

    def has_crib(self):
        house_crib = self.amenities.filter(amenity.PropertyCode == 'B15')
        # . Crib (B15 Babybed ('Y' = available))
        if house_crib.count() > 0:
            firstValue = house_crib.first().AmenityValue
            return firstValue == 'Y'
        else:
            return False

    def carbon_monoxide(self):
        house_CO = self.amenities.filter(amenity.PropertyCode == 'C18')
        # . Carbon monoxide alarm (C18 Carbon monoxide detector ('Y' = available))
        if house_CO.count() > 0:
            firstValue = house_CO.first().AmenityValue
            return firstValue == 'Y'
        else:
            return False

    def has_gym(self):
        house_gym = self.amenities.filter(amenity.PropertyCode == 'F12')
        # . Gym (F12 Fitness/Gym ('Y' = available))
        if house_gym.count() > 0:
            firstValue = house_gym.first().AmenityValue
            return firstValue == 'Y'
        else:
            return False

    def ski_in_out(self):
        house_ski_in = self.amenities.filter(or_(amenity.PropertyCode == 'S50', amenity.PropertyCode == 'S51'))
        # . Ski-in/ski-out (S50 Ski in Property Location) (S51 Ski out Property Location)
        return house_ski_in.count() > 0

    def num_beds(self):
        # * . # of Beds    (B13 Number of sleeping spots in bunkbeds)
        #   (D09 Number of sleeping spots in double beds)
        #   (K01 Number of (extra) cots / kids beds) 
        # (K05 Number of sleeping spots in King beds) 
        #   (S30 Number of sleeping spots in single beds) 
        # (S31 Number of sleeping spots on sofas/sofabeds)

        if self.amenities.filter(amenity.PropertyCode == 'B17').count() > 0:
            # (B17 Total number of beds)
            total_num_beds = int(self.amenities.filter(amenity.PropertyCode == 'B17').first().AmenityValue)
        else:
            total_num_beds = 0

        if self.amenities.filter(amenity.PropertyCode == 'S02').count() > 0:
            # (S02 Number of single beds)
            num_single_beds = int(self.amenities.filter(amenity.PropertyCode == 'S02').first().AmenityValue)
        else:
            num_single_beds = 0

        if self.amenities.filter(amenity.PropertyCode == 'D02').count() > 0:
            # (D02 Number of double beds)
            num_double_beds = int(self.amenities.filter(amenity.PropertyCode == 'D02').first().AmenityValue)
        else:
            num_double_beds = 0

        if self.amenities.filter(amenity.PropertyCode == 'G02').count() > 0:
            # (G02 Number of king size beds (= extra large double bed))
            num_king_beds = int(self.amenities.filter(amenity.PropertyCode == 'G02').first().AmenityValue)
        else:
            num_king_beds = 0

        if self.amenities.filter(amenity.PropertyCode == 'K06').count() > 0:
            # (K06 Number of King Beds (= extra large double bed))
            num_king_beds = num_king_beds + int(self.amenities.filter(amenity.PropertyCode == 'K06').first().AmenityValue)
        else:
            num_king_beds = num_king_beds

        if self.amenities.filter(amenity.PropertyCode == 'Q02').count() > 0:
            # (Q02 Number of Queen beds)
            num_queen_beds = int(self.amenities.filter(amenity.PropertyCode == 'Q02').first().AmenityValue)
        else:
            num_queen_beds = 0

        if self.amenities.filter(amenity.PropertyCode == 'B06').count() > 0:
            #(B06 Number of bunk beds)
            num_bunk_beds = int(self.amenities.filter(amenity.PropertyCode == 'B06').first().AmenityValue)
        else:
            num_bunk_beds = 0

        return max((num_single_beds + num_double_beds + num_king_beds + num_queen_beds + num_bunk_beds), total_num_beds)

    def num_bedrooms(self):
        # * . # of Bedrooms (B02 Number of bedrooms)
        house_bedrooms = self.amenities.filter(amenity.PropertyCode == 'B02')
        if house_bedrooms.count() > 0:
            return house_bedrooms.first().AmenityValue
        else:
            return 0

    def num_bathrooms(self):
        # * . # of Bathrooms (B01 Number of bathrooms)
        house_bathrooms = self.amenities.filter(amenity.PropertyCode == 'B01')
        if house_bathrooms.count() > 0:
            return house_bathrooms.first().AmenityValue
        else:
            return 0

    # calculate distance of house from search location
    def distance(self):
        return "{:20,.2f}".format(distance.distance(_m.search_area.point, 
            (self.Latitude, self.Longitude)).km)

    # is house available for given check-in/check-out/number of guests?
    def available(self):
        return self.MinPersons <= _m.num_guests and self.MaxPersons >= _m.num_guests and \
            self.calendars.filter(calendar.calendarDate == _m.check_in, calendar.maximumStay >= \
            (_m.check_out - _m.check_in).days, calendar.arrivalAllowed).count() > 0

    # geolocate the house address from coordinates
    def location(self):
        housepoint = self.Latitude, self.Longitude
        houseplace = geolocator.reverse(housepoint, exactly_one=True)
        if houseplace is None:
            mylookup = self.Place + ' ' + self.Country + ' ' + self.ZipCode
            myplace = geolocator.geocode(mylookup)
            if myplace is None:
                return ''
            else:
                return myplace.address
        else:
            return houseplace.address


    def currency_name(self):
        #return ccy.currency(self.Currency).name
        return self.Currency
    
    def first_pic(self):
        pic_url = self.pictures.first()
        if pic_url is None:
            return ''
        else:
            return pic_url.FileUrl

    def sojournID(self):
        return 'nextpax.' + self.HouseID

    def __repr__(self):
        return '<house %r %r %r %r>' % (self.HouseID, self.Country, self.Place, self.HouseName)

# format decimals in flask_table datatable (SojournTable)
class PointCol(Col):
    def td_format(self, content):
        return '{0:.3f}'.format(content)



if _m.search_area is None:
    _m.search_area = geolocator.geocode("Santa Fe NM")


# # **PLACES
# print('\nnextpax preprocess: UPDATING sojourn_db.places')
# fieldschema={"sojournID": String(50), "property_name": Text, 'country': String(50), 'place': Text, 
#     'zip_code': String(50), 'min_persons': Integer, 'max_persons': Integer, 'min_children': Integer, 'number_of_pets': Integer, 
#     'house_type': Text, 'latitude': Numeric(10,8), 'longitude': Numeric(11,8), "location":  Text, 'picture': Text, 'currency': String(50), 'rate': Numeric(10,2), 
#     'wifi': Boolean, 'iron': Boolean, 'air_conditioning': Boolean, 'hot_tub': Boolean, 'pool': Boolean, 'washer': Boolean, 
#     'free_parking': Boolean, 'TV': Boolean, 'dryer': Boolean, 'breakfast': Boolean, 'hangers': Boolean, 'hair_dryer': Boolean, 
#     'high_chair': Boolean, 'smoke_alarm': Boolean, 'pets_allowed': Boolean, 'kitchen': Boolean, 'heating': Boolean, 
#     'fireplace': Boolean, 'laptop_friendly': Boolean, 'crib': Boolean, 'carbon_monoxide_alarm': Boolean, 'gym': Boolean, 
#     'ski_in_out': Boolean, 'num_beds': Integer, 'num_bedrooms': Integer, 'num_bathrooms': Integer}

# places_df = pd.DataFrame([('NP' + x.HouseID, x.HouseName, x.Country, x.Place, x.ZipCode, x.MinPersons, 
#     x.MaxPersons, x.MinChildren, x.NumberOfPets, x.house_type.Description, x.house_latitude(), x.house_longitude(), x.location(), 
#     x.first_pic(), x.currency_name(), x.rate_value(),
#     x.has_wifi(), x.has_iron(), x.has_air_conditioning(), x.has_hot_tub(), x.has_pool(), x.has_washer(), x.has_free_parking(), 
#     x.has_TV(), x.has_dryer(), x.has_breakfast(), x.has_hangers(), x.has_hair_dryer(), x.has_high_chair(), x.has_smoke_alarm(), 
#     x.pets_allowed(), x.has_kitchen(), x.has_heating(), x.has_fireplace(), x.laptop_friendly(), x.has_crib(), x.carbon_monoxide(), 
#     x.has_gym(), x.ski_in_out(), x.num_beds(), x.num_bedrooms(), x.num_bathrooms()) for x in house.query.all()], 
#     columns=('sojournID', 'property_name', 'country', 'place', 'zip_code', 'min_persons', 'max_persons', 
#     'min_children', 'number_of_pets', 'house_type', 'latitude', 'longitude', 'location', 'picture', 'currency', 'rate', 'wifi', 'iron', 
#     'air_conditioning', 'hot_tub', 'pool', 'washer', 'free_parking', 'TV', 'dryer', 'breakfast', 'hangers', 'hair_dryer', 
#     'high_chair', 'smoke_alarm', 'pets_allowed', 'kitchen', 'heating', 'fireplace', 'laptop_friendly', 'crib', 
#     'carbon_monoxide_alarm', 'gym', 'ski_in_out', 'num_beds', 'num_bedrooms', 'num_bathrooms'))

# places_df.index = places_df.index.rename('id')

# print('\n# of places:')
# print(places_df.count()['sojournID'])

# places_df.to_sql('places', con = sojourn_engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema) 


# # **TAXES
# print('\nnextpax preprocess: UPDATING sojourn_db.taxes')
# fieldschema={"sojournID": String(50), "tax_type": Text, "percentage":  Numeric(11,8), "included": 
#     Boolean}

# taxes_df = pd.DataFrame([('NP' + x.HouseID, x.tax_type.Description, x.Percentage, x.Included) for x in 
#     tax.query.all()], columns=('sojournID', 'tax_type', 'percentage', 'included'))

# taxes_df.index = taxes_df.index.rename('id')

# print('\n# of taxes:')
# print(taxes_df.count()['sojournID'])

# taxes_df.to_sql('taxes', con = sojourn_engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema) 

# # **CALENDARS
# print('\nnextpax preprocess: UPDATING sojourn_db.calendars')
# fieldschema={"sojournID": String(50), 'calendar_date': Date, 'quantity': Integer, 'arrival_allowed': 
#     Boolean, 'departure_allowed': Boolean, 'minimum_stay': Integer, 'maximum_stay': Integer}

# calendars_df = pd.DataFrame([('NP' + x.HouseID, x.calendarDate, x.quantity, x.arrivalAllowed, x.departureAllowed, 
#     x.minimumStay, x.maximumStay) for x in calendar.query.all()], columns=('sojournID', 'calendar_date', 'quantity', 
#     'arrival_allowed', 'departure_allowed', 'minimum_stay', 'maximum_stay'))

# print('\n# of calendars records:')
# print(calendars_df.count()['sojournID'])

# calendars_df.index = calendars_df.index.rename('id')

# calendars_df.to_sql('calendars', con = sojourn_engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema) 

# # **AMENITIES
# print('\nnextpax preprocess: UPDATING sojourn_db.amenities')
# fieldschema={"sojournID": String(50), 'property_code': String(255), 'amenity_value': String(50)}

# amenities_df = pd.DataFrame([('NP' + x.HouseID, x.property_code.Description, x.AmenityValue) for x in amenity.query.all()], 
#     columns=('sojournID', 'property_code', 'amenity_value'))

# print('\n# of amenities:')
# print(amenities_df.count()['sojournID'])

# amenities_df.index = amenities_df.index.rename('id')

# amenities_df.to_sql('amenities', con = sojourn_engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema) 

# **ADDITIONAL COSTS
# print('\nnextpax preprocess: UPDATING sojourn_db.additional_costs')
# fieldschema={"sojournID": String(50), 'cost_code': String(50), 'cost_type': 
#     String(25), 'cost_currency': String(50), 'cost_amount': Numeric(10,2), 'amount_type': String(25), 'cost_number': Integer, 
#     'from_date': Date, 'until_date': Date}

# additional_costs_df = pd.DataFrame([('NP' + x.HouseID, '' if x.cost_code is None else x.cost_code.Description, 
#     x.cost_type.Description, x.CostCurrency, x.CostAmount, x.amount_type.Description, x.CostNumber, x.FromDate, x.UntilDate) 
#     for x in additional_cost.query.all()], columns=('sojournID', 'cost_code', 'cost_type', 'cost_currency', 'cost_amount', 
#     'amount_type', 'cost_number', 'from_date', 'until_date'))

# additional_costs_df.index = additional_costs_df.index.rename('id')

# print('\n# of additional costs:')
# print(additional_costs_df.count()['sojournID'])

# additional_costs_df.to_sql('additional_costs', con = sojourn_engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema) 

# # **DESCRIPTIONS
# print('\nnextpax preprocess: UPDATING sojourn_db.descriptions')
# fieldschema={"sojournID": String(50), 'language_code': String(5), 'description': UnicodeText, 'description_type': 
#     Text}

# descriptions_df = pd.DataFrame([('NP' + x.HouseID, x.LanguageCode, x.Description, x.description_type.Description) for x in 
#     description.query.all()], columns=('sojournID', 'language_code', 'description', 'description_type'))

# descriptions_df.index = descriptions_df.index.rename('id')

# print('\n# of descriptions:')
# print(descriptions_df.count()['sojournID'])

# descriptions_df.to_sql('descriptions', con = sojourn_engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema) 

# ** PICTURES
print('\nnextpax preprocess: UPDATING sojourn_db.pictures')
fieldschema={"sojournID": String(50), 'file_url': Text, 'picture_type': Text}

pictures_df = pd.DataFrame([('NP' + x.HouseID, x.FileUrl, x.picture_type.Description) for x in picture.query.all()], 
    columns=('sojournID', 'file_url', 'picture_type'))

pictures_df.index = pictures_df.index.rename('id')

print('\n# of pictures:')
print(pictures_df.count()['sojournID'])

pictures_df.to_sql('pictures', con = sojourn_engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema) 

# # **RATES
# print('\nnextpax preprocess: UPDATING sojourn_db.rates')
# fieldschema={"sojournID": String(50), "from_date":  Date, "till_date": Date, 'duration': Integer, 
#     'amount': Numeric(10,2), 'currency': String(50), 'persons': String(50), 'weekdays': String(50), 'minimum_stay': Integer, 
#     'maximum_stay': Integer, 'extra_person_fee_amount': Numeric(10,2), 'extra_person_fee_currency': String(50)}

# rates_df = pd.DataFrame([('NP' + x.HouseID, x.fromDate, x.tillDate, x.duration, x.amount, x.currency, 
#     x.persons, x.weekdays, x.minimumStay, x.maximumStay, x.extraPersonFeeAmount, x.extraPersonFeeCurrency) for x in 
#     rate.query.all()], columns=('sojournID', 'from_date', 'till_date', 'duration', 'amount', 'currency', 
#     'persons', 'weekdays', 'minimum_stay', 'maximum_stay', 'extra_person_fee_amount', 'extra_person_fee_currency'))

# rates_df.index = rates_df.index.rename('id')

# print('\n# of rates:')
# print(rates_df.count()['sojournID'])

# rates_df.to_sql('rates', con = sojourn_engine, if_exists = 'replace', chunksize = 1000, dtype=fieldschema) 

