from dotenv import load_dotenv
import os
import pymysql 
import pandas as pd
import datetime
from tabulate import tabulate

load_dotenv()

script_start = datetime.datetime.now()


import_date = datetime.datetime.now().strftime("%Y-%m-%d")
yesterday = (datetime.datetime.now() - datetime.timedelta(days = 1)).strftime("%Y-%m-%d")

SOJOURN_HOST = os.getenv('SOJOURN_HOST')
SOJOURN_USER = os.getenv('SOJOURN_USER')
SOJOURN_PWD = os.getenv('SOJOURN_PWD')

conn = pymysql.connect(host=SOJOURN_HOST, user=SOJOURN_USER, passwd=SOJOURN_PWD, db='sojourn', port=3306, charset='utf8mb4')

print('\n**** UPDATE META TAGS AND STAR RATINGS... ****')

cur = conn.cursor()
cur.execute('USE sojourn')

sql_string = "set sql_safe_updates = 0;"
exec_result = cur.execute(sql_string)
sql_string = "update sojourn.places set star_rating = 0 where star_rating is null;"
exec_result = cur.execute(sql_string)
print('\n' + sql_string)
print(f'\n{exec_result} row(s) affected...')
conn.commit()

# sql_string = f'''UPDATE sojourn.places pc
#     INNER JOIN sojourn.meta_tags mt
#     ON pc.sojournID = mt.sojournID
#     SET pc.meta_tags = mt.meta_tags;'''
# exec_result = cur.execute(sql_string)
# print('\n' + sql_string)
# print(f'\n{exec_result} row(s) affected...')
# conn.commit()

# sql_string = f'''UPDATE sojourn.places pc
#     INNER JOIN sojourn.star_ratings sr 
#     ON pc.sojournID = sr.sojournID
#     SET pc.star_rating = sr.star_rating;'''
# exec_result = cur.execute(sql_string)
# print('\n' + sql_string)
# print(f'\n{exec_result} row(s) affected...')
# conn.commit()

# # add the #Evolve hashtag if it's not there....
# sql_string = f"update sojourn.places set meta_tags = '|Evolve' where sojournID like 'FX%' and meta_tags is null;"
# exec_result = cur.execute(sql_string)
# print('\n' + sql_string)
# print(f'\n{exec_result} row(s) affected...')
# conn.commit()


# sql_string = f"update sojourn.places set meta_tags = concat('|Evolve',meta_tags) where sojournID like 'FX%' and meta_tags not like '%Evolve%';"
# exec_result = cur.execute(sql_string)
# print('\n' + sql_string)
# print(f'\n{exec_result} row(s) affected...')
# conn.commit()

# add the #Nextpax hashtag if it's not there...
sql_string = f"update sojourn.places set meta_tags = '|Nextpax' where sojournID like 'OR%' and meta_tags is null;"
exec_result = cur.execute(sql_string)
print('\n' + sql_string)
print(f'\n{exec_result} row(s) affected...')
conn.commit()


sql_string = f"update sojourn.places set meta_tags = concat('|Nextpax',meta_tags) where sojournID like 'OR%' and meta_tags not like '%Nextpax%';"
exec_result = cur.execute(sql_string)
print('\n' + sql_string)
print(f'\n{exec_result} row(s) affected...')
conn.commit()


# # add the #Whimstay hashtag if it's not there...
# sql_string = f"update sojourn.places set meta_tags = '|Whimstay' where sojournID like 'XU%' and meta_tags is null;"
# exec_result = cur.execute(sql_string)
# print('\n' + sql_string)
# print(f'\n{exec_result} row(s) affected...')
# conn.commit()


# sql_string = f"update sojourn.places set meta_tags = concat('|Whimstay',meta_tags) where sojournID like 'XU%' and meta_tags not like '%Whimstay%';"
# exec_result = cur.execute(sql_string)
# print('\n' + sql_string)
# print(f'\n{exec_result} row(s) affected...')
# conn.commit()

cur.close()
conn.close()

cur = None


