PATH=$PATH:/home/henry_schroy/google-cloud-sdk/bin
# cd ~/daily/nextpaxjob;
if [ -f /.dockerenv ]; then
    echo "Running $0 inside a Docker container"
    cd /app/app/daily;
else
    echo "$0 (Not running inside a Docker container)"
    cd /home/henry_schroy/daily/nextpaxjob/;
    source /home/henry_schroy/daily/nextpaxjob/venv/venv/bin/activate;
fi

if [ -f "bkgpal.is.running" ]; then
    echo "bkgpal extract is running already!"
    exit 1
else
    touch "bkgpal.is.running";
fi


# rm bkgpal_job.log;
if [ -f "bkgpal_job.log" ]; then
    rm -f bkgpal_job.log;
fi

(time bash bkgpal_job.sh) > bkgpal_job.log 2>&1
# source "/home/henry_schroy/venv/bin/activate"
# cd ~/daily/nextpaxjob;
if [ -f /.dockerenv ]; then
    echo "Running $0 inside a Docker container"
else
    echo "$0 (Not running inside a Docker container)"
    cd /home/henry_schroy/daily/nextpaxjob/;
    source /home/henry_schroy/daily/nextpaxjob/venv/venv/bin/activate;
fi


python send_bkgpal_email.py;

if [ -f "bkgpal.is.running" ]; then
    echo "Removing bkgpal.is.running file"
    rm -f "bkgpal.is.running";
fi
