import pymysql 
import pandas as pd
import datetime
from tabulate import tabulate
from dotenv import load_dotenv
import os
load_dotenv()

script_start = datetime.datetime.now()


import_date = datetime.datetime.now().strftime("%Y-%m-%d")
yesterday = (datetime.datetime.now() - datetime.timedelta(days = 1)).strftime("%Y-%m-%d")

SOJOURN_HOST = os.getenv('SOJOURN_HOST')
SOJOURN_USER = os.getenv('SOJOURN_USER')
SOJOURN_PWD = os.getenv('SOJOURN_PWD')
NEXTPAX_HOST = os.getenv('NEXTPAX_HOST')
NEXTPAX_USER = os.getenv('NEXTPAX_USER')
NEXTPAX_PWD = os.getenv('NEXTPAX_PWD')

conn = pymysql.connect(host=SOJOURN_HOST, user=SOJOURN_USER, passwd=SOJOURN_PWD, db='sojourn', port=3306, charset='utf8mb4')
conn2 = pymysql.connect(host=NEXTPAX_HOST, user=NEXTPAX_USER, passwd=NEXTPAX_PWD, db='nextpax', port=3306, charset='utf8mb4')

print('\n**** FINAL SUMMARIES... ****')

cur = conn.cursor()
cur.execute('USE sojourn')
sql_string = 'select MIN(calendar_date) AS earliest_date from sojourn.calendars'
cur.execute(sql_string)
print('\n\n\n' + sql_string + '\n')
for row in cur:
    print(row[0].strftime("Earliest Sojourn calendars date: %m/%d/%Y"))

cur.close()
conn.close()

cur = None

cur = conn2.cursor()
cur.execute('USE nextpax')
sql_string = 'select PropertyManagementSystem, MIN(fromDate) AS earliest_date, MAX(tillDate) AS last_date \n' + \
    '\tfrom nextpax.rates group by PropertyManagementSystem order by 2, 1'

#cur.execute(sql_string)
print('\n\n' + sql_string + '\n')

print('\nEarliest/latest NEXTPAX rates date by PMS:\n')

cur.execute(sql_string)
df2 = cur.fetchall()
df = pd.DataFrame(df2,columns=['PropertyManagementSystem','earliest_date','last_date'])

# df = pd.read_sql(sql_string,conn2)

print(df)


sql_string = 'select PropertyManagementSystem, MIN(calendarDate) AS earliest_date, MAX(calendarDate) AS last_date \n' + \
    '\tfrom nextpax.calendars group by PropertyManagementSystem order by 2, 1'

#cur.execute(sql_string)
print('\n\n' + sql_string + '\n')

print('\nEarliest/latest NEXTPAX calendars date by PMS:\n')

cur.execute(sql_string)
df2 = cur.fetchall()
df = pd.DataFrame(df2,columns=['PropertyManagementSystem','earliest_date','last_date'])
# df = pd.read_sql(sql_string,conn2)

print(df)

sql_string = f"""
INSERT INTO nextpax.daily_houses (import_date, HouseID)
SELECT dh.* FROM
(SELECT '{import_date}' AS import_date, HouseID 
FROM nextpax.houses) dh
ON DUPLICATE KEY UPDATE import_date=dh.import_date, HouseID=dh.HouseID;
"""

print('\n\nUpdating nextpax.daily_houses inventory list')
print('\n' + sql_string)
exec_result = cur.execute(sql_string)
print(f'\n{exec_result} row(s) affected...')
conn2.commit()

print('\n\nsee if any HouseIDs have been added over the last day:')

sql_string = f"""
SELECT dh.HouseID, hs.Partner, bs.PMC, hs.Brand, hs.Place, hs.Country, hs.ZipCode, hs.HouseName, hs.HouseType
FROM nextpax.daily_houses dh, nextpax.houses hs,nextpax.brands bs 
WHERE dh.HouseID = hs.HouseID AND hs.Brand = bs.BrandID 
AND hs.Brand <> 'DM' 
AND import_date =  '{import_date}' AND dh.HouseID NOT IN 
(SELECT HouseID FROM nextpax.daily_houses WHERE import_date = '{yesterday}')
ORDER BY Place, HouseID
"""

print('\n' + sql_string)
cur.execute(sql_string)
df2 = cur.fetchall()
df = pd.DataFrame(df2,columns=['HouseID','Partner','PMC','Brand','Place','Country','ZipCode','HouseName','HouseType'])
# df = pd.read_sql(sql_string,conn2)

if df.size == 0:
    print('\nZero (0) HouseIDs added...')
else:
    print(f'\n{len(df.index)} HouseIDs added:')
    summary_fields = ['HouseID', 'Partner','PMC','Brand','Place','Country','ZipCode','HouseName']
    print("<pre><code>")
    print(tabulate(df[summary_fields], headers=summary_fields, showindex=False))
    print("</code></pre>")


print('\n\nsee if any HouseIDs have been dropped over the last day:')

sql_string = f"""SELECT Partner, Brand, HouseID, Country, Place, ZipCode, HouseName 
FROM nextpax.houses_persist WHERE HouseID IN (
SELECT HouseID FROM nextpax.daily_houses WHERE import_date = '{yesterday}' AND HouseID NOT IN (
SELECT HouseID FROM nextpax.daily_houses WHERE import_date = '{import_date}'))
ORDER BY Place, HouseID
"""

print('\n' + sql_string)
cur.execute(sql_string)
df2 = cur.fetchall()
df = pd.DataFrame(df2,columns=['Partner','Brand','HouseID','Country','Place','ZipCode','HouseName'])
# df = pd.read_sql(sql_string,conn2)

if df.size == 0:
    print('\nZero (0) HouseIDs dropped...')
else:
    print(f'\n{len(df.index)} HouseIDs dropped:')
    summary_fields = ['Partner', 'Brand','HouseID','Country','Place','ZipCode','HouseName']
    print("<pre><code>")
    print(tabulate(df[summary_fields], headers=summary_fields, showindex=False))
    print("</code></pre>")


print("\nAdd to nextpax.houses_persist table if there's anything new to add...")
# add to houses_persist table
sql_string = f"""
INSERT INTO nextpax.houses_persist (Partner, Brand, HouseID, Country, Place, ZipCode,
HouseName, MinPersons, MaxPersons, NumberOfPets, HouseType, Latitude,
Longitude, ExternalPropertyId, ParentId, Classification)
SELECT hs.* FROM 
(SELECT Partner, Brand, HouseID, Country, Place, ZipCode,
HouseName, MinPersons, MaxPersons, NumberOfPets, HouseType, Latitude,
Longitude, ExternalPropertyId, ParentId, Classification FROM nextpax.houses) hs
ON DUPLICATE KEY UPDATE Partner=hs.Partner, Brand=hs.Brand, HouseID=hs.HouseID, 
Country=hs.Country, Place=hs.Place, ZipCode=hs.ZipCode,
HouseName=hs.HouseName, MinPersons=hs.MinPersons, MaxPersons=hs.MaxPersons, 
NumberOfPets=hs.NumberOfPets, HouseType=hs.HouseType, 
Latitude=hs.Latitude, Longitude=hs.Longitude, ExternalPropertyId=hs.ExternalPropertyId,
ParentId=hs.ParentId, Classification=hs.Classification
"""
print(sql_string)

exec_result = cur.execute(sql_string)

print(f'{exec_result} row(s) affected...')
conn2.commit()

# FIND EMPTY DESCRIPTIONS
print("\nFill any empty descriptions...")
sql_string = f"""SELECT HouseID FROM nextpax.houses_persist WHERE Description IS NULL"""
print(sql_string)
exec_result = cur.execute(sql_string)
result = cur.fetchall()

print(f'{exec_result} row(s) affected...')

for row in result:
    HouseID = row[0]
    sql_string = f"""
    SELECT QUOTE(Description) FROM nextpax.descriptions 
        WHERE HouseID = '{HouseID}' 
        AND LanguageCode = 'en' 
        LIMIT 1"""
    print('\n' + sql_string)
    exec_result = cur.execute(sql_string)
    description = cur.fetchone()

    if description is None:
        description = "'No description provided.'"
    else:
        description = description[0]
    
    sql_string = f"""UPDATE nextpax.houses_persist SET Description = {description} WHERE HouseID = '{HouseID}'"""
    print('\n' + sql_string)

    exec_result = cur.execute(sql_string)

    print(f'\n{exec_result} row(s) affected...')
    conn2.commit()

# FIND MISSING RATES (will only store rates as of the first time they're added)
print("\nFind missing rates (will only store rates as of the first time they're added to the houses_persist table)...")
sql_string = f"""SELECT HouseID FROM nextpax.houses_persist WHERE RateAmount IS NULL"""
print(sql_string)
exec_result = cur.execute(sql_string)
result = cur.fetchall()

print(f'{exec_result} row(s) affected...')

for row in result:
    HouseID = row[0]
    sql_string = f"""
        SELECT fromDate AS RateDate, amount/duration As RateAmount
        FROM nextpax.rates
        WHERE HouseID = '{HouseID}'
        ORDER BY fromDate, duration
        LIMIT 1
        """
    print('\n' + sql_string)
    exec_result = cur.execute(sql_string)
    rate = cur.fetchone()
    if rate is None:
        rate_amount = 0
        rate_date = import_date
    else:
        rate_amount = rate[1]
        rate_date = rate[0].strftime("%Y-%m-%d")

    sql_string = f"""
        UPDATE nextpax.houses_persist SET RateAmount = {rate_amount},
        RateDate = '{rate_date}'
        WHERE HouseID = '{HouseID}'
        """
    print('\n' + sql_string)
    
    exec_result = cur.execute(sql_string)

    print(f'\n{exec_result} row(s) affected...')
    conn2.commit()

# FIND MISSING PICTURES
print("\nFind missing pictures...")
sql_string = f"""SELECT HouseID FROM nextpax.houses_persist WHERE Picture IS NULL"""
print(sql_string)
exec_result = cur.execute(sql_string)
result = cur.fetchall()

print(f'{exec_result} row(s) affected...')


for row in result:
    HouseID = row[0]
    sql_string = f"""
        SELECT FileUrl AS Picture
        FROM nextpax.pictures
        WHERE HouseID = '{HouseID}'
        ORDER BY id
        LIMIT 1
        """
    print('\n' + sql_string)
    exec_result = cur.execute(sql_string)
    image = cur.fetchone()
    if image is None:
        image = ''
    else:
        image = image[0]

    sql_string = f"""
        UPDATE nextpax.houses_persist SET Picture = '{image}'
        WHERE HouseID = '{HouseID}'
        """
    print('\n' + sql_string)

    exec_result = cur.execute(sql_string)

    print(f'\n{exec_result} row(s) affected...')
    conn2.commit()

print('\n\n\nNumber of places by pmc/brand:')

sql_string = f"""
    (select dm.*, br.Num_Places from
    (SELECT distinct Partner, Brand, bs.PMC, bs.MainContact, bs.Location
    FROM nextpax.houses hs left join nextpax.brands bs
    on hs.Brand = bs.BrandID
    where Brand <> 'DM') dm
    left join (SELECT Brand, Count(Brand) num_places FROM nextpax.houses
    group by Brand) br
    on dm.Brand = br.Brand 
    order by Brand)
    union
    (select '  -- Total --','','','','', Count(Brand) from nextpax.houses where Brand <> 'DM')
    """
cur.execute(sql_string)
df2 = cur.fetchall()
df = pd.DataFrame(df2,columns=['Partner', 'Brand','PMC','MainContact','Location','Num_Places'])
# df = pd.read_sql(sql_string,conn2)

summary_fields = ['Partner', 'Brand','PMC','MainContact','Location','Num_Places']
print("<pre><code>")
print(tabulate(df[summary_fields], headers=summary_fields, showindex=False))
print("</code></pre>")


cur.close()
conn2.close()


print('\n\n')


print(f"\nNEXTPAX final summary script elapsed time: {datetime.datetime.now() - script_start}")
